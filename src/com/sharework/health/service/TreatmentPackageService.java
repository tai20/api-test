package com.sharework.health.service;

import com.sharework.health.dto.TreatmentPackageDto;
import com.sharework.health.dto.TreatmentPackageServiceDto;

import java.util.List;

public interface TreatmentPackageService extends BaseService<TreatmentPackageDto, Integer> {

    TreatmentPackageDto searchTreatmentPackageByName(String name);

    boolean addTreatmentPackageService(TreatmentPackageServiceDto dto);

    List<TreatmentPackageServiceDto> findAllByTreatmentPackage(Integer treatmentPackageId);

    TreatmentPackageDto findTreatmentPackageAfterInsert();
}
