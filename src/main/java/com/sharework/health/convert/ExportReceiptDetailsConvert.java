package com.sharework.health.convert;

import com.sharework.health.dto.*;
import com.sharework.health.entity.ExportReceiptDetails;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ExportReceiptDetailsConvert implements BaseConvert<ExportReceiptDetails, ExportReceiptDetailsDto>{

    @Override
    public ExportReceiptDetailsDto entityToDto(ExportReceiptDetails entity){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, ExportReceiptDetailsDto.class)
                .setExportReceiptDto(modelMapper.map(entity.getExportReceipt(), ExportReceiptDto.class))
                .setClinicStockDto(modelMapper.map(entity.getClinicStock(), ClinicStockDto.class))
                .setProductBatchDto(modelMapper.map(entity.getProductBatchId(), ProductBatchDto.class));
    }

    @Override
    public ExportReceiptDetails dtoToEntity(ExportReceiptDetailsDto dto){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, ExportReceiptDetails.class);
    }
}
