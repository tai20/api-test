package com.sharework.health.controller.admin;

import com.sharework.health.dto.ClinicDto;
import com.sharework.health.service.ClinicService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/clinics")
@AllArgsConstructor
public class ClinicController {

    private ClinicService clinicService;

    @GetMapping("")
    public Object getAllClinic() {
        List<ClinicDto> clinicDtos = clinicService.findAll();
        if (clinicDtos.isEmpty()) {
            return new ResponseEntity<>("Không có dữ liệu", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(clinicDtos,HttpStatus.OK);
    }

    @GetMapping("/{clinic_id}")
    public Object getClinicById(@PathVariable("clinic_id") Integer id) {
        ClinicDto dto = clinicService.findById(id);
        if (dto == null) {
            return new ResponseEntity<>("Không tìm thấy", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }

    @PostMapping("")
    public Object addClinic(@RequestBody ClinicDto dto,
                                           BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = clinicService.insert(dto);
        if (!result){
            return new ResponseEntity<>("Thêm phòng khám thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm phòng khám thành công",HttpStatus.CREATED);
    }

    @PutMapping("/{clinic_id}")
    public Object updateClinic(@PathVariable("clinic_id") Integer id , @RequestBody ClinicDto dto,
                                       BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = clinicService.update(id, dto);
        if (!result){
            return new ResponseEntity<>("Cập nhật thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhật thành công", HttpStatus.OK);
    }

    @PostMapping("searchClinicByClinicName")
    public Object searchClinicByClinicName(@RequestParam("clinic_name") String clinicName ) {

        ClinicDto clinicDto = clinicService.searchClinicByClinicName(clinicName);
        if (clinicDto == null){
            return new ResponseEntity<>("Không tìm thấy", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(clinicDto, HttpStatus.OK);
    }
}
