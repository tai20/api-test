package com.sharework.health.convert;

import com.sharework.health.dto.CustomerDto;
import com.sharework.health.entity.Customer;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class CustomerConvert implements BaseConvert<Customer, CustomerDto> {

    @Override
    public CustomerDto entityToDto(Customer entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, CustomerDto.class);
    }

    @Override
    public Customer dtoToEntity(CustomerDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, Customer.class);
    }
}
