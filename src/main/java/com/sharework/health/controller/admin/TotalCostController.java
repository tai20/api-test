package com.sharework.health.controller.admin;

import com.sharework.health.dto.TotalCostDto;
import com.sharework.health.service.TotalCostService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/totalcost")
@AllArgsConstructor
public class TotalCostController {

    private TotalCostService totalCostService;

    @GetMapping("")
    public Object getAllTotalCost() {
        List<TotalCostDto> totalCostDtos = totalCostService.findAll();
        if (totalCostDtos.isEmpty() || totalCostDtos == null) {
            return new ResponseEntity<>("Không có dữ liệu", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(totalCostDtos, HttpStatus.OK);
    }

    @PostMapping("")
    public Object addTotalCost(@RequestBody TotalCostDto dto, @RequestParam("medicalId") Integer id,
                               BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = totalCostService.insert(dto);
        if (!result) {
            return new ResponseEntity<>("Thêm yêu cầu thất bại", HttpStatus.BAD_REQUEST);
        }
        boolean resultUpdateTotalCostInMedicardCost = totalCostService.updateTotalCostInMedicalCard(id, totalCostService.getTotalCostAfterInsert());
        return new ResponseEntity<>("Thêm yêu cầu thành công", HttpStatus.CREATED);
    }

}
