package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@ToString
@Table(name = "TreatmentPackage")
public class TreatmentPackage implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @Enumerated(EnumType.STRING)
    private Status status;

    private BigDecimal total;

    @OneToMany(mappedBy = "treatmentPackage")
    private List<SlipUse> slipUses;

    @OneToMany(mappedBy = "treatmentPackage")
    private List<TreatmentPackageService> treatmentPackageServices;
}
