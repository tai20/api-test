package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Accessors
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class CategoryIndicationCard implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private String description;

    private BigDecimal price;

    @OneToMany(mappedBy = "categoryIndicationCard", fetch = FetchType.LAZY)
    private List<IndicationCardDetail> indicationCardDetails;

    @ManyToOne
    @JoinColumn(name = "subcategoryindicationcard_id")
    private SubCategoryIndicationCard subCategoryIndicationCard;

    @OneToMany(mappedBy = "categoryIndicationCard", fetch = FetchType.LAZY)
    private List<PackageTestServiceDetail> packageTestServiceDetails;

    @OneToMany(mappedBy = "categoryIndicationCard", fetch = FetchType.LAZY)
    private List<SlipUsePackageTestDetail> slipusePackageTestDetails;


}
