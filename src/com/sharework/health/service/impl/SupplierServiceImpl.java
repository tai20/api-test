package com.sharework.health.service.impl;

import com.sharework.health.convert.SupplierConvert;
import com.sharework.health.dto.SupplierDto;
import com.sharework.health.entity.*;
import com.sharework.health.repository.SupplierRepository;
import com.sharework.health.service.SupplierService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class SupplierServiceImpl implements SupplierService {

    private SupplierRepository supplierRepository;

    private SupplierConvert supplierConvert;

    @Override
    public List<SupplierDto> findAll() {
        List<Supplier> suppliers = supplierRepository.findAll();
        List<SupplierDto> supplierDtos = new ArrayList<>();

        for (Supplier entity: suppliers
             ) {
            if (entity.getStatus() != Status.DEACTIVE){
                SupplierDto dto = supplierConvert.entityToDto(entity);
                supplierDtos.add(dto);
            }
        }
        return supplierDtos;
    }

    @Override
    public SupplierDto findById(Integer id) {
        Supplier entity = supplierRepository.findById(id).get();
        if (entity == null){
            return null;
        }
        return supplierConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(SupplierDto dto) {
        if (dto == null) {
            return false;
        }
        try {
            Supplier entity = supplierConvert.dtoToEntity(dto);
            entity.setId(null);
            entity.setStatus(Status.ACTIVE);
            entity.setType(SupplierType.Supplier);
            supplierRepository.save(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(Integer id, SupplierDto dto) {
        Supplier entity = supplierRepository.findById(id).get();
        if (entity == null) {
            return false;
        }
        try {
            entity = supplierConvert.dtoToEntity(dto);
            supplierRepository.save(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        Supplier entity = supplierRepository.findById(id).get();
        try {
            entity.setStatus(Status.DEACTIVE);
            supplierRepository.save(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<SupplierDto> findAllByStatusLikeSupplier() {
        List<Supplier> suppliers = supplierRepository.findAllByStatusLikeSupplier();
        List<SupplierDto> supplierDtos = new ArrayList<>();

        for (Supplier entity: suppliers
             ) {
            SupplierDto dto = supplierConvert.entityToDto(entity);
            supplierDtos.add(dto);
        }
        return supplierDtos;
    }

    @Override
    public List<SupplierDto> findAllByStatusLikeClinic() {
        List<Supplier> suppliers = supplierRepository.findAllByStatusLikeClinic();
        List<SupplierDto> supplierDtos = new ArrayList<>();

        for (Supplier entity: suppliers
        ) {
            SupplierDto dto = supplierConvert.entityToDto(entity);
            supplierDtos.add(dto);
        }
        return supplierDtos;
    }
}
