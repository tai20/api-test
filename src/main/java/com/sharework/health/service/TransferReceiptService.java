package com.sharework.health.service;

import com.sharework.health.dto.*;

import java.util.List;

public interface TransferReceiptService extends BaseService<TransferReceiptDto, Integer>{
    public TransferReceiptDto insertTransferReceipt(TransferReceiptDto dto);
    public TransferReceiptDto confirmRecevied(Integer id);
    public TransferReceiptDetailsDto insertTransferReceiptDetail(TransferReceiptDetailsDto dto);
    public List<TransferReceiptDetailsQueryDto> getAllTransferReceiptDetailByReceiptid(Integer id);
    public boolean deleteTransferReceipt(Integer id);
    public boolean deleteTransferReceiptDetail(Integer receipt_id, Integer clinic_stock_id);
    public TransferReceiptDetailsQueryDto updateTransferReceiptDetail(TransferReceiptDetailsQueryDto dto);
    public boolean restoreTransferReceipt(Integer id);
    List<TransferReceiptDto> findAllByIdClinic(Integer id);

    public TransferReceiptDto findByIdAnDRole(Integer id, Integer user_id);
}
