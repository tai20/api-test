package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Accessors
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CountExaminationOfDoctorByMonthAndYearDto {

    private Integer userId;

    private String userName;

    private Integer clinicId;

    private String clinicName;

    private Integer serviceId;

    private String serviceName;

    private Long countExamination;

}
