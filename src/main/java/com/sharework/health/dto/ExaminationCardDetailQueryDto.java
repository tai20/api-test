package com.sharework.health.dto;

import com.sharework.health.entity.ExaminationStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ExaminationCardDetailQueryDto {

    private Integer examinationCardId;

    private LocalDateTime dateOfExamination;

    private String userName;

    private Integer serviceId;

    private String serviceName;

    private String customerName;

    private ExaminationStatus status;

    private Integer timeUse;
}
