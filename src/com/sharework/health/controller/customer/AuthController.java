package com.sharework.health.controller.customer;

import com.sharework.health.dto.LoginDto;
import com.sharework.health.dto.PasswordDto;
import com.sharework.health.dto.TokenAndRoleNameDto;
import com.sharework.health.dto.UserDto;
import com.sharework.health.service.AuthService;
import com.sharework.health.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth")
@AllArgsConstructor
public class AuthController {

    private AuthService authService;

    private UserService userService;

    @PostMapping("")
    public Object auth(@RequestBody LoginDto dto) {
        if (dto == null) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }

        String token = authService.auth(dto);
        if (token != null) {
            UserDto userDto = userService.findByEmail(dto.getEmail());
            if (userDto != null) {
//                Map<String, String> map = new HashMap<>();
//                map.put(token,userDto.getRoleDto().getName());
                TokenAndRoleNameDto dto1 = new TokenAndRoleNameDto()
                        .setToken(token)
                        .setRoleName(userDto.getRoleDto().getName());
                return new ResponseEntity<>(dto1, HttpStatus.CREATED);
            }

        }
        //return new ResponseEntity<>(token, HttpStatus.CREATED);
        return new ResponseEntity<>("Lỗi", HttpStatus.BAD_REQUEST);
    }

    @PutMapping("resetPassword")
    public Object resetPassword(@RequestParam("email") String email) {
        if (email == null) {
            return new ResponseEntity<>("Chưa nhập email", HttpStatus.BAD_REQUEST);
        }
        LoginDto dto = userService.resetPassword(email);
        return new ResponseEntity<>(dto, HttpStatus.CREATED);
    }

    @GetMapping("profile")
    public Object getProfile() {
        //String token = authService.auth(dto);
        try {
            UserDto userDto = userService.getProfile();
            if (userDto == null) {
                return new ResponseEntity<>("Lỗi", HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(userDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Lỗi", HttpStatus.BAD_REQUEST);

    }

    @PutMapping("updateProfile")
    public Object updateProfile(@RequestBody UserDto dto) {
        if (dto == null) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        //String token = authService.auth(dto);
        boolean result = userService.updateProfile(dto);
        if (!result) {
            return new ResponseEntity<>("Cập nhật không thành công", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhật thành công", HttpStatus.OK);
    }

    @PutMapping("changePassword")
    public Object changePassword(@RequestBody PasswordDto dto) {
        if (dto == null) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        String message = userService.changePassword(dto);
        return new ResponseEntity<>(message, HttpStatus.OK);
    }
}
