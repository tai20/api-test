package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Embeddable
public class ExaminationCardDetail_PK implements Serializable {

    private Integer examinationCard;

    private Integer service;

    private Integer user;
}
