package com.sharework.health.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.sharework.health.filter.AuthFilter;

@Configuration
@EnableWebSecurity
public class AdminSecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
	private UserDetailsService userDetailsService;

	
	@Bean
	@Override
	protected AuthenticationManager authenticationManager() throws Exception {
		return super.authenticationManager();
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring()
		.antMatchers("/v2/api-docs",
		"/configuration/ui",
		"/swagger-resources/**",
		"/configuration/security",
		"/swagger-ui.html",
		"/webjars/**");


	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {

//		http.csrf().disable()
//		.antMatcher("/api/admin/**")
//		.authorizeRequests()
//		.antMatchers("/api/auth")
//		.permitAll();

		http.cors().and().csrf().disable()
		//http.csrf().disable()
				.antMatcher("/api/**")
				.authorizeRequests()
				.antMatchers("/api/auth", "/api/images/**", "/api/signatures/**","/api/getFiles/**")
				.permitAll()
				.antMatchers("/api/admin/**", "/api/auth/**")
				.hasAnyRole("ADMIN", "ACCOUNTANT", "OPERATION", "DOCTOR", "COUNSELOR", "TECHNICIAN", "SUBWAREHOUSE", "BRANCHWAREHOUSE", "RECEPTIONIST", "OBSTETRICIAN", "RECEPTIONIST_OBSTETRICAL")
				.anyRequest()
				.authenticated();

		http.addFilter(new AuthFilter(authenticationManager(),userDetailsService));
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService)
		//.passwordEncoder();
		.passwordEncoder(new BCryptPasswordEncoder());
	}
		
}
