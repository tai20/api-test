package com.sharework.health.convert;

import com.sharework.health.dto.MedicalCardDto;
import com.sharework.health.dto.TotalCostDto;
import com.sharework.health.entity.TotalCost;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class TotalCostConvert implements BaseConvert<TotalCost, TotalCostDto> {

    @Override
    public TotalCostDto entityToDto(TotalCost entity) {

        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, TotalCostDto.class);
    }

    @Override
    public TotalCost dtoToEntity(TotalCostDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, TotalCost.class);
    }
}
