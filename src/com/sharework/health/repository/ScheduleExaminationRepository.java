package com.sharework.health.repository;

import com.sharework.health.entity.ScheduleExamination;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScheduleExaminationRepository extends JpaRepository<ScheduleExamination, Integer> {
    @Query(nativeQuery = true, value = "select se.* from scheduleexamination se join customer c on se.customer_id = c.id where c.id= :customerId")
    ScheduleExamination findScheduleExaminationByCustomer(Integer customerId);

    @Query(nativeQuery = true, value = "select se.* from scheduleexamination se join users u on se.user_id = u.id where u.id= :userId")
    List<ScheduleExamination> findAllByUser(Integer userId);

    @Query(nativeQuery = true, value = "select * from scheduleexamination order by id desc limit 1")
    ScheduleExamination getScheduleExaminationAfterInsert();

    @Query(nativeQuery = true, value = "select * from scheduleexamination where (current_date - dateofexamination::timestamp::date) <= 3")
    List<ScheduleExamination> getAllScheduleExaminationNearest();
}
