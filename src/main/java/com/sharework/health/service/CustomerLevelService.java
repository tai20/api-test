package com.sharework.health.service;

import com.sharework.health.dto.CustomerLevelDto;
import com.sharework.health.dto.RankCustomerDto;
import com.sharework.health.entity.Customer;
import com.sharework.health.entity.CustomerLevel;
import io.swagger.models.auth.In;

import java.math.BigDecimal;
import java.util.List;

public interface CustomerLevelService extends BaseService<CustomerLevelDto,Integer>{


     void updateCustomerLevelByid(Integer customer_id, BigDecimal order_value_total);

     List<CustomerLevel> getListCustomerLevelById(Integer customer_id);

     void updateCustomerLevelByuid(Integer customer_id, BigDecimal order_value_total);
}
