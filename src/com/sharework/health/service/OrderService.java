package com.sharework.health.service;

import java.time.LocalDateTime;
import java.util.List;

import com.sharework.health.dto.*;

public interface OrderService extends BaseService<OrderDto, Integer> {

	
	boolean addOrderDetailProduct(OrderDetailProductDto dto);

	boolean addOrderDetailTreatmentPackage(OrderDetailTreatmentPackageDto dto);

	boolean addOrderDetailService(OrderDetailServiceDto dto);

	boolean updateOrderDetailProduct(OrderDetailProductDto dto);
	
	List<OrderDto> getOrderByOrderDate(LocalDateTime orderDate);
	
	List<OrderDto> getOrderByCustomer(Integer customerId);
	
	List<OrderDto> getOrderByUser(Integer userId);

	List<OrderDetailProductDto> findAllOrderDetailProductWithOrder(Integer orderId);

	List<OrderDetailTreatmentPackageDto> findAllOrderDetailTreatmentPackageWithOrder(Integer orderId);

	List<OrderDetailServiceDto> findAllOrderDetailServiceWithOrder(Integer orderId);

	List<OrderDto> findAllOrderByClinic(Integer clinicId);

	List<OrderDto> findAllOrderWithExamination();

	List<OrderDto> findAllOrderWithOrderDetails();

	List<OrderDto> findByOrderDateBetween(LocalDateTime startDate, LocalDateTime endDate);

	boolean confirmOrderBeforePayment(Integer orderId);

	boolean paymentOrder(Integer orderId, PaymentDto dto);

	boolean paymentRest(Integer orderId, PaymentDto dto);

	OrderDto getOrderAfterInsert();

	boolean paymentClinic(Integer orderId, PaymentDto dto);


}
