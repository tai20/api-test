package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "Product")
public class Product implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @Enumerated(EnumType.STRING)
    private Status status;

    private int quantity;

    @Column(precision = 19, scale = 2)
    private BigDecimal wholesalePrice;

    private String unit;

    private int capacity;

    private int totalCapacity;

    @Column(precision = 19, scale = 2)
    private BigDecimal price;

    @ManyToOne
    @JoinColumn(name = "product_category_id")
    private ProductCategory productCategory;

    @OneToMany(mappedBy = "product")
    private List<WarehouseReceiptDetail> warehouseReceiptDetails;

    @OneToMany(mappedBy = "product")
    private List<OrderDetailProduct> orderDetailProducts;

    @ManyToOne
    @JoinColumn(name = "clinic_id")
    private Clinic clinic;

    @OneToMany(mappedBy = "product")
    private List<SlipReturnProductDetail> slipReturnProductDetails;

    @OneToMany(mappedBy = "product")
    private List<HistoryReturnProductDetail> historyReturnProductDetails;

    @OneToMany(mappedBy = "product")
    private List<ProductServiceDetail> productServiceDetails;

    @OneToMany(mappedBy = "product")
    private List<PrescriptionDetail> prescriptionDetails;
}
