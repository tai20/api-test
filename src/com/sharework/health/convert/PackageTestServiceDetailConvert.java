package com.sharework.health.convert;

import com.sharework.health.dto.PackageTestServiceDetailDto;
import com.sharework.health.entity.PackageTestServiceDetail;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class PackageTestServiceDetailConvert implements BaseConvert<PackageTestServiceDetail, PackageTestServiceDetailDto> {
    @Override
    public PackageTestServiceDetailDto entityToDto(PackageTestServiceDetail entity) {

        //Convert
        PackageTestConvert packageTestConvert = new PackageTestConvert();

        CategoryIndicationCardConvert categoryIndicationCardConvert = new CategoryIndicationCardConvert();

        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, PackageTestServiceDetailDto.class)
                .setPackageTestDto(packageTestConvert.entityToDto(entity.getPackageTest()))
                .setCategoryIndicationCardDto(categoryIndicationCardConvert.entityToDto(entity.getCategoryIndicationCard()));
    }

    @Override
    public PackageTestServiceDetail dtoToEntity(PackageTestServiceDetailDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, PackageTestServiceDetail.class);
    }
}
