package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "exportreceipt")
public class ExportReceipt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "clinic_id")
    private Clinic clinic;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;

    private LocalDateTime dateexport;

    @Column(precision = 19, scale = 2)
    private BigDecimal sumprice;

    @Enumerated(EnumType.STRING)
    private ReceiptStatus status;

    @Column(length = 255)
    private String reason;

    @OneToMany(mappedBy = "exportReceipt")
    private List<ExportReceiptDetails> exportReceiptDetails;
}
