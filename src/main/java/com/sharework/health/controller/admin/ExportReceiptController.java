package com.sharework.health.controller.admin;

import com.sharework.health.Constants;
import com.sharework.health.dto.*;
import com.sharework.health.entity.ExportReceipt;
import com.sharework.health.repository.ExportReceiptRepository;
import com.sharework.health.response.ExportReceiptReponse;
import com.sharework.health.response.ImportRecepitReponse;
import com.sharework.health.service.ExportReceiptService;
import com.sharework.health.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/admin/exportreceipts")
@AllArgsConstructor
public class ExportReceiptController {

    private ExportReceiptService exportReceiptService;
    private ExportReceiptRepository exportReceiptRepository;
    private UserService userService;

    //lay danh sach xuat hang theo phan quyen kho
    @GetMapping("")
    public ResponseEntity<ResponseObject> getAllExportReceipt(Principal principal){
        String email=principal.getName();
        UserDto user = userService.findByEmail(email); //Tim tai khoan theo token
        List<ExportReceiptDto> exportReceiptDtos;
        if(user.getClinicDto().getType().name().equals("DEPOT")) {
            exportReceiptDtos = exportReceiptService.findAll();

            if (exportReceiptDtos.isEmpty()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ResponseObject(400, "Không có dữ liệu", "")
                );
            }

        }else{
            exportReceiptDtos = exportReceiptService.getListExportReceiptByClinicId(user.getClinicDto().getId());
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(200, "Truy vấn thành công", exportReceiptDtos)
        );
    }

    //controller tra ve danh sach cac phieu xuat theo kho
//    @GetMapping("/warehouse")
//    public ResponseEntity<ResponseObject> getAllExportWareHouseReceipt(@RequestParam("clinic_id") Integer clinic_id){
//        List<ExportReceiptDto> exportReceiptDtos = exportReceiptService.getAllExportWarehouseReceipt(clinic_id);
//        if(exportReceiptDtos.isEmpty()){
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
//                    new ResponseObject(400, "Không có dữ liệu", "")
//            );
//        }
//        return ResponseEntity.status(HttpStatus.OK).body(
//                new ResponseObject(200,  "Truy vấn thành công", exportReceiptDtos)
//        );
//    }


    @GetMapping("/{exportReceipt_id}")
    public ResponseEntity<ResponseObject> getExportReceiptById(@PathVariable("exportReceipt_id") Integer id){
        ExportReceiptDto dto = exportReceiptService.findById(id);
        if (dto!=null)
        {
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Truy vấn thành công", dto)
            );

        } else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject(404, "Không tìm thấy", "")
            );
    }


    @PostMapping("")
    public ResponseEntity<ResponseObject> addExportReceipt(@RequestBody ExportReceiptDto dto ){
        if (dto == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không có dữ liệu", "")
            );
        }
        ExportReceiptDto result = exportReceiptService.insertExportReceipt(dto);
        if(result == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Thêm không thành công", "")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(200,  "Thêm phiếu thành công", result)
        );
    }

    @GetMapping("/detail/{receipt_id}")
    public ResponseEntity<ResponseObject> getExportReceiptDetails(@PathVariable("receipt_id") Integer id){
        List<ExportReceiptDetailsQueryDto> dto = exportReceiptService.getAllExportReceiptDetailByReceiptid(id);
        if (dto.isEmpty())
        {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject(404, "Không tìm thấy", "")
            );
        } else
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Truy vấn thành công", dto)
            );

    }


    @PostMapping("/detail")
    public ResponseEntity<ResponseObject> addExportReceiptDetail(@RequestBody ExportReceiptDetailsDto dto){
        if (dto == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không có dữ liệu", "")
            );
        }
        ExportReceiptDetailsDto result = exportReceiptService.insertExportReceiptDetail(dto);
        if(result == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Thêm không thành công", "")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(200,  "Thêm phiếu thành công", result)
        );
    }



    @DeleteMapping("{receipt_id}")
    public ResponseEntity<ResponseObject> deleteExportReceipt(@PathVariable("receipt_id") Integer id){
        boolean result = exportReceiptService.deleteExportReceipt(id);
        if (result){
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Đã xóa thành công", "")
            );
        } else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không được xóa", "")
            );
        }
    }


    @DeleteMapping("/detail/{receipt_id}/{clinic_stock_id}")
    public ResponseEntity<ResponseObject> deleteExportReceiptDetail(@PathVariable("receipt_id") Integer receipt_id, @PathVariable("clinic_stock_id") Integer clinic_stock_id){
        boolean result = exportReceiptService.deleteExportReceiptDetail(receipt_id,clinic_stock_id);
        if (result){
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Đã xóa thành công", "")
            );
        } else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không được xóa", "")
            );
        }
    }

    @PutMapping("/detail")
    public ResponseEntity<ResponseObject> updateExportReceiptDetail(@RequestBody ExportReceiptDetailsQueryDto dto){
        ExportReceiptDetailsQueryDto result = exportReceiptService.updateExportReceiptDetail(dto);
        if(result == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Sửa không thành công", "")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(200,  "Sửa thành công", result)
        );

    }

    @PutMapping("/restore/{receipt_id}")
    public ResponseEntity<ResponseObject> restoreExportReceipt(@PathVariable("receipt_id") Integer id){
        boolean result = exportReceiptService.restoreExportReceipt(id);
        if (result){
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Đã khôi phục thành công", "")
            );
        } else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không khôi phục được. Hoặc đã có sẵn", "")
            );
        }
    }



    @PutMapping("/lock/{receipt_id}")
    public ResponseEntity<ResponseObject> lockExportReceipt(@PathVariable("receipt_id") Integer id){
        boolean result = exportReceiptService.lockExportReceipt(id);
        if(result){
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Đã khóa phiếu", "")
            );
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                new ResponseObject(400, "Khóa phiếu không thành công, phiếu đã được khóa", "")
        );
    }

    //Use to Pageable improve performance
    @GetMapping("page")
    public ExportReceiptReponse getAllImportRecepit(
            Principal principal,
            @RequestParam(value = "sortBy", defaultValue = Constants.DEFAULT_SORT_BY, required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = Constants.DEFAULT_SORT_DIRECTION, required = false) String sortDir,
            @RequestParam(value = "current", defaultValue = Constants.DEFAULT_SORT_DIRECTION, required = false) int pageNo ,
            @RequestParam(value = "pageSize", defaultValue = Constants.DEFAULT_SORT_DIRECTION, required = false) int pageSize) {

        ExportReceiptReponse exportRecepitReponse = new ExportReceiptReponse();
        String email= principal.getName();
        UserDto u = userService.findByEmail(email);
        int clinic_id = u.getClinicDto().getId();// lay clinic theo tai khoan
        String role = u.getClinicDto().getType().toString();//lay the loai cua kho
        //kiem tra xem la kho tong hay kho chi nhanh
        if(role == "DEPOT"){
            exportRecepitReponse = exportReceiptService.getAllIExportReceipt(pageNo, pageSize, sortBy, sortDir);
        } else {
            exportRecepitReponse = exportReceiptService.getAllExportReceiptClinic(pageNo, pageSize, sortBy, sortDir, clinic_id);
        }
        if (exportRecepitReponse.getData().isEmpty()){
            return null;
        }
        return exportRecepitReponse;
    }
}
