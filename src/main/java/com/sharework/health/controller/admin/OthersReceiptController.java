package com.sharework.health.controller.admin;

import com.sharework.health.dto.*;
import com.sharework.health.repository.OthersWarehouseReceiptRepository;
import com.sharework.health.service.OthersWarehouseReceiptService;
import com.sharework.health.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/admin/otherreceipts")
@AllArgsConstructor
public class OthersReceiptController {

    private OthersWarehouseReceiptService othersWarehouseReceiptService;
    private UserService userService;
    @GetMapping("")
    public ResponseEntity<ResponseObject> getAllImportReceipt(Principal principal){
        List<OthersWarehouseReceiptDto> othersWarehouseReceiptDtos = null; //khoi tao danh sach la rong
        String email= principal.getName();
        UserDto u = userService.findByEmail(email);
        int clinic_id = u.getClinicDto().getId();// lay clinic theo tai khoan
        String role = u.getClinicDto().getType().toString();//lay the loai cua kho

        //kiem tra xem user la thuoc kho nao
        if (role == "DEPOT") {
            othersWarehouseReceiptDtos = othersWarehouseReceiptService.findAll();
            if(othersWarehouseReceiptDtos.isEmpty()){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ResponseObject(400, "Không có dữ liệu", "")
                );
            }
        }
        if (role == "BRANCH"){
            othersWarehouseReceiptDtos = othersWarehouseReceiptService.getAllOtherReceiptClinicDetailByReceipt(clinic_id);
            if(othersWarehouseReceiptDtos.isEmpty()){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new ResponseObject(400, "Không có dữ liệu", "")
                );
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(200,  "Truy vấn thành công", othersWarehouseReceiptDtos)
        );
    }


    @GetMapping("/{receipt_id}")
    public ResponseEntity<ResponseObject> getOtherReceiptById(@PathVariable("receipt_id") Integer id){
        OthersWarehouseReceiptDto dto = othersWarehouseReceiptService.findById(id);
        if (dto!=null)
        {
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Truy vấn thành công", dto)
            );

        } else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject(404, "Không tìm thấy", "")
            );
    }



    @PostMapping("")
    public ResponseEntity<ResponseObject> addOtherReceipt(@RequestBody OthersWarehouseReceiptDto dto ){
        if (dto == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không có dữ liệu", "")
            );
        }
        OthersWarehouseReceiptDto result = othersWarehouseReceiptService.insertOtherReceipt(dto);
        if(result == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Thêm không thành công", "")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(200,  "Thêm phiếu thành công", result)
        );
    }

    @PostMapping("/detail")
    public ResponseEntity<ResponseObject> addOtherReceiptDetail(@RequestBody OthersWarehouseReceiptDetailsDto dto){
        if (dto == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không có dữ liệu", "")
            );
        }
        OthersWarehouseReceiptDetailsDto result = othersWarehouseReceiptService.insertOtherReceiptDetail(dto);
        if(result == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Thêm không thành công", "")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(200,  "Thêm phiếu thành công", result)
        );
    }

    @GetMapping("/detail/{receipt_id}")
    public ResponseEntity<ResponseObject> getOtherReceiptDetails(@PathVariable("receipt_id") Integer id){
        List<OtherReceiptDetailsQueryDto> dto = othersWarehouseReceiptService.getAllOtherReceiptDetailByReceiptid(id);
        if (dto.isEmpty())
        {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject(404, "Không tìm thấy", "")
            );
        } else
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Truy vấn thành công", dto)
            );

    }


    @DeleteMapping("{receipt_id}")
    public ResponseEntity<ResponseObject> deleteOtherReceipt(@PathVariable("receipt_id") Integer id){
        boolean result = othersWarehouseReceiptService.deleteOtherReceipt(id);
        if (result){
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Đã xóa thành công", "")
            );
        } else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không được xóa", "")
            );
        }
    }

    @DeleteMapping("/detail/{receipt_id}/{clinic_stock_id}")
    public ResponseEntity<ResponseObject> deleteOtherReceiptDetail(@PathVariable("receipt_id") Integer receipt_id, @PathVariable("clinic_stock_id") Integer clinic_stock_id){
        boolean result = othersWarehouseReceiptService.deleteOtherReceiptDetail(receipt_id,clinic_stock_id);
        if (result){
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Đã xóa thành công", "")
            );
        } else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không được xóa", "")
            );
        }
    }


    @PutMapping("/detail")
    public ResponseEntity<ResponseObject> updateOtherReceiptDetail(@RequestBody OtherReceiptDetailsQueryDto dto){
        OtherReceiptDetailsQueryDto result = othersWarehouseReceiptService.updateOtherReceiptDetail(dto);
        if(result == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Sửa không thành công", "")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(200,  "Sửa thành công", result)
        );

    }


    @PutMapping("/restore/{receipt_id}")
    public ResponseEntity<ResponseObject> restoreOtherReceipt(@PathVariable("receipt_id") Integer id){
        boolean result = othersWarehouseReceiptService.restoreOtherReceipt(id);
        if (result){
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Đã khôi phục thành công", "")
            );
        } else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không khôi phục được. Hoặc đã có sẵn", "")
            );
        }
    }



    @PutMapping("/lock/{receipt_id}")
    public ResponseEntity<ResponseObject> lockImportReceipt(@PathVariable("receipt_id") Integer id){
        boolean result = othersWarehouseReceiptService.lockOtherReceipt(id);
        if (result){
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Đã khóa phiếu", "")
            );
        } else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Phiếu đã được khóa hoặc đã xóa", "")
            );
        }
    }




}
