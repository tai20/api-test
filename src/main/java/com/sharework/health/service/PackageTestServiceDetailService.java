package com.sharework.health.service;

import com.sharework.health.dto.PackageTestServiceDetailDto;

import java.util.List;

public interface PackageTestServiceDetailService extends BaseService<PackageTestServiceDetailDto, Integer> {
    List<PackageTestServiceDetailDto> getPackageTestDetalByPackageId(Integer packagetest_id);

}
