package com.sharework.health.service.impl;

import com.sharework.health.convert.SlipUseConvert;
import com.sharework.health.convert.SlipUseDetailConvert;
import com.sharework.health.dto.SlipUseDetailDto;
import com.sharework.health.dto.SlipUseDto;
import com.sharework.health.entity.*;
import com.sharework.health.repository.*;
import com.sharework.health.service.SlipUseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class SlipUseServiceImpl implements SlipUseService {

    private static final Path CURRENT_FOLDER = Paths.get(System.getProperty("user.dir"));

    private SlipUseRepository slipUseRepository;

    private SlipUseConvert slipUseConvert;

    private TreatmentPackageRepository treatmentPackageRepository;

    private CustomerRepository customerRepository;

    private UserRepository userRepository;

    private SlipUseDetailRepository slipUseDetailRepository;

    private SlipUseDetailConvert slipUseDetailConvert;

    @Override
    public List<SlipUseDto> findAll() {
        List<SlipUse> slipUses = slipUseRepository.findAll();
        List<SlipUseDto> slipUseDtos = new ArrayList<>();

        for (SlipUse entity: slipUses
             ) {
            if (entity.getEndDate().compareTo(LocalDate.now()) < 0){
                entity.setStatus(Status.DEACTIVE);
                slipUseRepository.save(entity);
            }
            SlipUseDto dto = slipUseConvert.entityToDto(entity);
            slipUseDtos.add(dto);
        }
        return slipUseDtos;
    }

    @Override
    public SlipUseDto findById(Integer id) {
        SlipUse entity = slipUseRepository.findById(id).get();
        if (entity == null){
            return null;
        }
        return slipUseConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(SlipUseDto dto) {
        if (dto == null){
            return false;
        }

        try {
            TreatmentPackage treatmentPackage = treatmentPackageRepository.findById(dto.getTreatmentPackageDto().getId()).get();
            Customer customer = customerRepository.findById(dto.getCustomerDto().getId()).get();
            if (treatmentPackage == null){
                return false;
            }
            if (customer == null){
                return false;
            }
            SlipUse entity = slipUseConvert.dtoToEntity(dto);
            entity.setTreatmentPackage(treatmentPackage);
            entity.setCustomer(customer);
            entity.setStatus(Status.ACTIVE);
            slipUseRepository.save(entity);
            return true;

        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(Integer id, SlipUseDto dto) {
        if (dto == null){
            return false;
        }
        SlipUse entity = slipUseRepository.findById(id).get();
        try {
            //TreatmentPackage treatmentPackage = treatmentPackageRepository.findById(dto.getTreatmentPackageDto().getId()).get();
            //Customer customer = customerRepository.findById(dto.getCustomerDto().getId()).get();
//            if (treatmentPackage == null){
//                return false;
//            }
//            if (customer == null){
//                return false;
//            }
            //entity.setStartDate(dto.getStartDate());
            if (dto.getEndDate() != null){
                entity.setEndDate(dto.getEndDate());
            }

            //entity.setTreatmentPackage(treatmentPackage);
            //entity.setCustomer(customer);
            slipUseRepository.save(entity);
            return true;

        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        SlipUse entity = slipUseRepository.findById(id).get();
        if (entity == null){
            return false;
        }
        try {
            entity.setStatus(Status.DEACTIVE);
            slipUseRepository.save(entity);
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<SlipUseDto> findAllByCustomer(Integer customerId) {
        Customer customer = customerRepository.findById(customerId).get();
        if (customer == null){
            return null;
        }
        List<SlipUse> slipUses = slipUseRepository.findAllByCustomer(customer);
        List<SlipUseDto> slipUseDtos = new ArrayList<>();

        for (SlipUse entity: slipUses
             ) {
            if (entity.getStatus() == Status.ACTIVE){
                SlipUseDto dto = slipUseConvert.entityToDto(entity);
                slipUseDtos.add(dto);
            }
        }
        return slipUseDtos;
    }

    @Override
    public List<SlipUseDto> findAllByTreatmentPackage(String treatmentPackageName) {
        TreatmentPackage treatmentPackage = treatmentPackageRepository.findByName(treatmentPackageName);
        if (treatmentPackage == null){
            return null;
        }
        List<SlipUse> slipUses = slipUseRepository.findAllByTreatmentPackage(treatmentPackage);
        List<SlipUseDto> slipUseDtos = new ArrayList<>();

        for (SlipUse entity: slipUses
        ) {
            SlipUseDto dto = slipUseConvert.entityToDto(entity);
            slipUseDtos.add(dto);
        }
        return slipUseDtos;
    }

    @Override
    public List<SlipUseDetailDto> getAllSlipUseDetailBySlipUse(Integer slipUseId) {
        List<SlipUseDetail> slipUseDetails = slipUseDetailRepository.findAllBySlipUse(slipUseId);
        List<SlipUseDetailDto> slipUseDetailDtos = new ArrayList<>();
        for (SlipUseDetail entity: slipUseDetails
             ) {
            SlipUseDetailDto dto = slipUseDetailConvert.entityToDto(entity);
            slipUseDetailDtos.add(dto);
        }
        return slipUseDetailDtos;
    }

    @Override
    public boolean uploadImageBeforeSlipUse(Integer slipUseId, MultipartFile imageBefore) throws IOException {
        SlipUse entity = slipUseRepository.findById(slipUseId).get();
        if (entity == null){
            return false;
        }

        Path staticPath = Paths.get("static");
        Path imagePath = Paths.get("images");

        if (!Files.exists(CURRENT_FOLDER.resolve(staticPath).resolve(imagePath))){
            Files.createDirectories(CURRENT_FOLDER.resolve(staticPath).resolve(imagePath));
        }
        if (imageBefore != null && entity.getImageBefore() == null){
            //Path file = CURRENT_FOLDER.resolve(staticPath).resolve(imagePath).resolve(imageBefore.getOriginalFilename());
            Path path = Paths.get("static/images/");
            try {
                InputStream inputStream = imageBefore.getInputStream();
                Files.copy(inputStream, path.resolve(imageBefore.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
                entity.setImageBefore(imageBefore.getOriginalFilename().toLowerCase());
                slipUseRepository.save(entity);
                return true;
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean uploadImageAfterSlipUse(Integer slipUseId, MultipartFile imageAfter) throws IOException {
        SlipUse entity = slipUseRepository.findById(slipUseId).get();
        if (entity == null){
            return false;
        }

        Path staticPath = Paths.get("static");
        Path imagePath = Paths.get("images");

        if (!Files.exists(CURRENT_FOLDER.resolve(staticPath).resolve(imagePath))){
            Files.createDirectories(CURRENT_FOLDER.resolve(staticPath).resolve(imagePath));
        }
        if (imageAfter != null && entity.getImageBefore() != null){
            //Path file = CURRENT_FOLDER.resolve(staticPath).resolve(imagePath).resolve(imageBefore.getOriginalFilename());
            Path path = Paths.get("static/images/");
            try {
                InputStream inputStream = imageAfter.getInputStream();
                Files.copy(inputStream, path.resolve(imageAfter.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
                entity.setImageAfter(imageAfter.getOriginalFilename().toLowerCase());
                slipUseRepository.save(entity);
                return true;
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean activeSlipUse(Integer slipUseId) {
        SlipUse entity = slipUseRepository.findById(slipUseId).get();
        if (entity != null && entity.getStatus() == Status.DEACTIVE){
            entity.setStatus(Status.ACTIVE);
            slipUseRepository.save(entity);
            return true;
        }
        return false;
    }


}
