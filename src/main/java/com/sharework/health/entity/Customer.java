package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "customer")
public class Customer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private String email;

    private LocalDate birthDate;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    private String address;

    private String cmnd;

    private String phoneNumber;

    private String skinStatus;

    private LocalDateTime createdDate;

    private String typeCustomer;

    private String customerResource;

    private String avatar;

    @ManyToOne
    @JoinColumn(name = "clinic_id")
    private Clinic clinic;

    @OneToMany(mappedBy = "customer")
    private List<SlipUse> slipUses;

    @ManyToOne
    @JoinColumn(name = "customer_category_id")
    private CustomerCategory customerCategory;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY)
    private List<Order> orders;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY)
    private List<Advisory> advisories;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY)
    private List<SlipReturnProduct> slipReturnProducts;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY)
    private List<FreeTrial> freeTrials;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY)
    private List<ScheduleExamination> scheduleExaminations;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY)
    private List<CustomerLevel> customerLevels;

}
