package com.sharework.health.convert;

import com.sharework.health.dto.TreatmentPackageDto;
import com.sharework.health.entity.TreatmentPackage;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class TreatmentPackageConvert implements BaseConvert<TreatmentPackage, TreatmentPackageDto> {
    @Override
    public TreatmentPackageDto entityToDto(TreatmentPackage entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, TreatmentPackageDto.class);
    }

    @Override
    public TreatmentPackage dtoToEntity(TreatmentPackageDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, TreatmentPackage.class);
    }
}
