package com.sharework.health.controller.admin;

import com.sharework.health.dto.GiftDetailDto;
import com.sharework.health.dto.GiftListDto;
import com.sharework.health.service.GiftService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/admin/giftlist")
@AllArgsConstructor
public class GiftListController {

    private GiftService giftService;

    @PostMapping("")
    public Object addGiftList(@RequestBody GiftListDto dto){
        if (dto == null){
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = giftService.addGiftList(dto);
        if (!result){
            return new ResponseEntity<>("Thêm quà tặng thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm quà tặng thành công", HttpStatus.OK);
    }

    @GetMapping("{gift_id}")
    public Object getdetailGiftByGiftId(@PathVariable("gift_id") Integer giftId){
        GiftDetailDto giftDetailDto = giftService.getDetalGiftDtosById(giftId);
        if (giftDetailDto==null){
            return new ResponseEntity<>("Hiện không có mã khuyến mã nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(giftDetailDto, HttpStatus.OK);
    }

    @PutMapping("{gift_id}")
    public Object updateGiftList(@PathVariable("gift_id") Integer giftId , @RequestBody GiftListDto dto){

        if (dto == null){
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }

        boolean result = giftService.updateGiftList(giftId, dto);
        if (!result){
            return new ResponseEntity<>("Cập nhập quà tặng thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhập quà tặng thành công", HttpStatus.OK);
    }

    @DeleteMapping("")
    public ResponseEntity<GiftListDto> deleteGiftListByGID(@RequestBody GiftListDto dto) {
        giftService.deleteGiftList(dto);
        return new ResponseEntity<>( HttpStatus.OK);
    }

}
