package com.sharework.health.service;

import com.sharework.health.dto.ImportReceiptDetailsDto;
import com.sharework.health.dto.ImportReceiptDetailsQueryDto;
import com.sharework.health.dto.ImportReceiptDto;
import com.sharework.health.entity.ImportReceiptDetails;
import com.sharework.health.response.ImportRecepitReponse;

import java.util.List;
import java.util.Optional;

public interface ImportReceiptService extends BaseService<ImportReceiptDto, Integer>{
//    List<ImportReceiptDetails> getAllByReceiptid(Integer receipt_id);
    public ImportReceiptDto insertImportReceipt(ImportReceiptDto dto);
    public ImportReceiptDetailsDto insertImportReceiptDetail(ImportReceiptDetailsDto dto);
    List<ImportReceiptDetailsQueryDto> getAllImportReceiptDetailByReceiptid(Integer receipt_id);
    public boolean deleteImportReceipt(Integer id);
    public boolean deleteImportReceiptDetail(Integer receipt_id, Integer clinic_stock_id);
    public ImportReceiptDetailsQueryDto updateImportReceiptDetail(ImportReceiptDetailsQueryDto dto);
    public boolean restoreImportReceipt(Integer id);
    public boolean lockImportReceipt(Integer id);

    //list tra ve all phieu nhap theo kho
    List<ImportReceiptDto> getAllImportReceiptClinicDetail(Integer clinic_id);

    List<ImportReceiptDto> findAllByIdClinic(Integer id);

    ImportRecepitReponse  getAllImportRecepit(int pageNo, int pageSize, String sortBy, String sortDir);
    ImportRecepitReponse  getAllImportRecepitClinic(int pageNo, int pageSize, String sortBy, String sortDir, int id);
}
