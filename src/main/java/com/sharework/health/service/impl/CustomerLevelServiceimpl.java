package com.sharework.health.service.impl;

import com.sharework.health.convert.CustomerConvert;
import com.sharework.health.convert.CustomerLevelConvert;
import com.sharework.health.dto.*;
import com.sharework.health.entity.*;
import com.sharework.health.repository.CustomerLevelRepository;
import com.sharework.health.repository.LevelRepository;
import com.sharework.health.service.CustomerLevelService;
import com.sharework.health.service.CustomerService;
import com.sharework.health.service.LevelService;
import com.sharework.health.service.OrderService;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class CustomerLevelServiceimpl implements CustomerLevelService {

    private LevelService levelService;

    //    private OrderService orderService;
//
    private CustomerService customerService;

    private LevelRepository levelRepository;

    private CustomerLevelRepository customerLevelRepository;


    private CustomerConvert customerConvert;
    private CustomerLevelConvert customerLevelConvert;

    @Override
    public List<CustomerLevelDto> findAll() {
        List<CustomerLevel> customerLevel = customerLevelRepository.findAll();
        List<CustomerLevelDto> customerLevelDtos = new ArrayList<>();
        for (CustomerLevel entity : customerLevel
        ) {
            CustomerLevelDto dto = customerLevelConvert.entityToDto(entity);
            customerLevelDtos.add(dto);
        }
        return customerLevelDtos;
    }

    @Override
    public CustomerLevelDto findById(Integer id) {

        return null;
    }

    @Override
    public boolean insert(CustomerLevelDto dto) {
//        Customer customer = customerRepository.findById(dto.getCustomer_id().getId().get());
        return true;
    }

    @Override
    public boolean update(Integer id, CustomerLevelDto dto) {
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    public void updateCustomerLevelByid(Integer customer_id, BigDecimal order_value_total) {
        CustomerLevel customerLevel = customerLevelRepository.findCustomerlevelById(customer_id);
        Customer newCustomer = customerConvert.dtoToEntity(customerService.findById(customer_id));

        LevelDto level = levelService.findById(customerLevel.getLevel().getId());
        BigDecimal newTotal = customerLevel.getTotal_order_value().add(order_value_total);

        if ( level.getMax_value().compareTo(newTotal) == -1 ) {
            customerService.deleteCustomerLevel(customer_id);
            Level newLevel = levelService.findNameCustomerLever(order_value_total);
            customerLevelRepository.save(new CustomerLevel(newCustomer, newLevel, order_value_total));
        } else {
            customerLevel.setTotal_order_value(newTotal);
            customerLevelRepository.save(customerLevel);
        }

    }

    public List<CustomerLevel> getListCustomerLevelById(Integer custemer_id) {
        return customerLevelRepository.findByCustomer_IdEquals(custemer_id);
    }

    @Override
    public void updateCustomerLevelByuid(Integer customer_id, BigDecimal order_value_total) {
        CustomerLevel customerLevel =  customerLevelRepository.findCustomerlevelById(customer_id);
        List<Level> levels = levelRepository.findAll();

        for (Level level : levels) {
            if(customerLevel.getTotal_order_value().compareTo(level.getMax_value()) < 0 || customerLevel.getTotal_order_value().compareTo(level.getMin_value()) >= 0){

            }
        }
    }


}
