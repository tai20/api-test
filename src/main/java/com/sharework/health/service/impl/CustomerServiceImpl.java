package com.sharework.health.service.impl;

import com.sharework.health.Constants;
import com.sharework.health.convert.CustomerConvert;
import com.sharework.health.convert.CustomerLevelConvert;
import com.sharework.health.convert.LevelConvert;
import com.sharework.health.dto.*;
import com.sharework.health.entity.*;
import com.sharework.health.repository.*;
import com.sharework.health.response.CustomerReponse;
import com.sharework.health.service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class CustomerServiceImpl implements CustomerService {

    private static final Path CURRENT_FOLDER = Paths.get(System.getProperty("user.dir"));

    private CustomerRepository customerRepository;

    private CustomerConvert customerConvert;

    private CustomerLevelConvert customerLevelConvert;

    private LevelConvert levelConvert;

    private CustomerCategoryRepository customerCategoryRepository;

    private RoleRepository roleRepository;

    private LevelRepository levelRepository;

    private ClinicRepository clinicRepository;

    private CustomerLevelRepository customerLevelRepository;

    private OrderRepository orderRepository;

    @Override
    public List<CustomerDto> findAll() {
        return null;
    }

    @Override
    public List<CustomerLevelDto> findAllByAdmin() {
        List<CustomerLevel> customerLevel= customerLevelRepository.findAll();
        List<CustomerLevelDto> customerLevelDtos = new ArrayList<>();

        for(CustomerLevel entity : customerLevel){
            CustomerDto customerDto = customerConvert.entityToDto(entity.getCustomer());
            LevelDto levelDto = levelConvert.entityToDto(entity.getLevel());
            CustomerLevelDto dto = customerLevelConvert.entityToDto(entity);
            dto.setCustomer_id(customerDto);
            dto.setLevel_id(levelDto);
            customerLevelDtos.add(dto);
        }
        return customerLevelDtos;
    }
    @Override
    public CustomerDto findById(Integer id) {
        Customer entity = customerRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return customerConvert.entityToDto(entity);
    }


    @Override
    public List<CustomerLevelDto> findByUID(Integer id) {
        CustomerLevel customer = customerLevelRepository.findCustomerByUID(id);
        CustomerLevelDto dto = customerLevelConvert.entityToDto(customer);
        List<CustomerLevelDto> customerLevelDtos = new ArrayList<>();
        CustomerDto customerDto = customerConvert.entityToDto(customer.getCustomer());
        LevelDto levelDto = levelConvert.entityToDto(customer.getLevel());

        dto.setCustomer_id(customerDto);
        dto.setLevel_id(levelDto);

        customerLevelDtos.add(dto);

//        List<CustomerLevel> customerLevel= customerLevelRepository.findAll();
//        List<CustomerLevelDto> customerLevelDtos = new ArrayList<>();
//
//        for(CustomerLevel entity : customerLevel){
//            System.err.println(entity.getCustomer().getId() + " " + id);
//            if(entity.getCustomer().getId() == id) {
//                CustomerDto customerDto = customerConvert.entityToDto(entity.getCustomer());
//                LevelDto levelDto = levelConvert.entityToDto(entity.getLevel());
//                CustomerLevelDto dto = customerLevelConvert.entityToDto(entity);
//                dto.setCustomer_id(customerDto);
//                dto.setLevel_id(levelDto);
//                customerLevelDtos.add(dto);
//            }
//        }
        return customerLevelDtos;
    }

    @Override
    public boolean update(Integer id, CustomerDto dto) {
        Customer entity = customerRepository.findById(id).get();
        Clinic clinic = clinicRepository.findById(dto.getClinicDto().getId()).get();
        if (dto == null) {
            return false;
        }

        entity.setEmail(dto.getEmail());
        if (dto.getGender().equalsIgnoreCase("Nam")){
            entity.setGender(Gender.Nam);
        }
        if (dto.getGender().equalsIgnoreCase("Nữ")){
            entity.setGender(Gender.Nữ);
        }
        entity.setName(dto.getName());
        entity.setBirthDate(dto.getBirthDate());
        entity.setAddress(dto.getAddress());
        entity.setCmnd(dto.getCmnd());
        entity.setSkinStatus(dto.getSkinStatus());
        entity.setPhoneNumber(dto.getPhoneNumber());
        entity.setCustomerResource(dto.getCustomerResource());
        entity.setClinic(clinic);
        entity.setTypeCustomer(dto.getTypeCustomer());

        customerRepository.save(entity);
        return true;
    }

    @Override
    public boolean delete(Integer id) {
        Customer entity = customerRepository.findById(id).get();
        try {
            customerRepository.save(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public CustomerDto findByPhoneNumber(String phoneNumber) {
        Customer entity = customerRepository.findByPhoneNumber(phoneNumber);
        if (entity == null) {
            return null;
        }
        return customerConvert.entityToDto(entity);
    }

    @Override
    public CustomerDto getProfile() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String email = ((UserDetails) principal).getUsername();

        Customer customer = customerRepository.findByEmail(email);
        return customerConvert.entityToDto(customer);
    }

    @Override
    public String changePassword(PasswordDto dto) {
        return null;
    }

    @Override
    public CustomerDto findCustomerAfterInsert() {
        Customer entity = customerRepository.findCustomerAfterInsert();
        if (entity == null){
            return null;
        }
        return customerConvert.entityToDto(entity);
    }

    @Override
    public List<CustomerDto> findAllByCustomerCategory(Integer customerCategoryId) {
        List<Customer> customers = customerRepository.findAllByCustomerCategory(customerCategoryId);
        List<CustomerDto> customerDtos = new ArrayList<>();
        for (Customer entity: customers
        ) {
            CustomerDto dto = customerConvert.entityToDto(entity);
            customerDtos.add(dto);
        }
        return customerDtos;
    }




    @Override
    public boolean insertCustomer( String name, String email,
                                   String birthDate, String gender,
                                   String address, String cmnd,
                                   String phone, String skinStatus,
                                   String customerResource, MultipartFile avatar,
                                   Integer clinicId, String typeCustomer) throws IOException {
        Customer entity = new Customer();
        if (name == null || phone == null || clinicId == null || gender == null || birthDate == null || customerResource == null ||
                typeCustomer == null || avatar == null){
            return false;
        }

        if (customerRepository.findByPhoneNumber(phone) == null && customerRepository.findByEmail(email) == null &&
                customerRepository.findByCmnd(cmnd) == null ){
            Clinic clinic = clinicRepository.findById(clinicId).get();
            entity.setId(null);
            entity.setName(name);
            entity.setGender(Gender.valueOf(gender));
            entity.setCreatedDate(LocalDateTime.now());
            entity.setCustomerCategory(customerCategoryRepository.findById(1).get());
            entity.setClinic(clinic);
            entity.setTypeCustomer(typeCustomer);
            entity.setCustomerResource(customerResource);
            entity.setPhoneNumber(phone);

            if(skinStatus.isEmpty() || skinStatus.equalsIgnoreCase("undefined")){
                entity.setSkinStatus(null);
            } else { entity.setSkinStatus(skinStatus);}

            if(cmnd.isEmpty() || cmnd.equalsIgnoreCase("undefined")){
                entity.setCmnd(null);
            } else { entity.setCmnd(cmnd);}

            if(email.isEmpty() || email.equalsIgnoreCase("undefined")){
                entity.setEmail(null);
            } else { entity.setEmail(email);}

            if(address.isEmpty() || address.equalsIgnoreCase("undefined")){
                entity.setAddress(null);
            } else { entity.setAddress(address);}

            entity.setBirthDate(LocalDate.parse(birthDate));

            Path staticPath = Paths.get("static");
            Path imagePath = Paths.get("avatarCustomer");
            if (!Files.exists(CURRENT_FOLDER.resolve(staticPath).resolve(imagePath))) {
                Files.createDirectories(CURRENT_FOLDER.resolve(staticPath).resolve(imagePath));
            }

            Path path = Paths.get("static/avatarCustomer/");
            InputStream inputStream = avatar.getInputStream();
            String fileName = avatar.getOriginalFilename();
            String suffixName=fileName.substring(fileName.lastIndexOf("."));
            fileName= UUID.randomUUID()+suffixName;
            Files.copy(inputStream, path.resolve(fileName), StandardCopyOption.REPLACE_EXISTING);
            entity.setAvatar(fileName);

            customerRepository.save(entity);
            return true;
        }
        return false;

    }

    @Override
    public boolean addCustomerLevel(CustomerLevelDto dto) {
        List<Level> lv = levelRepository.findAll();

        HashMap<Integer,BigDecimal> minvalue = new HashMap<Integer,BigDecimal>();
        for (Level l : lv) {
            minvalue.put(l.getId(), l.getMin_value());
        }

        ValueComparator bvc = new ValueComparator(minvalue);
        TreeMap<Integer,BigDecimal> sorted_map = new TreeMap<Integer,BigDecimal>(bvc);
        sorted_map.putAll(minvalue);

        Customer cus = customerRepository.findById(dto.getCustomer_id().getId()).get();
        Level lv1 = new Level();
        if (cus!= null){
            CustomerLevel entity = new CustomerLevel();
            entity.setCustomer(cus);
            entity.setTotal_order_value(sorted_map.firstEntry().getValue());
            entity.setLevel(lv1.setId(sorted_map.firstEntry().getKey()));
            customerLevelRepository.save(entity);
            return true;
        }
        return false;
    }

    @Override
    public boolean updateCustomerLevel(CustomerLevelDto dto) {
        List<Level> lv = levelRepository.findAll();

        HashMap<Integer,BigDecimal> minvalue = new HashMap<Integer,BigDecimal>();
        for (Level l : lv) {
            minvalue.put(l.getId(), l.getMin_value());
        }

        Customer cus = customerRepository.findById(dto.getCustomer_id().getId()).get();
        Level lv1 = levelRepository.findById(dto.getLevel_id().getId()).get();
        if(cus != null && lv1 != null){
            CustomerLevel entity = new CustomerLevel();
            entity.setCustomer(cus);
            for (HashMap.Entry<Integer,BigDecimal> entry : minvalue.entrySet()) {
                if(lv1.getId().equals(entry.getKey())){
                    entity.setTotal_order_value(entry.getValue());
                    entity.setLevel(lv1.setId(entry.getKey()));
                }
            }
            customerLevelRepository.save(entity);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteCustomerLevel(Integer id) {
        CustomerLevel entity = customerLevelRepository.findCustomerlevelById(id);
        try {
            customerLevelRepository.delete(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    @Override
    public boolean insert(CustomerDto dto){
        return false;
    }

    @Override
    public boolean updateAvatar(Integer id, MultipartFile avatar) throws IOException {
        Customer entity = customerRepository.findById(id).get();

        if (entity == null || avatar == null){
            return false;
        }
        else {
            Path staticPath = Paths.get("static");
            Path imagePath = Paths.get("avatarCustomer");
            if (!Files.exists(CURRENT_FOLDER.resolve(staticPath).resolve(imagePath))) {
                Files.createDirectories(CURRENT_FOLDER.resolve(staticPath).resolve(imagePath));
            }
            {
                Path path = Paths.get("static/avatarCustomer/");
                InputStream inputStream = avatar.getInputStream();
                //Tạo tên file ngẫu nhiên
                String fileName = avatar.getOriginalFilename();
                String suffixName=fileName.substring(fileName.lastIndexOf("."));
                fileName= UUID.randomUUID()+suffixName;
//                avatar.transferTo(dest);
                Files.copy(inputStream, path.resolve(fileName), StandardCopyOption.REPLACE_EXISTING);
                entity.setAvatar(fileName);
            }
        }
        customerRepository.save(entity);
        return true;
    }

    @Override
    public List<String> getListNotification() {
        LocalDate time = LocalDate.now();
        List<String> list = new ArrayList<>();
        List<Customer> customers = customerRepository.findAllByAppointments(time);
        List<Customer> birthDay = customerRepository.findAllByBirthDay(time.getMonthValue(), time.getDayOfMonth());
        if (birthDay != null) {
            for (Customer customer : birthDay) {
                list.add("Hôm nay " + time + " là sinh nhật của khách hàng " + customer.getName());
            }
        }
        if (customers != null) {
            for (Customer customer : birthDay) {
                list.add("Hôm nay " + time + " có lịch hẹn của khách hàng " + customer.getName());
            }
        }
        return list;
    }

    public List<RankCustomerDto>  findAllRankCustomer() {

        List<CustomerLevel> customerLevel = customerLevelRepository.findAll();
        List<RankCustomerDto> customerRank = new ArrayList<>();
        for (CustomerLevel entity : customerLevel
        ) {

            RankCustomerDto dto = new RankCustomerDto();
            CustomerDto convertDto = customerConvert.entityToDto(entity.getCustomer());
            dto.setCustomer(convertDto);
            dto.setNameRank(entity.getLevel().getName());
            customerRank.add(dto);
        }
        return customerRank;
    }

    @Override
    public CustomerDto getCustomerByIDSU(Integer id) {
        return customerConvert.entityToDto(customerRepository.findBySlipUses_IdEquals(id));
    }

    @Override
    public CustomerReponse getAllCustomer(int pageNo, int pageSize, String sortBy, String sortDir ) {

        Pageable pageable = PageRequest.of(pageNo-1, pageSize);

        Page<CustomerLevel> customers = customerLevelRepository.findAll(pageable);
        List<CustomerLevel> listOfCustomers = customers.getContent();

        List<CustomerLevelDto> customerLevelDtos = new ArrayList<>();

        for (CustomerLevel css : listOfCustomers
             ) {
            CustomerDto customerDto = customerConvert.entityToDto(css.getCustomer());
            LevelDto levelDto = levelConvert.entityToDto(css.getLevel());
            CustomerLevelDto dto = customerLevelConvert.entityToDto(css);
            dto.setCustomer_id(customerDto);
            dto.setLevel_id(levelDto);
            customerLevelDtos.add(dto);
        }

        List<CustomerLevelDto> content = customerLevelDtos.stream().map(post -> post).collect(Collectors.toList());

        CustomerReponse customerResponse = new CustomerReponse();
        customerResponse.setData(content);
        customerResponse.setCurrent(customers.getNumber());
        customerResponse.setPageSize(customers.getSize());
        customerResponse.setTotalElements(customers.getTotalElements());
        customerResponse.setTotalPages(customers.getTotalPages());
        customerResponse.setLast(customers.isLast());

        return customerResponse;
    }

    @Override
    public CustomerReponse findAllByType(int pageNo, int pageSize, String sortBy, String sortDir,String type) {

        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
                : Sort.by(sortBy).descending();

        Pageable pageable = PageRequest.of(pageNo-1, pageSize, sort);

        Page<CustomerLevel> customers = customerLevelRepository.findByType(pageable,type);
        List<CustomerLevel> listOfCustomers = customers.getContent();

        List<CustomerLevelDto> customerLevelDtos = new ArrayList<>();

        for (CustomerLevel css : listOfCustomers
        ) {
            CustomerDto customerDto = customerConvert.entityToDto(css.getCustomer());
            LevelDto levelDto = levelConvert.entityToDto(css.getLevel());
            CustomerLevelDto dto = customerLevelConvert.entityToDto(css);
            dto.setCustomer_id(customerDto);
            dto.setLevel_id(levelDto);
            customerLevelDtos.add(dto);
        }

        List<CustomerLevelDto> content = customerLevelDtos.stream().map(post -> post).collect(Collectors.toList());

        CustomerReponse customerResponse = new CustomerReponse();
        customerResponse.setData(content);
        customerResponse.setCurrent(customers.getNumber());
        customerResponse.setPageSize(customers.getSize());
        customerResponse.setTotalElements(customers.getTotalElements());
        customerResponse.setTotalPages(customers.getTotalPages());
        customerResponse.setLast(customers.isLast());

        return customerResponse;
    }

    @Override
    public CustomerReponse findByType(int pageNo, int pageSize, String sortBy, String sortDir, String type, String datafind) {
        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
                : Sort.by(sortBy).descending();

        Pageable pageable = PageRequest.of(pageNo-1, pageSize, sort);

        String result = datafind.replaceAll("[\"-+.^:,}{]","")
                .replaceAll(Constants.REMOVE_NAME,"")
                .replaceAll(Constants.REMOVE_PHONE,"");
        boolean isNum = false;
        isNum = result.matches("[0-9]+[\\.]?[0-9]*");
        Page<CustomerLevel> customers = null;
        if(isNum == false) {
            customers = customerLevelRepository.findByName(pageable,type,datafind);
        }
        if(isNum == true){
            customers = customerLevelRepository.findByPhone(pageable,type,datafind);
        }

        List<CustomerLevel> listOfCustomers = customers.getContent();

        List<CustomerLevelDto> customerLevelDtos = new ArrayList<>();

        for (CustomerLevel css : listOfCustomers
        ) {
            CustomerDto customerDto = customerConvert.entityToDto(css.getCustomer());
            LevelDto levelDto = levelConvert.entityToDto(css.getLevel());
            CustomerLevelDto dto = customerLevelConvert.entityToDto(css);
            dto.setCustomer_id(customerDto);
            dto.setLevel_id(levelDto);
            customerLevelDtos.add(dto);
        }

        List<CustomerLevelDto> content = customerLevelDtos.stream().map(post -> post).collect(Collectors.toList());

        CustomerReponse customerResponse = new CustomerReponse();
        customerResponse.setData(content);
        customerResponse.setCurrent(customers.getNumber());
        customerResponse.setPageSize(customers.getSize());
        customerResponse.setTotalElements(customers.getTotalElements());
        customerResponse.setTotalPages(customers.getTotalPages());
        customerResponse.setLast(customers.isLast());

        return customerResponse;
    }

    @Override
    public CustomerReponse findByTypeOfAdmin(int pageNo, int pageSize, String sortBy, String sortDir, String datafind) {

        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
                : Sort.by(sortBy).descending();

        Pageable pageable = PageRequest.of(pageNo-1, pageSize, sort);

        String result = datafind.replaceAll("[\"-+.^:,}{]","")
                .replaceAll(Constants.REMOVE_NAME,"")
                .replaceAll(Constants.REMOVE_PHONE,"");
        boolean isNum = false;
        isNum = result.matches("[0-9]+[\\.]?[0-9]*");
        Page<CustomerLevel> customers = null;

        if(isNum == false) {
            customers = customerLevelRepository.findAllByName(pageable,datafind);
        }
        if(isNum == true){
            customers = customerLevelRepository.findAllByPhone(pageable,datafind);
        }

        List<CustomerLevel> listOfCustomers = customers.getContent();

        List<CustomerLevelDto> customerLevelDtos = new ArrayList<>();

        for (CustomerLevel css : listOfCustomers
        ) {
            CustomerDto customerDto = customerConvert.entityToDto(css.getCustomer());
            LevelDto levelDto = levelConvert.entityToDto(css.getLevel());
            CustomerLevelDto dto = customerLevelConvert.entityToDto(css);
            dto.setCustomer_id(customerDto);
            dto.setLevel_id(levelDto);
            customerLevelDtos.add(dto);
        }

        List<CustomerLevelDto> content = customerLevelDtos.stream().map(post -> post).collect(Collectors.toList());

        CustomerReponse customerResponse = new CustomerReponse();
        customerResponse.setData(content);
        customerResponse.setCurrent(customers.getNumber());
        customerResponse.setPageSize(customers.getSize());
        customerResponse.setTotalElements(customers.getTotalElements());
        customerResponse.setTotalPages(customers.getTotalPages());
        customerResponse.setLast(customers.isLast());

        return customerResponse;
    }

    @Override
    public List<CustomerLevelDto> findAllByType(String type) {

        List<CustomerLevel> customerLevel= customerLevelRepository.findAll();
        List<CustomerLevelDto> customerLevelDtos = new ArrayList<>();

        for(CustomerLevel entity : customerLevel){
            if(entity.getCustomer().getTypeCustomer().equalsIgnoreCase(type)) {
                CustomerDto customerDto = customerConvert.entityToDto(entity.getCustomer());
                LevelDto levelDto = levelConvert.entityToDto(entity.getLevel());
                CustomerLevelDto dto = customerLevelConvert.entityToDto(entity);
                dto.setCustomer_id(customerDto);
                dto.setLevel_id(levelDto);
                customerLevelDtos.add(dto);
            }
        }

        return customerLevelDtos;
    }

}
class ValueComparator implements Comparator<Integer> {
    Map<Integer, BigDecimal> base;

    public ValueComparator(Map<Integer, BigDecimal> base) {
        this.base = base;
    }

    public int compare(Integer a, Integer b) {
        if (base.get(a).compareTo(base.get(b)) <= 0) {
            return -1;
        } else {
            return 1;
        } // returning 0 would merge keys
    }
}


