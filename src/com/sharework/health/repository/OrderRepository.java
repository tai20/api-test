package com.sharework.health.repository;

import com.sharework.health.dto.SelectCustomerIdDebtDto;
import com.sharework.health.dto.SelectCustomerIdTotalDto;
import com.sharework.health.entity.Customer;
import com.sharework.health.entity.Order;
import com.sharework.health.entity.User;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {
	
	//List<Order> findAllOrderWithNoOrderDetail();
	
	List<Order> findByOrderDate(LocalDateTime orderDate);
	
	List<Order> findByCustomer(Customer customer);
	
	List<Order> findByUser(User user);

	@Query(nativeQuery = true, value = "select o.* from orders o join users u on o.user_id = u.id join clinic c on c.id = u.clinic_id where c.id = :clinicId")
	List<Order> findByClinic(Integer clinicId);

	@Query(nativeQuery = true, value = "select * from orders where status like 'Examining' or status like 'Examined'")
	List<Order> findAllOrderWithExamination();

	@Query(nativeQuery = true, value = "select * from orders where status like 'Processing' or status like 'Confirmed' or status like 'Paying' or status like 'Paid'")
	List<Order> findAllOrderWithOrderDetails();

	@Query(nativeQuery = true, value = "select * from orders where orderdate between :startDate and :endDate")
	List<Order> statisticalOrderByOrderDateIsBetween(LocalDateTime startDate, LocalDateTime endDate);

	@Query(nativeQuery = true, value = "select count(o.id) from orders o join customer c on o.customer_id = c.id where c.id= :customerId")
	Integer countAllByCustomer(Integer customerId);

	@Query(nativeQuery = true, value = "select sum(o.total) from orders o join customer c on o.customer_id = c.id where c.id= :customerId")
	BigDecimal sumOrderTotalOfCustomer(Integer customerId);

	@Query(nativeQuery = true, value = "select * from orders order by id desc limit 1")
	Order getOrderAfterInsert();

//	@Query(nativeQuery = true, value = "select distinct customer_id as id, sum(debtMoney) as debtMoney from orders where extract(month from orderdate) = :month and extract(year from orderdate) = :year group by customer_id having sum(debtMoney) > 0")
//	List<SelectCustomerIdDebtDto> findAllCustomerHaveDebtByMonthOfOrderDate(Integer month, Integer year);

	@Query("select distinct new com.sharework.health.dto.SelectCustomerIdDebtDto(c.id, sum(o.debtMoney)) from Order o join Customer c on o.customer.id = c.id where MONTH(o.orderDate) = :month and YEAR(o.orderDate) = :year group by c.id having sum(o.debtMoney) > 0")
	List<SelectCustomerIdDebtDto> findAllCustomerHaveDebtByMonthOfOrderDate(Integer month, Integer year);

	@Query("select distinct new com.sharework.health.dto.SelectCustomerIdTotalDto(c.id, sum(o.total)) \n" +
			"from Order o join Customer c on o.customer.id = c.id \n" +
			"where MONTH(o.orderDate) = :month and YEAR(o.orderDate) = :year  group by c.id\n" +
			"having sum(o.total) > 300000000")
	List<SelectCustomerIdTotalDto> findAllCustomerHaveTotalByMonthOfOrderDate(Integer month, Integer year);


}
