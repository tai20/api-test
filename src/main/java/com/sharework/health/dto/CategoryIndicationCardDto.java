package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CategoryIndicationCardDto {

    private Integer id;

    private String name;

    private String description;

    private BigDecimal price;

    private SubCategoryIndicationCardDto subCategoryIndicationCardDto;

}
