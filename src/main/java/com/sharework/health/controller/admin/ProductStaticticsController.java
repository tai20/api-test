package com.sharework.health.controller.admin;


import com.sharework.health.dto.*;
import com.sharework.health.service.ProductStatisticsService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;


@RestController
@RequestMapping("/api/admin/statistics")
@AllArgsConstructor
public class ProductStaticticsController {


    private ProductStatisticsService productStatisticsService;


    @GetMapping("/remainingProducts")
    public ResponseEntity<ResponseObject> getRemainingProducts(){
        List<RemainingProductStatisticsQueryDto> dto = productStatisticsService.remainingProductStatistics();
        if (dto!=null)
        {
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Truy vấn thành công", dto)
            );
        } else {

            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject(404, "Không có dữ liệu", "")
            );

        }
    }

    @GetMapping("/getStatisticsImportedProducts")
    public ResponseEntity<ResponseObject> getStatisticsImportedProducts(@RequestParam("from") String from, @RequestParam("to") String to){
        LocalDateTime from_time = LocalDateTime.of(LocalDate.parse(from).getYear(), LocalDate.parse(from).getMonth(),LocalDate.parse(from).getDayOfMonth(), 0,0,0);
        LocalDateTime to_time =  LocalDateTime.of(LocalDate.parse(to).getYear(), LocalDate.parse(to).getMonth(),LocalDate.parse(to).getDayOfMonth(), 23,59,59);
        List<StatisticsImportedProductsQueryDto> dto = productStatisticsService.getStatisticsImportedProducts(from_time, to_time);
        if (dto!=null)
        {
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Truy vấn thành công", dto)
            );
        } else {

            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject(404, "Không có dữ liệu", "")
            );

        }

    }

    @GetMapping("/getStatisticsExportedProducts")
    public ResponseEntity<ResponseObject> getStatisticsExportedProducts(@RequestParam("from") String from, @RequestParam("to") String to){
        LocalDateTime from_time = LocalDateTime.of(LocalDate.parse(from).getYear(), LocalDate.parse(from).getMonth(),LocalDate.parse(from).getDayOfMonth(), 0,0,0);
        LocalDateTime to_time =  LocalDateTime.of(LocalDate.parse(to).getYear(), LocalDate.parse(to).getMonth(),LocalDate.parse(to).getDayOfMonth(), 23,59,59);
        List<StatisticsExportedProductsQueryDto> dto = productStatisticsService.getStatisticsExportedProducts(from_time, to_time);
        if (dto!=null)
        {
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Truy vấn thành công", dto)
            );
        } else {

            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject(404, "Không có dữ liệu", "")
            );

        }

    }

    @GetMapping("/getStatisticsOtherProducts")
    public ResponseEntity<ResponseObject> getStatisticsOtherProducts(@RequestParam("from") String from, @RequestParam("to") String to){
        LocalDateTime from_time = LocalDateTime.of(LocalDate.parse(from).getYear(), LocalDate.parse(from).getMonth(),LocalDate.parse(from).getDayOfMonth(), 0,0,0);
        LocalDateTime to_time =  LocalDateTime.of(LocalDate.parse(to).getYear(), LocalDate.parse(to).getMonth(),LocalDate.parse(to).getDayOfMonth(), 23,59,59);
        List<StatisticsOtherProductsQueryDto> dto = productStatisticsService.getStatisticsOtherProducts(from_time, to_time);
        if (dto!=null)
        {
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Truy vấn thành công", dto)
            );
        } else {

            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject(404, "Không có dữ liệu", "")
            );

        }

    }

    @GetMapping("/getStatisticsAllProducts")
    public ResponseEntity<ResponseObject> getStatisticsAllProducts(@RequestParam("from") String from, @RequestParam("to") String to){
        LocalDateTime from_time = LocalDateTime.of(LocalDate.parse(from).getYear(), LocalDate.parse(from).getMonth(),LocalDate.parse(from).getDayOfMonth(), 0,0,0);
        LocalDateTime to_time =  LocalDateTime.of(LocalDate.parse(to).getYear(), LocalDate.parse(to).getMonth(),LocalDate.parse(to).getDayOfMonth(), 23,59,59);
        List<StatisticsAllProductsQueryDto> dto = productStatisticsService.getStatisticsAllProducts(from_time, to_time);
        if (dto!=null)
        {
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Truy vấn thành công", dto)
            );
        } else {

            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject(404, "Không có dữ liệu", "")
            );

        }
    }
}
