package com.sharework.health.repository;

import com.sharework.health.entity.Gift;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GiftRepository extends JpaRepository<Gift, Integer> {

    Gift findByIdEquals(Integer id);

    @Query(nativeQuery = true, value = "select * from gift order by id desc limit 1")
    Gift  findGiftAfterInsert();

    @Query(nativeQuery = true, value = "select * from gift where quantity = :times")
    List<Gift> findByTimes(Integer times);

}
