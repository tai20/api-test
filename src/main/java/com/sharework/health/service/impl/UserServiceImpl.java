package com.sharework.health.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import com.sharework.health.dto.LoginDto;
import com.sharework.health.entity.*;
import com.sharework.health.helper.InitValue;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.sharework.health.convert.UserConvert;
import com.sharework.health.dto.PasswordDto;
import com.sharework.health.dto.UserDto;
import com.sharework.health.repository.ClinicRepository;
import com.sharework.health.repository.RoleRepository;
import com.sharework.health.repository.UserRepository;
import com.sharework.health.service.UserService;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    private UserConvert userConvert;

    private RoleRepository roleRepository;

    private ClinicRepository clinicRepository;

    private InitValue initValue;

    @Override
    public List<UserDto> findAll() {
        //List<User> users = userRepository.findAll();
        List<User> users = userRepository.findAllByStatusLikeActive();
        List<UserDto> userDtos = new ArrayList<>();
//        for (User entity: users) {
//            if(entity.getStatus() == Status.ACTIVE){
//                UserDto dto = userConvert.entityToDto(entity);
//                userDtos.add(dto);
//            }
//        }
        for (User entity: users) {
                UserDto dto = userConvert.entityToDto(entity);
                userDtos.add(dto);
        }
        return userDtos;
    }

    @Override
    public UserDto findById(Integer id) {
        User entity = userRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return userConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(UserDto dto) {
        if (dto == null) {
            return false;
        }
        if (userRepository.findByEmail(dto.getEmail()) == null) {
            Role role = roleRepository.findById(dto.getRoleDto().getId()).get();
            Clinic clinic = clinicRepository.findById(dto.getClinicDto().getId()).get();
            try {
                User user = userConvert.dtoToEntity(dto);
                user.setId(null);
                user.setStatus(Status.ACTIVE);
                if (role == null){
                    return false;
                }
                user.setRole(role);
                if (clinic == null){
                    return false;
                }
                user.setClinic(clinic);
                userRepository.save(user);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean update(Integer id, UserDto dto) {
        User entity = userRepository.findById(id).get();

        if (entity == null) {
            return false;
        }
        Role role = roleRepository.findById(dto.getRoleDto().getId()).get();
        Clinic clinic = clinicRepository.findById(dto.getClinicDto().getId()).get();

            entity.setEmail(dto.getEmail());
            entity.setName(dto.getName());
            entity.setAddress(dto.getAddress());
            entity.setBirthDate(dto.getBirthDate());
            entity.setPhoneNumber(dto.getPhoneNumber());
            entity.setCertificate(dto.getCertificate());
            entity.setWorkExperience(dto.getWorkExperience());
            if (dto.getGender().equalsIgnoreCase("Nam")){
                entity.setGender(Gender.Nam);
            }
            if (dto.getGender().equalsIgnoreCase("Nữ")){
                entity.setGender(Gender.Nữ);
            }
            entity.setCmnd(dto.getCmnd());
            entity.setRole(role);
            entity.setClinic(clinic);
            userRepository.save(entity);
            return true;
    }

    @Override
    public boolean delete(Integer id) {
        User entity = userRepository.findById(id).get();
        try {
            entity.setStatus(Status.DEACTIVE);
            userRepository.save(entity);
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<UserDto> searchUserByUsername(String name) {
        List<User> users = userRepository.findUserByNameContains(name);
        List<UserDto> userDtos = new ArrayList<>();
        for (User entity: users
             ) {
            if (entity.getStatus() == Status.ACTIVE){
                UserDto dto = userConvert.entityToDto(entity);
                userDtos.add(dto);
            }
        }
        return userDtos;
    }

	@Override
	public UserDto getProfile() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String email = ((UserDetails)principal).getUsername();

        User user = userRepository.findByEmail(email);
        return userConvert.entityToDto(user);

	}

    @Override
    public String getRoleName() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String email = ((UserDetails)principal).getUsername();
        User entity = userRepository.findByEmail(email);
        String roleName = entity.getRole().getName();
        return roleName;
    }

    @Override
	public String changePassword(PasswordDto dto) {
		if (dto.getNewPassword().equals(dto.getOldPassword())) {
			return "Mật khẩu cũ và mật khẩu mới không được trùng nhau";
		}
		
		User user = userRepository.findByEmail(dto.getEmail());
		// So mật khẩu
		if (!BCrypt.checkpw(dto.getOldPassword(), user.getPassword())) {
			return "Mật khẩu cũ không đúng";
		}
		String hashed = BCrypt.hashpw(dto.getNewPassword(), BCrypt.gensalt());
		
		// Cập nhật lại mật khẩu
		user.setPassword(hashed);
		userRepository.save(user);
		
		return "Cập nhật mật khẩu thành công";
	}

    @Override
    public LoginDto resetPassword(String email) {
        User user = userRepository.findByEmail(email);
        if (user == null){
            return null;
        }
        user.setPassword(null);
        userRepository.save(user);
        String generatepassword = initValue.generateCouponCode();
        String hashpassword = BCrypt.hashpw(generatepassword, BCrypt.gensalt());
        user.setPassword(hashpassword);
        userRepository.save(user);
        return new LoginDto(email,generatepassword);
    }

    @Override
    public LoginDto showLoginWhenAddUser(UserDto dto) {
        if (dto == null) {
            return null;
        }
        if (userRepository.findByEmail(dto.getEmail()) == null) {
            Role role = roleRepository.findById(dto.getRoleDto().getId()).get();
            Clinic clinic = clinicRepository.findById(dto.getClinicDto().getId()).get();
            try {
                String generatepassword = initValue.generateCouponCode();
                String hashpassword = BCrypt.hashpw(generatepassword, BCrypt.gensalt());
                User user = userConvert.dtoToEntity(dto);
                user.setId(null);
                user.setPassword(hashpassword);
                user.setStatus(Status.ACTIVE);
                if (role == null){
                    return null;
                }
                user.setRole(role);
                if (clinic == null){
                    user.setClinic(null);
                }
                user.setClinic(clinic);
                LoginDto loginDto = new LoginDto()
                        .setEmail(dto.getEmail())
                        .setPassword(generatepassword);
                userRepository.save(user);
                return loginDto;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public UserDto findByEmail(String email) {
        User entity = userRepository.findByEmail(email);
        if (entity == null){
            return null;
        }
        return userConvert.entityToDto(entity);
    }

    @Override
    public boolean updateProfile(UserDto dto) {
        User entity = userRepository.findById(dto.getId()).get();
        if (entity == null){
            return false;
        }
        entity.setName(dto.getName());
        entity.setBirthDate(dto.getBirthDate());
        if (dto.getGender().equalsIgnoreCase("Nam")){
            entity.setGender(Gender.Nam);
        }
        if (dto.getGender().equalsIgnoreCase("Nữ")){
            entity.setGender(Gender.Nữ);
        }
        entity.setAddress(dto.getAddress());
        entity.setPhoneNumber(dto.getPhoneNumber());
        entity.setCertificate(dto.getCertificate());
        entity.setWorkExperience(dto.getWorkExperience());
        entity.setCmnd(dto.getCmnd());
        userRepository.save(entity);
        return true;
    }


}
