package com.sharework.health.convert;

import com.sharework.health.dto.ClinicDto;
import com.sharework.health.dto.ProductCategoryDto;
import com.sharework.health.dto.ProductDto;
import com.sharework.health.entity.Product;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ProductConvert implements BaseConvert<Product, ProductDto> {
    @Override
    public ProductDto entityToDto(Product entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, ProductDto.class)
                .setProductCategoryDto(modelMapper.map(entity.getProductCategory(), ProductCategoryDto.class));
                //.setClinicDto(modelMapper.map(entity.getClinic(), ClinicDto.class));
    }

    @Override
    public Product dtoToEntity(ProductDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, Product.class);
    }
}
