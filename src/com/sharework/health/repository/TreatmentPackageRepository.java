package com.sharework.health.repository;

import com.sharework.health.entity.TreatmentPackage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TreatmentPackageRepository extends JpaRepository<TreatmentPackage, Integer> {

    TreatmentPackage findByName(String name);

    @Query(nativeQuery = true, value = "select * from treatmentpackage order by id desc limit 1")
    TreatmentPackage findTreatmentPackageAfterInsert();
}
