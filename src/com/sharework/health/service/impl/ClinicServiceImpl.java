package com.sharework.health.service.impl;

import com.sharework.health.convert.ClinicConvert;
import com.sharework.health.dto.ClinicDto;
import com.sharework.health.entity.*;
import com.sharework.health.repository.ClinicRepository;
import com.sharework.health.repository.SupplierRepository;
import com.sharework.health.service.ClinicService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class ClinicServiceImpl implements ClinicService {

    private ClinicRepository clinicRepository;

    private ClinicConvert clinicConvert;

    private SupplierRepository supplierRepository;

    @Override
    public List<ClinicDto> findAll() {
        List<Clinic> clinics = clinicRepository.findAll();
        List<ClinicDto> clinicDtos = new ArrayList<>();
        for (Clinic entity: clinics) {
            ClinicDto dto = clinicConvert.entityToDto(entity);
            clinicDtos.add(dto);
        }
        return clinicDtos;
    }

    @Override
    public ClinicDto findById(Integer id) {
        Clinic entity = clinicRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return clinicConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(ClinicDto dto) {
        if (dto == null) {
            return false;
        }
        Clinic entity = clinicConvert.dtoToEntity(dto);
        entity.setId(null);
        entity.setStatus(Status.ACTIVE);
        clinicRepository.save(entity);

        Supplier supplier = new Supplier()
                .setName(dto.getName())
                .setStatus(Status.ACTIVE)
                .setType(SupplierType.Clinic)
                .setAddress(dto.getAddress())
                ;
        supplierRepository.save(supplier);
        return true;

    }

    @Override
    public boolean update(Integer id, ClinicDto dto) {
        Clinic entity = clinicRepository.findById(id).get();
        if (entity == null) {
            return false;
        }
        if (dto == null){
            return false;
        }
        entity.setName(dto.getName());
        entity.setAddress(dto.getAddress());
        clinicRepository.save(entity);
        return true;
    }

    @Override
    public boolean delete(Integer id) {
        Clinic entity = clinicRepository.findById(id).get();
        if (entity == null) {
            return false;
        }
        entity.setStatus(Status.ACTIVE);
        clinicRepository.save(entity);
        return true;
    }

    @Override
    public ClinicDto searchClinicByClinicName(String clinicName) {
        Clinic entity = clinicRepository.findByName(clinicName);
        if (entity == null){
            return null;
        }
        return clinicConvert.entityToDto(entity);
    }
}
