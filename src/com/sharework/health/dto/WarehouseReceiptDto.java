package com.sharework.health.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class WarehouseReceiptDto {

    private Integer id;

    private LocalDate dateimport;

    private String status;

    private String receiptType;

    private ClinicDto clinicDto;

    private SupplierDto supplierDto;

    private UserDto userDto;

    @JsonIgnore
    private List<WarehouseReceiptDetailDto> warehouseReceiptDetailDtos;
}
