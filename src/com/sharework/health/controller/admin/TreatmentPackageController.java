package com.sharework.health.controller.admin;

import com.sharework.health.dto.TreatmentPackageAfterInsertDto;
import com.sharework.health.dto.TreatmentPackageDto;
import com.sharework.health.dto.TreatmentPackageServiceDto;
import com.sharework.health.service.TreatmentPackageService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/treatmentpackages")
@AllArgsConstructor
public class TreatmentPackageController {

    private TreatmentPackageService treatmentPackageService;

    @GetMapping("")
    public ResponseEntity<List<TreatmentPackageDto>> getAllTreatmentPackage() {
        List<TreatmentPackageDto> treatmentPackageDtos = treatmentPackageService.findAll();
        if (treatmentPackageDtos.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(treatmentPackageDtos,HttpStatus.OK);
    }

    @PostMapping("")
    public Object addTreatmentPackage(@RequestBody TreatmentPackageDto dto,
                                           BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        boolean result = treatmentPackageService.insert(dto);
        if (!result){
            return new ResponseEntity<>("Thêm không thành công", HttpStatus.BAD_REQUEST);
        }
        TreatmentPackageDto treatmentPackageDto = treatmentPackageService.findTreatmentPackageAfterInsert();

        TreatmentPackageAfterInsertDto treatmentPackageAfterInsertDto = new TreatmentPackageAfterInsertDto()
                .setMessage("Thêm gói thành công")
                .setTreatmentPackageDto(treatmentPackageDto);
        return new ResponseEntity<>(treatmentPackageAfterInsertDto,HttpStatus.CREATED);
    }

    @PutMapping("/{treatmentpackage_id}")
    public ResponseEntity<TreatmentPackageDto> updateTreatmentPackage(@PathVariable("treatmentpackage_id") Integer id , @RequestBody TreatmentPackageDto dto,
                                                  BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        treatmentPackageService.update(id, dto);
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }

    @DeleteMapping("/{treatmentpackage_id}")
    public ResponseEntity<TreatmentPackageDto> deleteTreatmentPackage(@PathVariable("treatmentpackage_id") Integer id) {
        treatmentPackageService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("searchName")
    public Object searchTreatmentPackageByName(@RequestParam("treatmentpackage_name") String name) {
        TreatmentPackageDto dto = treatmentPackageService.searchTreatmentPackageByName(name);
        if (dto == null){
            return new ResponseEntity<>("Không tìm thấy gói điều trị cần tìm",HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }

    @PostMapping("addTreatmentPackageService")
    public Object addTreatmentPackageService(@RequestBody TreatmentPackageServiceDto dto) {
        boolean result = treatmentPackageService.addTreatmentPackageService(dto);
        if (!result){
            return new ResponseEntity<>("Bị lỗi trùng dòng trong bảng hoặc không tìm thấy dữ liệu nào",HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Tạo 1 gói dịch vụ thành công",HttpStatus.CREATED);
    }

    @GetMapping("getTreatmentPackageServiceByTreatmentPackage/{treatmentpackage_id}")
    public Object getTreatmentPackageServiceByTreatmentPackage(@PathVariable("treatmentpackage_id") Integer treatmentpackageId) {
        List<TreatmentPackageServiceDto> treatmentPackageServiceDtos = treatmentPackageService.findAllByTreatmentPackage(treatmentpackageId);
        if (treatmentPackageServiceDtos.isEmpty() || treatmentPackageServiceDtos == null){
            return new ResponseEntity<>("Gói dịch vụ chưa có dịch vụ",HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(treatmentPackageServiceDtos,HttpStatus.OK);
    }
}
