package com.sharework.health.repository;

import com.sharework.health.entity.OrderDetailPackageTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDetailPackageTestRepository extends JpaRepository<OrderDetailPackageTest, Integer> {
    @Query(nativeQuery = true, value = "select a.* from orderdetailpackagetest a join orders c on a.order_id = c.id where c.id= :orderId")
    List<OrderDetailPackageTest> getOrderDetailPackageTestByOrderId(Integer orderId);
}
