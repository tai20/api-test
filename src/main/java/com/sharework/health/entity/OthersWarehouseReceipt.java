package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "otherswarehousereceipt")
public class OthersWarehouseReceipt implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "clinic_id")
    private Clinic clinic;

    @Column
    private Boolean type;

    private LocalDateTime datecreate;

    @Column
    private String content;

    @Enumerated(EnumType.STRING)
    private ReceiptStatus status;

    @OneToMany(mappedBy = "othersWarehouseReceipt")
    private List<OthersWarehouseReceiptDetails> othersWarehouseReceiptDetails;
}
