package com.sharework.health.service;

import com.sharework.health.dto.TreatmentPackageDto;
import com.sharework.health.dto.TreatmentPackageServiceDto;
import com.sharework.health.response.TreatmentPackageReponse;

import java.util.List;

public interface TreatmentPackageService extends BaseService<TreatmentPackageDto, Integer> {

    TreatmentPackageDto searchTreatmentPackageByName(String name);

    boolean addTreatmentPackageService(TreatmentPackageServiceDto dto);

    List<TreatmentPackageServiceDto> findAllByTreatmentPackage(Integer treatmentPackageId);

    TreatmentPackageDto findTreatmentPackageAfterInsert();

    List<TreatmentPackageServiceDto> findAllByCustomerId(Integer customerId);

    List<TreatmentPackageDto> findByCustomerId(Integer customerId);

    TreatmentPackageReponse getAllTreatmentPackage(int pageNo, int pageSize, String sortBy, String sortDir);
    TreatmentPackageReponse getAllTreatmentPackageByInput(int pageNo, int pageSize, String sortBy, String sortDir, String input);
}
