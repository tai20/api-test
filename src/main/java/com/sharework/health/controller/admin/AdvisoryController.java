package com.sharework.health.controller.admin;

import com.sharework.health.dto.AdvisoryDto;
import com.sharework.health.service.AdvisoryService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/advisories")
@AllArgsConstructor
public class AdvisoryController {

    private AdvisoryService advisoryService;

    @GetMapping("")
    public Object getAllAdvisory(){
        List<AdvisoryDto> advisoryDtos = advisoryService.findAll();
        if (advisoryDtos.isEmpty() || advisoryDtos == null){
            return new ResponseEntity<>("Không có tư vấn nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(advisoryDtos, HttpStatus.OK);
    }

    @GetMapping("/{customer_id}")
    public Object getAllAdvisoryByCustomer(@PathVariable("customer_id") Integer customerId){
        List<AdvisoryDto> advisoryDtos = advisoryService.findAllByCustomer(customerId);
        if (advisoryDtos.isEmpty() || advisoryDtos == null){
            return new ResponseEntity<>("Khách hàng này không có tư vấn nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(advisoryDtos, HttpStatus.OK);
    }

    @PostMapping("")
    public Object addAdvisory(@RequestBody AdvisoryDto dto){
        if (dto == null){
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = advisoryService.insert(dto);
        if (!result){
            return new ResponseEntity<>("Thêm tư vấn thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm tư vấn thành công", HttpStatus.CREATED);
    }

    @PutMapping("{advisory_id}")
    public Object updateAdvisory(@PathVariable("advisory_id") Integer advisoryId, @RequestBody AdvisoryDto dto){
        if (dto.getCustomerDto() == null || dto.getUserDto() == null){
            return new ResponseEntity<>("Thiếu thông tin nhân viên hoặc khách hàng", HttpStatus.BAD_REQUEST);
        }
        boolean result = advisoryService.update(advisoryId, dto);
        if (!result){
            return new ResponseEntity<>("Cập nhật tư vấn thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhật tư vấn thành công", HttpStatus.OK);
    }

    @DeleteMapping("{advisory_id}")
    public Object deleteAdvisory(@PathVariable("advisory_id") Integer advisoryId){

        boolean result = advisoryService.delete(advisoryId);
        if (!result){
            return new ResponseEntity<>("Xóa tư vấn thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Xóa tư vấn thành công", HttpStatus.OK);
    }
}
