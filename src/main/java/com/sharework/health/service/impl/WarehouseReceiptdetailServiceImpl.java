package com.sharework.health.service.impl;

import com.sharework.health.convert.ProductConvert;
import com.sharework.health.convert.WarehouseReceiptConvert;
import com.sharework.health.convert.WarehouseReceiptDetailConvert;
import com.sharework.health.dto.WarehouseReceiptDetailDto;
import com.sharework.health.dto.WarehouseReceiptDto;
import com.sharework.health.entity.*;
import com.sharework.health.repository.*;
import com.sharework.health.service.ProductService;
import com.sharework.health.service.WarehouseReceiptDetailInfo;
import com.sharework.health.service.WarehouseReceiptService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;


@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class WarehouseReceiptdetailServiceImpl implements WarehouseReceiptDetailInfo {
    private WarehouseReceiptDetailRepository warehouseReceiptDetailRepository;

    private ProductService productService;

    private ProductConvert productConvert;

    private  WarehouseReceiptConvert warehouseReceiptConvert;

    private WarehouseReceiptService warehouseReceiptService;
    @Override
    public int getNumberProductImport() {
        return 0;
    }

    @Override
    public ProductInfo getProduct() {
        return null;
    }

    @Override
    public WarehouseReceiptInfo getWarehouseReceipt() {
        return null;
    }

    public boolean update( Integer num, Integer pid, Integer rid) {
        Product product =productConvert.dtoToEntity( productService.findById(pid));
        WarehouseReceipt receipt = warehouseReceiptConvert.dtoToEntity(warehouseReceiptService.findById(rid));
        warehouseReceiptDetailRepository.updateNumberProductImportByProductEqualsAndWarehouseReceiptEquals(num,product,receipt );
        return true;
    }
}
