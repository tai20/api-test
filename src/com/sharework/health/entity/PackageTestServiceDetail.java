package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;


@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "packagetestservicedetail")
public class PackageTestServiceDetail implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "packagetest_id")
    private PackageTest packageTest;

    @ManyToOne
    @JoinColumn(name = "categoryindicatoncard_id")
    private CategoryIndicationCard categoryIndicationCard;

    private Integer numberofuse;

}
