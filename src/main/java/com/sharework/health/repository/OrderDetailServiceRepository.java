package com.sharework.health.repository;

import com.sharework.health.entity.Order;
import com.sharework.health.entity.OrderDetailService;
import io.swagger.models.auth.In;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDetailServiceRepository extends JpaRepository<OrderDetailService, Integer> {

    @Query(nativeQuery = true, value = "select od.* from orderdetailservice od join orders o on od.order_id = o.id where o.id= :orderId")
    List<OrderDetailService> findAllByOrder(Integer orderId);

    @Query(nativeQuery = true, value = "select od.* from orderdetailservice od join orders o on od.order_id = o.id join service s on od.service_id = s.id where  o.id= :orderId and s.id= :serviceId ")
    OrderDetailService findByOrderService(Integer orderId, Integer serviceId);
}
