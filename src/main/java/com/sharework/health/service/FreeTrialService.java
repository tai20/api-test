package com.sharework.health.service;

import com.sharework.health.dto.FreeTrialDto;

public interface FreeTrialService extends BaseService<FreeTrialDto, Integer> {
}
