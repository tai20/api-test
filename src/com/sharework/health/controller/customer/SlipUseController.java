package com.sharework.health.controller.customer;

import com.sharework.health.dto.SlipUseDto;
import com.sharework.health.service.SlipUseService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/slipuses")
@AllArgsConstructor
public class SlipUseController {

    private SlipUseService slipUseService;

    @GetMapping("/{customer_id}")
    public Object getSlipuseByCustomer(@PathVariable("customer_id") Integer customerId){
        List<SlipUseDto> slipUseDtos = slipUseService.findAllByCustomer(customerId);
        if (slipUseDtos.isEmpty() || slipUseDtos == null){
            return new ResponseEntity<>("Bệnh nhân chưa có phiếu sử dụng nào cả", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(slipUseDtos, HttpStatus.OK);
    }
}
