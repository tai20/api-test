package com.sharework.health.controller.admin;

import com.sharework.health.dto.*;
import com.sharework.health.repository.TransferReceiptRepository;
import com.sharework.health.service.TransferReceiptService;
import com.sharework.health.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/admin/transferreceipts")
@AllArgsConstructor
public class TransferReceiptController {

    private TransferReceiptService transferReceiptService;
    private TransferReceiptRepository transferReceiptRepository;

    private UserService userService;


    @GetMapping("")
    public ResponseEntity<ResponseObject> getAllTransferReceipt(Principal principal){
        String email= principal.getName();
        UserDto user = userService.findByEmail(email);
        List<TransferReceiptDto> transferReceiptDtos = new ArrayList<>();

        if(user.getClinicDto().getType().name().equals("DEPOT")){
            transferReceiptDtos = transferReceiptService.findAll();
        }
        else{
            transferReceiptDtos = transferReceiptService.findAllByIdClinic(user.getClinicDto().getId());
        }
        if (transferReceiptDtos.isEmpty()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không có dữ liệu", "")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(200,  "Truy vấn thành công", transferReceiptDtos)
        );
    }

    @GetMapping("{transferrecipt_id}")
    public ResponseEntity<ResponseObject> getTransferReceiptById(@PathVariable("transferrecipt_id") Integer id, Principal principal){
        String email= principal.getName();
        UserDto user = userService.findByEmail(email);
        TransferReceiptDto dto = new TransferReceiptDto();
        if(user.getRoleDto().getId()==1){
            dto = transferReceiptService.findById(id);
        }
        else{
            dto = transferReceiptService.findByIdAnDRole(id,user.getId());
        }

        if (dto!=null)
        {
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Truy vấn thành công", dto)
            );

        } else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject(404, "Không tìm thấy", "")
            );
    }

    @PostMapping("")
    public ResponseEntity<ResponseObject> addTransferReceipt(@RequestBody TransferReceiptDto dto){
        if (dto == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không có dữ liệu", "")
            );
        }
        if (dto.getClinicRecevingDto().getId() ==  dto.getClinicSendingDto().getId()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Kho nhận và kho đến trùng nhau", "")
            );
        }
        TransferReceiptDto result = transferReceiptService.insertTransferReceipt(dto);
        if(result == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Thêm không thành công", "")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(200,  "Thêm phiếu thành công", result)
        );
    }

    @PutMapping("/confirm/{transferrecipt_id}")
    public ResponseEntity<ResponseObject> confirmRecevied(@PathVariable("transferrecipt_id") Integer id){
        TransferReceiptDto result = transferReceiptService.confirmRecevied(id);
        if (result==null){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject(404, "Xác nhận không thành công. Hàng đã chuyển hoặc phiếu đã xóa", "")
            );
        } else{
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Chuyển hàng thành công", result)
            );
        }
    }


    @PostMapping("/detail")
    public ResponseEntity<ResponseObject> addTransferReceiptDetail(@RequestBody TransferReceiptDetailsDto dto){
        if (dto ==  null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không có dữ liệu", "")
            );
        }
        TransferReceiptDetailsDto result = transferReceiptService.insertTransferReceiptDetail(dto);
        if (result == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Thêm không thành công", "")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(200,  "Thêm phiếu thành công", result)
        );
    }


    @GetMapping("/detail/{receipt_id}")
    public ResponseEntity<ResponseObject> getTransferReceiptDetails(@PathVariable("receipt_id") Integer id){
        List<TransferReceiptDetailsQueryDto> dto = transferReceiptService.getAllTransferReceiptDetailByReceiptid(id);
        if (dto.isEmpty())
        {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject(404, "Không tìm thấy", "")
            );
        } else
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Truy vấn thành công", dto)
            );

    }

    @DeleteMapping("{receipt_id}")
    public ResponseEntity<ResponseObject> deleteTransferReceipt(@PathVariable("receipt_id") Integer id){
        boolean result = transferReceiptService.deleteTransferReceipt(id);
        if (result){
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Đã xóa thành công", "")
            );
        } else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không được xóa", "")
            );
        }
    }

    @DeleteMapping("/detail/{receipt_id}/{clinic_stock_id}")
    public ResponseEntity<ResponseObject> deleteTransferReceiptDetail(@PathVariable("receipt_id") Integer receipt_id, @PathVariable("clinic_stock_id") Integer clinic_stock_id){
        boolean result = transferReceiptService.deleteTransferReceiptDetail(receipt_id,clinic_stock_id);
        if (result){
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Đã xóa thành công", "")
            );
        } else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không được xóa", "")
            );
        }
    }



    @PutMapping("/detail")
    public ResponseEntity<ResponseObject> updateTransferReceiptDetail(@RequestBody TransferReceiptDetailsQueryDto dto){
        TransferReceiptDetailsQueryDto result = transferReceiptService.updateTransferReceiptDetail(dto);
        if(result == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Sửa không thành công", "")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(200,  "Sửa thành công", result)
        );

    }

    @PutMapping("/restore/{receipt_id}")
    public ResponseEntity<ResponseObject> restoreTransferReceipt(@PathVariable("receipt_id") Integer id){
        boolean result = transferReceiptService.restoreTransferReceipt(id);
        if (result){
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Đã khôi phục thành công", "")
            );
        } else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không khôi phục được. Hoặc đã có sẵn", "")
            );
        }
    }

}
