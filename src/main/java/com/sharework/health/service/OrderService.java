package com.sharework.health.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import com.sharework.health.dto.*;
import com.sharework.health.entity.Order;
import com.sharework.health.response.OrderReponse;

public interface OrderService extends BaseService<OrderDto, Integer> {


	List<OrderDto> getAllOrderSortDateDESC();

	boolean addOrderDetailProduct(OrderDetailProductDto dto);

	boolean addOrderDetailTreatmentPackage(OrderDetailTreatmentPackageDto dto);

	boolean addOrderDetailService(OrderDetailServiceDto dto);

	boolean updateOrderDetailProduct(OrderDetailProductDto dto);

	boolean updateOrderDetailService(Integer orderId, Integer serviceId, OrderDetailServiceDto dto);

	boolean updateTotalAndDebt(Integer orderId, MoneyDto dto);

//	boolean deleteOrderDetailProduct(Integer orderId);
//
//	boolean deleteOrderDetailService(Integer orderId);
//
//	boolean deleteOrderDetailTreatmentPackage(Integer orderId);

	boolean deleteByOrderId(Integer orderId);
	
	List<OrderDto> getOrderByOrderDate(LocalDateTime orderDate);
	
	List<OrderDto> getOrderByCustomer(Integer customerId);
	
	List<OrderDto> getOrderByUser(Integer userId);

	List<OrderDetailProductDto> findAllOrderDetailProductWithOrder(Integer orderId);

	List<OrderDetailTreatmentPackageDto> findAllOrderDetailTreatmentPackageWithOrder(Integer orderId);

	List<OrderDetailServiceDto> findAllOrderDetailServiceWithOrder(Integer orderId);

	List<OrderDto> findAllOrderByClinic(Integer clinicId);

	List<OrderDto> findAllOrderWithExamination();

	List<OrderDto> findAllOrderWithOrderDetails();

	List<OrderDto> findByOrderDateBetween(LocalDateTime startDate, LocalDateTime endDate);

	boolean confirmOrderBeforePayment(Integer orderId);

	boolean paymentOrder(Integer orderId, PaymentDto dto);

	boolean paymentRest(Integer orderId, PaymentDto dto);

	OrderDto getOrderAfterInsert();

	boolean paymentClinic(Integer orderId, PaymentDto dto);

	//lay tong tien cua khach hang
	public BigDecimal getTotalValueById(Integer id);
	public void createCustomerLevel();

	OrderReponse getAllOrder(int pageNo, int pageSize, String sortBy, String sortDir);

}
