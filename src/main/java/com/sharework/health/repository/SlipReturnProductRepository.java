package com.sharework.health.repository;

import com.sharework.health.entity.SlipReturnProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SlipReturnProductRepository extends JpaRepository<SlipReturnProduct, Integer> {

    @Query(nativeQuery = true,value = "select * from slipreturnproduct where status like 'ACTIVE'")
    List<SlipReturnProduct> findAllByStatusLike();

    @Query(nativeQuery = true, value = "select * from slipreturnproduct where customer_id= :customerId")
    List<SlipReturnProduct> findAllByCustomerId(Integer customerId);
}
