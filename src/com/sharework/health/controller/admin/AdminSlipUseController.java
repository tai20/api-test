package com.sharework.health.controller.admin;

import com.sharework.health.dto.SlipUseDetailDto;
import com.sharework.health.dto.SlipUseDto;
import com.sharework.health.service.SlipUseService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/admin/slipuses")
@AllArgsConstructor
public class AdminSlipUseController {


    private SlipUseService slipUseService;

    @GetMapping("")
    public Object getAllSlipUse() {
        List<SlipUseDto> slipUseDtos = slipUseService.findAll();
        if (slipUseDtos.isEmpty()) {
            return new ResponseEntity<>("Không tìm thấy", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(slipUseDtos, HttpStatus.OK);
    }

    @GetMapping("/{slipuse_id}")
    public Object getSlipUseById(@PathVariable("slipuse_id") Integer slipUseId) {
        SlipUseDto slipUseDto = slipUseService.findById(slipUseId);
        if (slipUseDto == null) {
            return new ResponseEntity<>("Không tìm thấy", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(slipUseDto, HttpStatus.OK);
    }

    @PostMapping("treatmentpackage")
    public Object getSlipUseByTreatmentPackage(@RequestParam("treatmentpackage_name") String treatmentPackageName) {
        List<SlipUseDto> slipUseDtos = slipUseService.findAllByTreatmentPackage(treatmentPackageName);
        if (slipUseDtos == null) {
            return new ResponseEntity<>("Không tìm thấy", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(slipUseDtos, HttpStatus.OK);
    }

    @PostMapping("")
    public Object addSlipUse(@RequestBody SlipUseDto dto, BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>("Thêm phiếu sử dụng thất bại", HttpStatus.BAD_REQUEST);
        }
        slipUseService.insert(dto);
        return new ResponseEntity<>("Thêm phiếu sử dụng thành công", HttpStatus.CREATED);
    }

    @PutMapping("/{slipuse_id}")
    public Object updateSlipUse(@PathVariable("slipuse_id") Integer id, @RequestBody SlipUseDto dto,
                                BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = slipUseService.update(id, dto);
        if (!result) {
            return new ResponseEntity<>("Cập nhật thất bại", HttpStatus.OK);
        }
        return new ResponseEntity<>("Cập nhật thành công", HttpStatus.OK);
    }

    @DeleteMapping("/{slipuse_id}")
    public Object deleteSlipUse(@PathVariable("slipuse_id") Integer id) {
        slipUseService.delete(id);
        return new ResponseEntity<>("Xóa thành công", HttpStatus.OK);
    }

    @GetMapping("getSlipUseByCustomer/{customer_id}")
    public Object getSlipUseByCustomer(@PathVariable("customer_id") Integer customerId) {
        List<SlipUseDto> slipUseDtos = slipUseService.findAllByCustomer(customerId);
        if (slipUseDtos.isEmpty() || slipUseDtos == null) {
            return new ResponseEntity<>("Khách hàng chưa có phiếu sử dụng nào cả", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(slipUseDtos, HttpStatus.OK);
    }

    @GetMapping("getAllSlipUseDetailBySlipUse/{slipuse_id}")
    public Object getAllSlipUseDetailBySlipUse(@PathVariable("slipuse_id") Integer slipUseId) {
        List<SlipUseDetailDto> slipUseDetailDtos = slipUseService.getAllSlipUseDetailBySlipUse(slipUseId);
        if (slipUseDetailDtos.isEmpty() || slipUseDetailDtos == null) {
            return new ResponseEntity<>("Lỗi phiếu sử dụng chưa có chi tiết", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(slipUseDetailDtos, HttpStatus.OK);
    }

    @PutMapping("uploadImageBeforeSlipUse/{slipUse_id}")
    public Object uploadImageBeforeSlipUse(@PathVariable("slipUse_id") Integer slipUseId, @RequestParam("imageBefore") MultipartFile imageBefore) throws IOException {
        if (imageBefore == null || imageBefore.isEmpty()) {
            return new ResponseEntity<>("Chưa có ảnh", HttpStatus.BAD_REQUEST);
        }
        boolean result = slipUseService.uploadImageBeforeSlipUse(slipUseId, imageBefore);
        if (!result) {
            return new ResponseEntity<>("Lỗi upload ảnh", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Upload ảnh thành công", HttpStatus.OK);
    }

    @PutMapping("uploadImageAfterSlipUse/{slipUse_id}")
    public Object uploadImageAfterSlipUse(@PathVariable("slipUse_id") Integer slipUseId, @RequestParam("imageAfter") MultipartFile imageAfter) throws IOException {
        if (imageAfter == null || imageAfter.isEmpty()) {
            return new ResponseEntity<>("Chưa có ảnh", HttpStatus.BAD_REQUEST);
        }
        boolean result = slipUseService.uploadImageAfterSlipUse(slipUseId, imageAfter);
        if (!result) {
            return new ResponseEntity<>("Lỗi upload ảnh", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Upload ảnh thành công", HttpStatus.OK);
    }


//    @GetMapping("getImageSlipUseUpload/{image}")
//    public Object getImageSlipUseUpload(@PathVariable("image") String image) throws IOException {
//        if (!image.equals("") || image != null) {
//            try {
////                Path staticPath = Paths.get("static");
////                Path imagePath = Paths.get("images");
//                Path fileName = Paths.get("static/images", image);
//                byte[] buffer = Files.readAllBytes(fileName);
//                ByteArrayResource byteArrayResource = new ByteArrayResource(buffer);
//                return ResponseEntity.ok()
//                        .contentLength(buffer.length)
//                        .contentType(MediaType.parseMediaType("image/png"))
//                        .body(byteArrayResource);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        return new ResponseEntity<>("Không có ảnh", HttpStatus.BAD_REQUEST);
//    }

    @PutMapping("activeSlipUse/{slipUse_id}")
    public Object activeSlipUse(@PathVariable("slipUse_id") Integer slipUseId){
        boolean result = slipUseService.activeSlipUse(slipUseId);
        if (!result){
            return new ResponseEntity<>("Phiếu đã được kích hoạt hoặc có lỗi", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Phiếu đã được kích hoạt thành công", HttpStatus.OK);
    }

}
