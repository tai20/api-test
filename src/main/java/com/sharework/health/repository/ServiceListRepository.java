package com.sharework.health.repository;

import com.sharework.health.entity.ServiceList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceListRepository extends JpaRepository<ServiceList, Integer> {
    @Query(nativeQuery = true, value = "select* from servicelist where customer_id = :customerId" )
    List<ServiceList> findAllServiceListByCustomerId (Integer customerId);
}
