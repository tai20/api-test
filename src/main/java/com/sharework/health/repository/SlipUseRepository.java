package com.sharework.health.repository;

import com.sharework.health.dto.SlipUseQuery;
import com.sharework.health.entity.Customer;
import com.sharework.health.entity.SlipUse;
import com.sharework.health.entity.TreatmentPackage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Repository
public interface SlipUseRepository extends JpaRepository<SlipUse, Integer> {

    List<SlipUse> findAllByCustomer(Customer customer);

    List<SlipUse> findAllByTreatmentPackage(TreatmentPackage treatmentPackage);

    @Query(nativeQuery = true, value = "select * from slipuse where enddate >= now()")
    List<SlipUse> findAll();

    List<SlipUse> findByCustomer_Id(Integer id);

//    @Query(value = "SELECT new com.sharework.health.dto.SlipUseQuery(sv.id, S.id, sl.numberOfStock) FROM SlipUseDetail sl " +
//             " JOIN SlipUse s ON sl.service.id = s.id JOIN Service sv ON sv.id = sl.service.id order by sl.slipUse.id desc NULLS first ")
    @Query(nativeQuery = true, value = "SELECT sl.service_id, sl.slipuse_id, sl.numberofstock " +
            "FROM public.slipusedetail sl " +
            "join service sv on sl.service_id = sv.id " +
            "join slipuse s on sl.slipuse_id = s.id " +
            "order by slipuse_id desc limit 1;")
    SlipUseQuery findSlipAfterInsert();

    //tim kiem theo goi dich vu co phan trang
    @Query(nativeQuery = true, value = "SELECT * " +
            " FROM public.slipuse s  " +
            " join public.customer c " +
            " on s.customer_id = c.id " +
            " where c.name ilike %:name% ")
    Page<SlipUse> findAllByNameCustomer(Pageable pageable, String name);

    @Query(nativeQuery = true, value = "select * from slipuse where startdate =:startdate")
    Page<SlipUse> findAllByDate(Pageable pageable, LocalDate startdate);

    @Query(nativeQuery = true, value = "select * from slipuse  where  status =:status")
    Page<SlipUse> findAllByStatus(Pageable pageable, String status);

    @Query(nativeQuery = true, value = "select * from slipuse  where  name ilike %:name%")
    Page<SlipUse> findAllByNameSlipUse(Pageable pageable, String name);


}
