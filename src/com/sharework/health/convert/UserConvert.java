package com.sharework.health.convert;

import com.sharework.health.dto.ClinicDto;
import com.sharework.health.dto.RoleDto;
import com.sharework.health.dto.UserDto;
import com.sharework.health.entity.User;
import org.mindrot.jbcrypt.BCrypt;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class UserConvert implements BaseConvert<User, UserDto> {

    @Override
    public User dtoToEntity(UserDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        String hashed = BCrypt.hashpw(dto.getPassword(), BCrypt.gensalt());
//        return new User()
//                .setName(dto.getName())
//                .setEmail(dto.getEmail())
//                .setPassword(hashed)
//                .setBirthDate(dto.getBirthDate())
//                .setGender(dto.getGender())
//                .setAddress(dto.getAddress())
//                .setPhoneNumber(dto.getPhoneNumber())
//                .setStatus(Status.ACTIVE)
//                .setCertificate(dto.getCertificate())
//                .setWorkExperience(dto.getWorkExperience())
//                .setCmnd(dto.getCmnd());
        return modelMapper.map(dto, User.class).setPassword(hashed);
    }

    @Override
    public UserDto entityToDto(User entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, UserDto.class)
                .setRoleDto(modelMapper.map(entity.getRole(), RoleDto.class))
                .setClinicDto(modelMapper.map(entity.getClinic(), ClinicDto.class));
    }
}
