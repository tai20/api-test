package com.sharework.health.dto;

import com.sharework.health.entity.ReceiptStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class StatisticsOtherProductsQueryDto {

    private Integer product_id;

    private String product_name;

    private String product_category;

    private String unit;

    private Integer quantity;

    private Integer warehouse_id;

    private String warehouse_name;

    private boolean type;

}
