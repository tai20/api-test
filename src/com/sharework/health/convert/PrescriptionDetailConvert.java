package com.sharework.health.convert;

import com.sharework.health.dto.PrescriptionDetailDto;
import com.sharework.health.dto.PrescriptionDto;
import com.sharework.health.dto.ProductDto;
import com.sharework.health.entity.PrescriptionDetail;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class PrescriptionDetailConvert implements BaseConvert<PrescriptionDetail, PrescriptionDetailDto> {

    @Override
    public PrescriptionDetailDto entityToDto(PrescriptionDetail entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, PrescriptionDetailDto.class)
                .setProductDto(modelMapper.map(entity.getProduct(), ProductDto.class))
                .setPrescriptionDto(modelMapper.map(entity.getPrescription(), PrescriptionDto.class));
    }

    @Override
    public PrescriptionDetail dtoToEntity(PrescriptionDetailDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, PrescriptionDetail.class);
    }
}
