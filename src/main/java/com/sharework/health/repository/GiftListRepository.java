package com.sharework.health.repository;

import com.sharework.health.entity.CustomerLevel;
import com.sharework.health.entity.Gift;
import com.sharework.health.entity.GiftList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GiftListRepository extends JpaRepository<GiftList, Gift> {
    List<GiftList> findByGift_IdEquals(Integer id);

    List<GiftList> findByGift_Id(Integer id);

    @Query(nativeQuery = true, value="select s.* from gift_list s WHERE s.gift_id = :id")
    List<GiftList> findGiftListByGID(Integer id);

    @Query(nativeQuery = true, value="DELETE FROM public.gift_list WHERE gift_id = id;")
    GiftList deleteGiftListByGID(Integer id);
}
