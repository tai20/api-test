package com.sharework.health.controller.admin;

import java.time.LocalDateTime;
import java.util.List;

import com.sharework.health.dto.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.sharework.health.service.OrderService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/api/admin/orders")
@AllArgsConstructor
public class AdminOrderController {
	
	private OrderService orderService;
	
	@GetMapping("")
	public Object getAllOrder() {
		List<OrderDto> orderDtos = orderService.findAll();
		if (orderDtos.isEmpty() || orderDtos == null) {
			return new ResponseEntity<>("Không có lịch hẹn", HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(orderDtos, HttpStatus.OK);
	}
	
	@PostMapping("")
	public Object addOrder(@RequestBody OrderDto dto) { ;
		if (dto == null) {
			return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
		}
//		boolean result = orderService.insertOrder(dto,clinicId);
		boolean result = orderService.insert(dto);
		if (!result) {
			return new ResponseEntity<>("Không thêm được", HttpStatus.BAD_REQUEST);
		}
		OrderDto dto1 = orderService.getOrderAfterInsert();
		OrderAfterInsertDto orderAfterInsertDto = new OrderAfterInsertDto()
				.setMessage("Thêm hóa đơn thành công")
				.setOrderDto(dto1);
		return new ResponseEntity<>(orderAfterInsertDto, HttpStatus.CREATED);
	}

	@PutMapping("{order_id}")
	public Object updateOrder(@PathVariable("order_id") Integer orderId,@RequestBody OrderDto dto) {
		if (dto == null) {
			return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
		}
		boolean result = orderService.update(orderId, dto);
		if (!result) {
			return new ResponseEntity<>("Cập nhật thất bại", HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>("Cập nhật hóa đơn thành công", HttpStatus.CREATED);
	}
	
	@PostMapping("addOrderDetailProduct")
	public Object addOrderDetailProduct(@RequestBody OrderDetailProductDto dto) {
		if (dto == null) {
			return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
		}
		boolean result = orderService.addOrderDetailProduct(dto);
		if (!result) {
			return new ResponseEntity<>("Không thêm được", HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>("Thêm thành công", HttpStatus.CREATED);
	}

	@PostMapping("addOrderDetailTreatmentPackage")
	public Object addOrderDetailTreatmentPackage(@RequestBody OrderDetailTreatmentPackageDto dto) {
		if (dto == null) {
			return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
		}
		boolean result = orderService.addOrderDetailTreatmentPackage(dto);
		if (!result) {
			return new ResponseEntity<>("Không thêm được", HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>("Thêm thành công", HttpStatus.CREATED);
	}

	@PostMapping("addOrderDetailService")
	public Object addOrderDetailService(@RequestBody OrderDetailServiceDto dto) {
		if (dto == null) {
			return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
		}
		boolean result = orderService.addOrderDetailService(dto);
		if (!result) {
			return new ResponseEntity<>("Không thêm được", HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>("Thêm thành công", HttpStatus.CREATED);
	}
	
	@PostMapping("{orderDate}")
	public Object getOrderByOrderDate(@PathVariable("orderDate") LocalDateTime orderDate) {
		if (orderDate == null) {
			return new ResponseEntity<>("Chưa chọn ngày và giờ", HttpStatus.BAD_REQUEST);
		}
		List<OrderDto> orderDtos = orderService.getOrderByOrderDate(orderDate);
		if (orderDtos.isEmpty() || orderDtos == null) {
			return new ResponseEntity<>("Không tìm thấy lịch hẹn trong thời gian này", HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(orderDtos, HttpStatus.OK);
	}
	
	@PostMapping("{customer_id}")
	public Object getOrderByCustomer(@PathVariable("customer_id") Integer customerId) {
	
		List<OrderDto> orderDtos = orderService.getOrderByCustomer(customerId);
		
		if (orderDtos.isEmpty() || orderDtos == null) {
			return new ResponseEntity<>("Khách hàng này không có lịch hẹn", HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(orderDtos, HttpStatus.OK);
	}
	
	@GetMapping("{user_id}")
	public Object getOrderByUser(@PathVariable("user_id") Integer userId) {
	
		List<OrderDto> orderDtos = orderService.getOrderByUser(userId);
		
		if (orderDtos.isEmpty() || orderDtos == null) {
			return new ResponseEntity<>("Nhân viên này không có lịch hẹn với bất kì khách hàng nào", HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(orderDtos, HttpStatus.OK);
	}

	@GetMapping("getOrderDetailProductByOrder/{order_id}")
	public Object getOrderDetailProductByOrder(@PathVariable("order_id") Integer orderId){
		List<OrderDetailProductDto> orderDetailProductDtos = orderService.findAllOrderDetailProductWithOrder(orderId);
		if (orderDetailProductDtos.isEmpty() || orderDetailProductDtos == null){
			return new ResponseEntity<>("Không có chi tiết hóa đơn nào cho hóa đơn này", HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(orderDetailProductDtos, HttpStatus.OK);
	}

	@GetMapping("getOrderDetailTreatmentPackageByOrder/{order_id}")
	public Object getOrderDetailTreatmentPackageByOrder(@PathVariable("order_id") Integer orderId){
		List<OrderDetailTreatmentPackageDto> orderDetailTreatmentPackageDtos = orderService.findAllOrderDetailTreatmentPackageWithOrder(orderId);
		if (orderDetailTreatmentPackageDtos.isEmpty() || orderDetailTreatmentPackageDtos == null){
			return new ResponseEntity<>("Không có chi tiết hóa đơn nào cho hóa đơn này", HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(orderDetailTreatmentPackageDtos, HttpStatus.OK);
	}

	@GetMapping("getOrderDetailServiceByOrder/{order_id}")
	public Object getOrderDetailServiceByOrder(@PathVariable("order_id") Integer orderId){
		List<OrderDetailServiceDto> orderDetailServiceDtos = orderService.findAllOrderDetailServiceWithOrder(orderId);
		if (orderDetailServiceDtos.isEmpty() || orderDetailServiceDtos == null){
			return new ResponseEntity<>("Không có chi tiết hóa đơn nào cho hóa đơn này", HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(orderDetailServiceDtos, HttpStatus.OK);
	}

	@GetMapping("getOrderById/{order_id}")
	public Object getOrderById(@PathVariable("order_id") Integer orderId){
		OrderDto orderDto = orderService.findById(orderId);
		if (orderDto == null){
			return new ResponseEntity<>("Không tìm thấy hóa đơn này", HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(orderDto, HttpStatus.OK);
	}

	@GetMapping("getAllOrderByClinic/{clinic_id}")
	public Object getAllOrderByClinic(@PathVariable("clinic_id") Integer clinicId){
		List<OrderDto> orderDtos = orderService.findAllOrderByClinic(clinicId);
		if (orderDtos.isEmpty() || orderDtos == null){
			return new ResponseEntity<>("Chi nhánh này không có hóa đơn", HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(orderDtos, HttpStatus.OK);
	}

//	@PutMapping("updateStatusOrderWithOrderDetails/{order_id}")
//	public Object updateStatusOrderWithOrderDetails(@PathVariable("order_id") Integer orderId){
//
//		boolean result = orderService.updateStatusOrder(orderId);
//		if (!result){
//			return new ResponseEntity<>("lỗi không cập nhật được nữa cập nhật", HttpStatus.BAD_REQUEST);
//		}
//		return new ResponseEntity<>("Đã cập nhật trạng thái hóa đơn", HttpStatus.OK);
//	}

//	@PutMapping("updateStatusOrderWithExamination/{order_id}")
//	public Object updateStatusOrderWithExamination(@PathVariable("order_id") Integer orderId){
//
//		boolean result = orderService.updateStatusOrderWithExamination(orderId);
//		if (!result){
//			return new ResponseEntity<>("Không tìm thấy id hoặc bác sĩ đã xác nhận phiếu khám này", HttpStatus.BAD_REQUEST);
//		}
//		return new ResponseEntity<>("Đã cập nhật trạng thái phiếu khám", HttpStatus.OK);
//	}

	@GetMapping("getAllOrderWithExamination")
	public Object getAllOrderWithExamination(){
		List<OrderDto> orderDtos = orderService.findAllOrderWithExamination();
		if (orderDtos.isEmpty() || orderDtos == null){
			return new ResponseEntity<>("Chi nhánh này không có hóa đơn", HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(orderDtos, HttpStatus.OK);
	}

	@GetMapping("getAllOrderWithOrderDetails")
	public Object getAllOrderWithOrderDetails(){
		List<OrderDto> orderDtos = orderService.findAllOrderWithOrderDetails();
		if (orderDtos.isEmpty() || orderDtos == null){
			return new ResponseEntity<>("Chi nhánh này không có hóa đơn", HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(orderDtos, HttpStatus.OK);
	}

	@GetMapping("getOrdersByOrderDateBetween")
	public Object getOrdersOrderDateBetween(@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate){
		List<OrderDto> orderDtos = orderService.findByOrderDateBetween(LocalDateTime.parse(startDate), LocalDateTime.parse(endDate));
		if (orderDtos.isEmpty() || orderDtos == null){
			return new ResponseEntity<>("Chi nhánh này không có hóa đơn", HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(orderDtos, HttpStatus.OK);
	}

	@PutMapping("confirmOrderBeforePayment/{order_id}")
	public Object confirmOrderBeforePayment(@PathVariable("order_id") Integer orderId){
		boolean result = orderService.confirmOrderBeforePayment(orderId);
		if (!result){
			return new ResponseEntity<>("Không cập nhật được nữa", HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>("Cập nhật trạng thái hóa đơn thành công", HttpStatus.OK);
	}

	@PutMapping("paymentOrder/{order_id}")
	public Object paymentOrder(@PathVariable("order_id") Integer orderId, @RequestBody PaymentDto dto){
		boolean result = orderService.paymentOrder(orderId, dto);
		if (!result){
			return new ResponseEntity<>("Thanh toán thất bại vì thiếu thuộc tính hoặc chưa được kế toán xác nhận", HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>("Thanh toán thành công", HttpStatus.OK);
	}

	@PutMapping("paymentRest/{order_id}")
	public Object paymentRest(@PathVariable("order_id") Integer orderId, @RequestBody PaymentDto dto){
		boolean result = orderService.paymentRest(orderId, dto);
		if (!result){
			return new ResponseEntity<>("Đã thanh toán hết hoặc có lỗi thanh toán", HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>("Thanh toán thành công", HttpStatus.OK);
	}

	@PutMapping("paymentClinic/{order_id}")
	public Object paymentClinic(@PathVariable("order_id") Integer orderId, @RequestBody PaymentDto dto){
		boolean result = orderService.paymentClinic(orderId, dto);
		if (!result){
			return new ResponseEntity<>("Thanh toán thất bại vì thiếu thuộc tính hoặc chưa được kế toán xác nhận", HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>("Thanh toán thành công", HttpStatus.OK);
	}
}
