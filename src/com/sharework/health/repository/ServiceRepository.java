package com.sharework.health.repository;

import com.sharework.health.entity.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceRepository extends JpaRepository<Service, Integer> {

    @Query(nativeQuery = true, value = "select * from service where status like 'ACTIVE'")
    List<Service> findALlByStatusLike();

    @Query(nativeQuery = true, value = "select * from service order by id desc limit 1")
    Service findServiceAfterInsert();

}
