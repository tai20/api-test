package com.sharework.health.service;

import com.sharework.health.dto.ClinicDto;
import com.sharework.health.dto.TotalCostDto;

public interface TotalCostService extends BaseService<TotalCostDto, Integer> {
    TotalCostDto searchTotalCostById(String totalcostId);
    boolean updateTotalCostInMedicalCard(Integer medicalCardId, TotalCostDto dto);
    TotalCostDto getTotalCostAfterInsert();
}
