package com.sharework.health.controller.admin;

import com.sharework.health.dto.ProductServiceDetailDto;
import com.sharework.health.dto.ServiceAfterInsertDto;
import com.sharework.health.dto.ServiceDto;
import com.sharework.health.service.ServiceService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/services")
@AllArgsConstructor
public class ServiceController {

    private ServiceService serviceService;

    @GetMapping("")
    public Object getAllService(){
        List<ServiceDto> serviceDtos = serviceService.findAll();
        if(serviceDtos.isEmpty() || serviceDtos == null){
            return new ResponseEntity<>("Không có dịch vụ", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(serviceDtos, HttpStatus.OK);
    }

    @PostMapping("")
    public Object addService(@RequestBody ServiceDto dto){
        if (dto == null){
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = serviceService.insert(dto);
        if (!result){
            return new ResponseEntity<>("Thêm dịch vụ thất bại", HttpStatus.BAD_REQUEST);
        }
        ServiceDto serviceDto = serviceService.findServiceAfterInsert();
        ServiceAfterInsertDto serviceAfterInsertDto = new ServiceAfterInsertDto()
                .setMessage("Thêm dịch vụ thành công")
                .setServiceDto(serviceDto);
        return new ResponseEntity<>(serviceDto, HttpStatus.OK);
    }

    @PutMapping("{service_id}")
    public Object updateService(@PathVariable("service_id") Integer serviceId , @RequestBody ServiceDto dto){
        if (dto == null){
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = serviceService.update(serviceId, dto);
        if (!result){
            return new ResponseEntity<>("Thêm dịch vụ thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm dịch vụ thành công", HttpStatus.OK);
    }

    @DeleteMapping("{service_id}")
    public Object deleteService(@PathVariable("service_id") Integer serviceId){
        boolean result = serviceService.delete(serviceId);
        if (!result){
            return new ResponseEntity<>("Xóa dịch vụ thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Xóa dịch vụ thành công", HttpStatus.OK);
    }

    @PostMapping("addProductServiceDetail")
    public Object addProductServiceDetail(@RequestBody ProductServiceDetailDto dto){
        if (dto == null){
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = serviceService.addProductServiceDetail(dto);
        if (!result){
            return new ResponseEntity<>("Thêm dịch vụ thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm dịch vụ thành công", HttpStatus.OK);
    }

    @GetMapping("getAllByServiceId/{service_id}")
    public Object getAllByServiceId(@PathVariable("service_id") Integer serviceId){
        List<ProductServiceDetailDto> productServiceDetailDtos = serviceService.findAllByServiceId(serviceId);
        if (productServiceDetailDtos.isEmpty()){
            return new ResponseEntity<>("Không có dữ liệu", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(productServiceDetailDtos, HttpStatus.OK);
    }
}
