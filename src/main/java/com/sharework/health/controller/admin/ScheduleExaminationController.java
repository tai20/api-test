package com.sharework.health.controller.admin;

import com.sharework.health.dto.ScheduleExaminationDto;
import com.sharework.health.service.ScheduleExaminationService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/scheduleexaminations")
@AllArgsConstructor
public class ScheduleExaminationController {

    private ScheduleExaminationService scheduleExaminationService;

    @GetMapping("")
    public Object getAllScheduleExaminations() {

        List<ScheduleExaminationDto> scheduleExaminationDtos = scheduleExaminationService.findAll();

        if (scheduleExaminationDtos.isEmpty()) {
            return new ResponseEntity<>("Không có lịch khám nào!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(scheduleExaminationDtos, HttpStatus.OK);
    }

    @GetMapping("/getScheduleByCustomer/{customerId}")
    public Object getScheduleExaminationsByCustomer(@PathVariable("customerId") Integer customerId) {
        ScheduleExaminationDto dto = scheduleExaminationService.findScheduleExaminationByCustomer(customerId);
        if (dto == null) {
            return new ResponseEntity<>("Không tìm thấy lịch khám của khách hàng", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping("/getScheduleByUser/{userId}")
    public Object geScheduleExaminationByUser(@PathVariable("userId") Integer userId) {
        List<ScheduleExaminationDto> scheduleExaminationDtos = scheduleExaminationService.findAllByUser(userId);

        if (scheduleExaminationDtos == null) {
            return new ResponseEntity<>("Bác sĩ không có lịch khám nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(scheduleExaminationDtos, HttpStatus.OK);
    }

    @GetMapping("/{scheduleId}")
    public Object getScheduleExmaminationById(@PathVariable("scheduleId") Integer id) {
        ScheduleExaminationDto dto = scheduleExaminationService.findById(id);
        if (dto == null) {
            return new ResponseEntity<>("Không có lịch khám hợp lệ", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping("")
    public Object addScheduleExamination(@RequestBody ScheduleExaminationDto dto) {

        if (dto == null) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = scheduleExaminationService.insert(dto);
        if (!result) {
            return new ResponseEntity<>("Thêm lịch khám thất bại", HttpStatus.BAD_REQUEST);
        }
        ScheduleExaminationDto dto1 = scheduleExaminationService.findScheduleExaminationAfterInsert();
        return new ResponseEntity<>(dto1, HttpStatus.CREATED);
    }

//    @PutMapping("/activeScheduleExamination/{scheduleExaminationId}")
//    public Object activeScheduleExamination(@PathVariable("scheduleExaminationId") Integer scheduleExaminationId, @RequestParam("signature") MultipartFile signature) throws IOException {
//        boolean result = scheduleExaminationService.activeScheduleExamination(scheduleExaminationId, signature);
//        if (!result){
//            return new ResponseEntity<>("Lịch khám đã được kích hoạt rồi hoặc không tồn tại lịch khám này hoặc không có vân tay", HttpStatus.BAD_REQUEST);
//        }
//        return new ResponseEntity<>("Lịch khám được kích hoạt thành công", HttpStatus.OK);
//    }

    @PutMapping("/activeScheduleExamination/{scheduleExaminationId}")
    public Object activeScheduleExamination(@PathVariable("scheduleExaminationId") Integer scheduleExaminationId) {
        boolean result = scheduleExaminationService.activeScheduleExamination(scheduleExaminationId);
        if (!result) {
            return new ResponseEntity<>("Lịch khám đã được kích hoạt rồi hoặc không tồn tại lịch khám này ", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Lịch khám được kích hoạt thành công", HttpStatus.OK);
    }

    @PutMapping("/confirmEndExamination/{scheduleExaminationId}")
    public Object confirmEndExamination(@PathVariable("scheduleExaminationId") Integer scheduleExaminationId) {
        boolean result = scheduleExaminationService.confirmEndExamination(scheduleExaminationId);
        if (!result) {
            return new ResponseEntity<>("Lịch khám chuyển trạng thái không thành công", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Lịch khám được chuyển sang trạng thái EXAMINED thành công", HttpStatus.OK);
    }

    @GetMapping("getAllScheduleExaminationNearest")
    public Object getAllScheduleExaminationNearest() {

        List<ScheduleExaminationDto> scheduleExaminationDtos = scheduleExaminationService.getAllScheduleExaminationNearest();

        if (scheduleExaminationDtos.isEmpty()) {
            return new ResponseEntity<>("Không có lịch khám nào!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(scheduleExaminationDtos, HttpStatus.OK);
    }
}
