package com.sharework.health.controller.admin;

import com.sharework.health.dto.ServiceCategoryDto;
import com.sharework.health.service.TreatmentPackageCategoryService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/treatmentpackageCategories")
@AllArgsConstructor
public class TreatmentPackageCategoryController {

    private TreatmentPackageCategoryService treatmentPackageCategoryService;

    @GetMapping("")
    public ResponseEntity<List<ServiceCategoryDto>> getAllTreatmentPackageCategory() {
        List<ServiceCategoryDto> serviceCategoryDtos = treatmentPackageCategoryService.findAll();
        if (serviceCategoryDtos.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(serviceCategoryDtos,HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<ServiceCategoryDto> addTreatmentPackageCategory(@RequestBody ServiceCategoryDto dto,
                                                                          BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        treatmentPackageCategoryService.insert(dto);
        return new ResponseEntity<>(dto,HttpStatus.CREATED);
    }

    @PutMapping("/{treatmentpackage_category_id}")
    public ResponseEntity<ServiceCategoryDto> updateTreatmentPackageCategoryService(@PathVariable("treatmentpackage_category_id") Integer id , @RequestBody ServiceCategoryDto dto,
                                                                                    BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        treatmentPackageCategoryService.update(id, dto);
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }

    @DeleteMapping("/{treatmentpackage_category_id}")
    public ResponseEntity<ServiceCategoryDto> deleteTreatmentPackageCategory(@PathVariable("treatmentpackage_category_id") Integer id) {
        treatmentPackageCategoryService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("searchName")
    public Object searchTreatmentPackageCategoryByCategoryName(@RequestParam("treatmentpackage_category_name") String name) {
        ServiceCategoryDto dto = treatmentPackageCategoryService.searchTreatmentPackageCategoryByName(name);
        if (dto == null){
            return new ResponseEntity<>("Không tìm thấy loại sản phẩm cần tìm",HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }
}
