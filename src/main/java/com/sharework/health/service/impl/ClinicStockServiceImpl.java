package com.sharework.health.service.impl;


import com.sharework.health.Constants;
import com.sharework.health.convert.ClinicStockConvert;
import com.sharework.health.dto.ClinicDto;
import com.sharework.health.dto.ClinicStockDto;
import com.sharework.health.dto.ProductDto;
import com.sharework.health.dto.ResponseObject;
import com.sharework.health.dto.UserDto;
import com.sharework.health.entity.ClinicStock;
import com.sharework.health.entity.Product;
import com.sharework.health.entity.Clinic;
import com.sharework.health.entity.Status;
import com.sharework.health.repository.ClinicRepository;
import com.sharework.health.repository.ClinicStockRepository;
import com.sharework.health.response.ClinicStockReponse;
import com.sharework.health.service.ClinicStockService;
import com.sharework.health.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class ClinicStockServiceImpl implements ClinicStockService {

    private ClinicStockRepository clinicStockRepository;

    private ClinicStockConvert clinicStockConvert;

    private ClinicRepository clinicRepository;

    private UserService userService;

    @Override
    public List<ClinicStockDto> findAll() {
        List<ClinicStock> clinicStocks = clinicStockRepository.findAll();
        List<ClinicStockDto> clinicStockDtos = new ArrayList<>();
        for (ClinicStock entity: clinicStocks
        ) {
            if (entity.getProduct().getStatus() == Status.ACTIVE){
                ClinicStockDto dto = clinicStockConvert.entityToDto(entity);
                clinicStockDtos.add(dto);
            }

        }
        return clinicStockDtos;
    }

    @Override
    public ClinicStockDto findById(Integer id) {
        ClinicStock entity = clinicStockRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return clinicStockConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(ClinicStockDto clinicStockDto){
        return false;
    }

    @Override
    public boolean update(Integer id, ClinicStockDto clinicStockDto){
        ClinicStock clinicStock = clinicStockRepository.findById(id).get();
        Clinic clinic = clinicRepository.findById(clinicStockDto.getClinicDto().getId()).get();
        if(clinicStock == null){
            return false;
        }
        clinicStock.setQuantity(clinicStockDto.getQuantity());
        clinicStock.setClinic(clinic);
        clinicStockRepository.save(clinicStock);
        return true;
    }


    @Override
    public List<ClinicStockDto> getClinicStockByClinicId(Integer id){
        List<ClinicStock> clinicStocks = clinicStockRepository.getClinicStockByClinicId(id);
        List<ClinicStockDto> clinicStockDtos = new ArrayList<>();
        for (ClinicStock entity:clinicStocks){
            if (entity.getProduct().getStatus() == Status.ACTIVE){
                ClinicStockDto dto = clinicStockConvert.entityToDto(entity);
                clinicStockDtos.add(dto);
            }

        }
        return clinicStockDtos;
    }
    //Hien thi ton kho theo chi nhanh
    @Override
    public List<ClinicStockDto> getClinicStockByClinicIdOfUsers(Integer id){
        Clinic clinic=clinicRepository.getClinicByClinicId(id);

        if(clinic.getType().name().equals("DEPOT")){
            List<ClinicStock> clinicStocks=clinicStockRepository.findAll();
            List<ClinicStockDto> clinicStockDtos = new ArrayList<>();
            for (ClinicStock entity:clinicStocks){
                if (entity.getProduct().getStatus() == Status.ACTIVE){
                    ClinicStockDto dto = clinicStockConvert.entityToDto(entity);
                    clinicStockDtos.add(dto);
                }
            }
            return clinicStockDtos;
        }else {
            List<ClinicStock> clinicStocks = clinicStockRepository.getClinicStockByClinicId(id);

            List<ClinicStockDto> clinicStockDtos = new ArrayList<>();
            for (ClinicStock entity : clinicStocks) {
                if (entity.getProduct().getStatus() == Status.ACTIVE) {
                    ClinicStockDto dto = clinicStockConvert.entityToDto(entity);
                    clinicStockDtos.add(dto);
                }

            }
            return clinicStockDtos;
        }

    }
    @Override
    public List<ClinicStockDto> getClinicStockByProductName(Principal principal, String name) {
        String email = principal.getName();
        UserDto u = userService.findByEmail(email);
        List<ClinicStockDto> clinicStockDtos = new ArrayList<>();
        List<ClinicStock>list  = clinicStockRepository.findByClinic_Users_IdEqualsAndProduct_NameLike(u.getId(),name);
        for (ClinicStock entity:list){
            if (entity.getProduct().getStatus() == Status.ACTIVE){
                ClinicStockDto dto = clinicStockConvert.entityToDto(entity);
                clinicStockDtos.add(dto);
            }
        }
        return clinicStockDtos;
    }

    @Override
    public ClinicStockReponse getAllClinicStock(int pageNo, int pageSize, String sortBy, String sortDir) {
        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
                : Sort.by(sortBy).descending();
        Pageable pageable = PageRequest.of(pageNo-1, pageSize, sort);

        Page<ClinicStock> clinicStocks = clinicStockRepository.findAll(pageable);
        List<ClinicStock> listOfClinicStock = clinicStocks.getContent();

        List<ClinicStockDto> content= listOfClinicStock.stream().map(post -> clinicStockConvert.entityToDto(post)).collect(Collectors.toList());

        ClinicStockReponse clinicStockReponse = new ClinicStockReponse();
        clinicStockReponse.setData(content);
        clinicStockReponse.setCurrent(clinicStocks.getNumber());
        clinicStockReponse.setPageSize(clinicStocks.getSize());
        clinicStockReponse.setTotalElements(clinicStocks.getTotalElements());
        clinicStockReponse.setTotalPages(clinicStocks.getTotalPages());
        clinicStockReponse.setLast(clinicStocks.isLast());

        return clinicStockReponse;
    }

    @Override
    public ClinicStockReponse getAllClinicStockClinic(int pageNo, int pageSize, String sortBy, String sortDir, int id) {

        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
                : Sort.by(sortBy).descending();
        Pageable pageable = PageRequest.of(pageNo-1, pageSize, sort);

        List<ClinicStock> clinicStockDtos = new ArrayList<>();

        Clinic clinic = clinicRepository.getClinicByClinicId(id);

        if(clinic.getType().name().equals("DEPOT")){
            List<ClinicStock> clinicStocks =clinicStockRepository.findAll();
            for (ClinicStock entity:clinicStocks){
                if (entity.getProduct().getStatus() == Status.ACTIVE){
                    clinicStockDtos.add(entity);
                }
            }
        }else {
            List<ClinicStock> clinicStocks = clinicStockRepository.getClinicStockByClinicId(id);
            for (ClinicStock entity : clinicStocks) {
                if (entity.getProduct().getStatus() == Status.ACTIVE) {
                    clinicStockDtos.add(entity);
                }
            }
        }

        int start = Math.min((int)pageable.getOffset(), clinicStockDtos.size());
        int end = Math.min((start + pageable.getPageSize()), clinicStockDtos.size());

        Page<ClinicStock> clinicStocks1  = new PageImpl<ClinicStock>(clinicStockDtos.subList(start, end), pageable, clinicStockDtos.size());

        List<ClinicStock> listOfCustomers = clinicStocks1.getContent();

        List<ClinicStockDto> content= listOfCustomers.stream().map(post -> clinicStockConvert.entityToDto(post)).collect(Collectors.toList());

        ClinicStockReponse clinicStockReponse = new ClinicStockReponse();
        clinicStockReponse.setData(content);
        clinicStockReponse.setCurrent(clinicStocks1.getNumber());
        clinicStockReponse.setPageSize(clinicStocks1.getSize());
        clinicStockReponse.setTotalElements(clinicStocks1.getTotalElements());
        clinicStockReponse.setTotalPages(clinicStocks1.getTotalPages());
        clinicStockReponse.setLast(clinicStocks1.isLast());

        return clinicStockReponse;

    }

    @Override
    public ClinicStockReponse getClinicStockByProductNamePage(Principal principal, int pageNo, int pageSize, String sortBy, String sortDir, String name) {
        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
                : Sort.by(sortBy).descending();
        Pageable pageable = PageRequest.of(pageNo-1, pageSize, sort);

        String email = principal.getName();
        UserDto u = userService.findByEmail(email);

        String check = String.valueOf(u.getClinicDto().getType());
        Page<ClinicStock> list = null;

        if(check.equalsIgnoreCase(Constants.BRANCH)){
            list  = clinicStockRepository.findClinicStockByPage(pageable, u.getClinicDto().getId(), name);
        }
        if(check.equalsIgnoreCase(Constants.DEPOT)){
            list  = clinicStockRepository.findClinicStockByPageOfDepot(pageable, name);
        }

        List<ClinicStock> clinicStocks = list.getContent();
        List<ClinicStockDto> clinicStockDtos = new ArrayList<>();
        for (ClinicStock entity :clinicStocks){
            if (entity.getProduct().getStatus() == Status.ACTIVE){
                ClinicStockDto dto = clinicStockConvert.entityToDto(entity);
                clinicStockDtos.add(dto);
            }
        }
        List<ClinicStockDto> content= clinicStockDtos.stream().map(post -> post).collect(Collectors.toList());

        ClinicStockReponse clinicStockReponse = new ClinicStockReponse();
        clinicStockReponse.setData(content);
        clinicStockReponse.setCurrent(list.getNumber());
        clinicStockReponse.setPageSize(list.getSize());
        clinicStockReponse.setTotalElements(list.getTotalElements());
        clinicStockReponse.setTotalPages(list.getTotalPages());
        clinicStockReponse.setLast(list.isLast());

        return clinicStockReponse;
    }

    @Override
    public ClinicStockReponse getClinicStockByCategoryNamePage(Principal principal, int pageNo, int pageSize, String sortBy, String sortDir, String productCategoryDto) {
        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
                : Sort.by(sortBy).descending();
        Pageable pageable = PageRequest.of(pageNo-1, pageSize, sort);

        String email = principal.getName();
        UserDto u = userService.findByEmail(email);

        String check = String.valueOf(u.getClinicDto().getType());
        Page<ClinicStock> list = null;

        if(check.equalsIgnoreCase(Constants.BRANCH)){
            list  = clinicStockRepository.findClinicStockByCategoryPage(pageable, u.getClinicDto().getId(), productCategoryDto);
        }
        if(check.equalsIgnoreCase(Constants.DEPOT)){
            list  = clinicStockRepository.findClinicStockByCategoryPage(pageable, productCategoryDto);
        }

        List<ClinicStock> clinicStocks = list.getContent();
        List<ClinicStockDto> clinicStockDtos = new ArrayList<>();
        for (ClinicStock entity :clinicStocks){
            if (entity.getProduct().getStatus() == Status.ACTIVE){
                ClinicStockDto dto = clinicStockConvert.entityToDto(entity);
                clinicStockDtos.add(dto);
            }
        }
        List<ClinicStockDto> content= clinicStockDtos.stream().map(post -> post).collect(Collectors.toList());

        ClinicStockReponse clinicStockReponse = new ClinicStockReponse();
        clinicStockReponse.setData(content);
        clinicStockReponse.setCurrent(list.getNumber());
        clinicStockReponse.setPageSize(list.getSize());
        clinicStockReponse.setTotalElements(list.getTotalElements());
        clinicStockReponse.setTotalPages(list.getTotalPages());
        clinicStockReponse.setLast(list.isLast());

        return clinicStockReponse;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }



}
