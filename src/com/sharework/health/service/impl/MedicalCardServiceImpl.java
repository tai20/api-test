package com.sharework.health.service.impl;

import com.sharework.health.convert.MedicalCardConvert;
import com.sharework.health.dto.MedicalCardDto;
import com.sharework.health.dto.MedicalCardQueryDto;
import com.sharework.health.entity.*;
import com.sharework.health.repository.*;
import com.sharework.health.service.MedicalCardService;
import com.sharework.health.service.SlipUsePackageTestService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class MedicalCardServiceImpl implements MedicalCardService {

    private MedicalCardRepository medicalCardRepository;

    private IndicationCardRepository indicationCardRepository;

    private TotalCostRepository totalCostRepository;

    private PrescriptionRepository prescriptionRepository;

    private TestServiceRepository testServiceRepository;

    private SlipUsePackageTestRepository slipUsePackageTestRepository;

    private MedicalCardConvert medicalCardConvert;

    private ScheduleExaminationRepository scheduleExaminationRepository;

    @Override
    public List<MedicalCardDto> findAll() {
        List<MedicalCard> medicalCards = medicalCardRepository.findAll();
        List<MedicalCardDto> medicalCardDtos = new ArrayList<>();

        for (MedicalCard entity : medicalCards
        ) {
            MedicalCardDto dto = medicalCardConvert.entityToDto(entity);
            medicalCardDtos.add(dto);
        }
        return medicalCardDtos;
    }

    @Override
    public MedicalCardDto findById(Integer id) {
        MedicalCard entity = medicalCardRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return medicalCardConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(MedicalCardDto dto) {
        if (dto == null) {
            return false;
        }

        SlipUsePackageTest slipUsePackageTest = null;

        TestService testService = null;

        Prescription prescription = null;

        TotalCost totalCost = null;

        IndicationCard indicationCard = null;


        if (dto.getSlipUsePackageTestDto() != null) {
            slipUsePackageTest = slipUsePackageTestRepository.findById(dto.getSlipUsePackageTestDto().getId()).get();
            if(slipUsePackageTest.getNumberofuse() != null ){
                if(slipUsePackageTest.getNumberofuse() <= 0){
                    return false;
                }
                slipUsePackageTest.setNumberofuse(slipUsePackageTest.getNumberofuse() -1 );
                slipUsePackageTestRepository.save((slipUsePackageTest));
            }
        }
        if (dto.getTestServiceDto() != null) {
            testService = testServiceRepository.findById(dto.getTestServiceDto().getTestservice_id()).get();
        }
        if (dto.getPrescriptionDto() != null) {
            prescription = prescriptionRepository.findById(dto.getPrescriptionDto().getId()).get();
        }
        if (dto.getTotalCostDto() != null) {
            totalCost = totalCostRepository.findById(dto.getTotalCostDto().getId()).get();
        }
        if (dto.getIndicationCardDto() != null) {
            indicationCard = indicationCardRepository.findById(dto.getIndicationCardDto().getId()).get();
        }

        if (testService == null) {
            return false;
        }

        MedicalCard entity = medicalCardConvert.dtoToEntity(dto);
        entity.setId(null);
        entity.setSlipUsePackageTest(slipUsePackageTest);
        entity.setTestService(testService);
        entity.setPrescription(prescription);
        entity.setTotalCost(totalCost);
        entity.setIndicationCard(indicationCard);
        medicalCardRepository.save(entity);
        return true;

    }

    @Override
    public boolean update(Integer id, MedicalCardDto dto) {
        if (dto == null) {
            return false;
        }
        MedicalCard medicalCard = medicalCardRepository.findById(id).get();
        if (medicalCard != null) {
            medicalCard.setDiagnose(dto.getDiagnose());
            medicalCard.setDifferentDiagnose(dto.getDifferentDiagnose());
            medicalCardRepository.save(medicalCard);
            return true;
        }
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public MedicalCardDto getMedicalCardAfterInsert() {
        MedicalCard entity = medicalCardRepository.getMedicalCardAfterInsert();
        if (entity == null) {
            return null;
        }
        return medicalCardConvert.entityToDto(entity);
    }

    @Override
    public List<MedicalCardQueryDto> getAllMedicalCard() {
        /*List<MedicalCardQueryDto> medicalCardQueryDtos = medicalCardRepository.getAllMedicalCard();
        if (medicalCardQueryDtos.isEmpty() || medicalCardQueryDtos == null) {
            return null;
        }
        return medicalCardQueryDtos;*/
        return null;
    }

    @Override
    public MedicalCardDto getMedicalCardByScheduleExaminationId(Integer scheduleExaminationId) {
        MedicalCard medicalCard = medicalCardRepository.getMedicalCardByScheduleExaminationId(scheduleExaminationId);
        if (medicalCard == null) {
            return null;
        }
        MedicalCardDto medicalCardDto = medicalCardConvert.entityToDto(medicalCard);
        return medicalCardDto;
    }

    @Override
    public List<MedicalCardDto> getMedicalCardByCustomerId(Integer customerId) {
        List<MedicalCard> medicalCards = medicalCardRepository.getMedicalCardByCustomerId(customerId);
        List<MedicalCardDto> medicalCardDtos = new ArrayList<>();

        if (medicalCards == null) {
            return null;
        }
        for (MedicalCard medicalCard : medicalCards) {
            MedicalCardDto medicalCardDto = medicalCardConvert.entityToDto(medicalCard);
            medicalCardDtos.add(medicalCardDto);
        }
        return medicalCardDtos;
    }

    @Override
    public MedicalCardDto getMedicalCardLatelyByCustomerId(Integer customerId) {
        MedicalCard medicalCard = medicalCardRepository.getMedicalCardLatelyByCustomerId(customerId);
        if (medicalCard == null) {
            return null;
        }
        MedicalCardDto medicalCardDto = medicalCardConvert.entityToDto(medicalCard);
        return medicalCardDto;
    }

    @Override
    public boolean updateMedicalCardInScheduleExamination(Integer scheduleExaminationId, MedicalCardDto dto){
        ScheduleExamination scheduleExamination = scheduleExaminationRepository.findById(scheduleExaminationId).get();
        MedicalCard medicalCard = medicalCardRepository.findById(dto.getId()).get();
        if(scheduleExamination != null){
            scheduleExamination.setMedicalCard(medicalCard);
            scheduleExaminationRepository.save(scheduleExamination);
            return true;
        }
        return false;
    }



//    @Override
//    public boolean updateMedicalCardByIndicationCardId(Integer medicalCardId, MedicalCardDto dto, Integer indicationCardId) {
//        if (dto == null) {
//            return false;
//        }
//        MedicalCard medicalCard = medicalCardRepository.findById(medicalCardId).get();
//        IndicationCard indicationCard = indicationCardRepository.findById(indicationCardId).get();
//        if (medicalCard != null) {
//            medicalCard.setIndicationCard(indicationCard);
//            medicalCardRepository.save(medicalCard);
//            return true;
//        }
//        return false;
//    }

}
