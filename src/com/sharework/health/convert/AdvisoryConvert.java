package com.sharework.health.convert;

import com.sharework.health.dto.AdvisoryDto;
import com.sharework.health.dto.CustomerDto;
import com.sharework.health.dto.TotalCostDto;
import com.sharework.health.dto.UserDto;
import com.sharework.health.entity.Advisory;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class AdvisoryConvert implements BaseConvert<Advisory, AdvisoryDto> {
    @Override
    public AdvisoryDto entityToDto(Advisory entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, AdvisoryDto.class)
                .setCustomerDto(modelMapper.map(entity.getCustomer(), CustomerDto.class))
                .setUserDto(modelMapper.map(entity.getUser(), UserDto.class));

    }

    @Override
    public Advisory dtoToEntity(AdvisoryDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, Advisory.class);
    }
}
