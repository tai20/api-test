package com.sharework.health.controller.admin;

import com.sharework.health.dto.*;
import com.sharework.health.service.ExaminationCardService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/admin/examinationcards")
@AllArgsConstructor
public class ExaminationCardController {

    private ExaminationCardService examinationCardService;

    @GetMapping("")
    public Object getAllExaminationCard(){
        //List<ExaminationCardDto> examinationCardDtos = examinationCardService.findAll();
        List<ExaminationCardCustomerQueryDto> examinationCardDtos = examinationCardService.getExaminationCardWithCustomer();
        if (examinationCardDtos.isEmpty() || examinationCardDtos == null){
            return new ResponseEntity<>("Không có phiếu khám nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(examinationCardDtos, HttpStatus.OK);
    }

    @PostMapping("")
    public Object addExaminationCard(@RequestBody ExaminationCardDto dto){
        if (dto == null){
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = examinationCardService.insert(dto);
        if (!result){
            return new ResponseEntity<>("Thêm phiếu khám thất bại", HttpStatus.BAD_REQUEST);
        }
        ExaminationCardDto dto1 = examinationCardService.findExaminationCardAfterInsert();
        ExaminationCardAfterInsertDto examinationCardAfterInsertDto = new ExaminationCardAfterInsertDto()
                .setMessage("Thêm phiếu khám thành công")
                .setExaminationCardDto(dto1);
        return new ResponseEntity<>(examinationCardAfterInsertDto, HttpStatus.CREATED);
    }

    @PutMapping("{examinationcard_id}")
    public Object updateExaminationCard(@PathVariable("examinationcard_id") Integer examinationCardId, @RequestBody ExaminationCardDto dto){
        if (dto == null){
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = examinationCardService.update(examinationCardId, dto);
        if (!result){
            return new ResponseEntity<>("Cập nhật phiếu khám thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhật phiếu khám thành công", HttpStatus.OK);
    }

    @PostMapping("addExaminationCardDetail")
    public Object addExaminationCardDetail(@RequestBody ExaminationCardDetailDto dto){
        if (dto == null){
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = examinationCardService.addExaminationCardDetail(dto);
        if (!result){
            return new ResponseEntity<>("Thêm chi tiết phiếu khám thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm chi tiết phiếu khám thành công", HttpStatus.CREATED);
    }

    @PutMapping("confirmExamination/{examinationcard_id}")
    public Object confirmExamination(@PathVariable("examinationcard_id") Integer examinationCardId, @RequestParam("user_id") String userId,
                                     @RequestParam("service_id") String serviceId){
        boolean result = examinationCardService.confirmExamination(examinationCardId, Integer.parseInt(serviceId), Integer.parseInt(userId));
        if (!result){
            return new ResponseEntity<>("Phiếu khám đã được xác nhận rồi hoặc không có phiếu khám có thông tin cần tìm", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Phiếu khám được xác nhận thành công", HttpStatus.OK);
    }

//    @PutMapping("activeExaminationCard/{examinationcard_id}")
//    public Object activeExaminationCard(@PathVariable("examinationcard_id") Integer examinationCardId, @RequestParam("signature") MultipartFile signature) throws IOException {
//        boolean result = examinationCardService.activeExaminationCard(examinationCardId, signature);
//        if (!result){
//            return new ResponseEntity<>("Phiếu khám đã được kích hoạt rồi hoặc không tồn tại phiếu khám này hoặc kkhoog có vân tay", HttpStatus.BAD_REQUEST);
//        }
//        return new ResponseEntity<>("Phiếu khám được kích hoạt thành công", HttpStatus.OK);
//    }

    @PutMapping("activeExaminationCard/{examinationcard_id}")
    public Object activeExaminationCard(@PathVariable("examinationcard_id") Integer examinationCardId) throws IOException {
        boolean result = examinationCardService.activeExaminationCard(examinationCardId);
        if (!result){
            return new ResponseEntity<>("Phiếu khám đã được kích hoạt rồi hoặc không tồn tại phiếu khám này", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Phiếu khám được kích hoạt thành công", HttpStatus.OK);
    }


    @GetMapping("getExaminationCardBySlipUse/{slipUse_id}")
    public Object getExaminationCardBySlipUse(@PathVariable("slipUse_id") Integer slipUseId){

        List<ExaminationCardDto> examinationCardDtos = examinationCardService.findAllBySlipUse(slipUseId);
        if (examinationCardDtos.isEmpty() || examinationCardDtos == null){
            return new ResponseEntity<>("Phiếu sử dụng chưa có lịch khám nào phiếu khám ", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(examinationCardDtos, HttpStatus.OK);
    }

    @GetMapping("getExaminationCardDetailByExaminationCard/{examinationcard_id}")
    public Object getExaminationCardDetailByExaminationCard(@PathVariable("examinationcard_id") Integer examinationCardId){

        List<ExaminationCardDetailDto> examinationCardDetailDtos = examinationCardService.findAllByExaminationCard(examinationCardId);
        if (examinationCardDetailDtos.isEmpty() || examinationCardDetailDtos == null){
            return new ResponseEntity<>("Phiếu khám này chưa có chi tiết", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(examinationCardDetailDtos, HttpStatus.OK);
    }

    @GetMapping("getExaminationCardDetailByUser/{user_id}")
    public Object getExaminationCardDetailByUser(@PathVariable("user_id") Integer userId){

        List<ExaminationCardDetailQueryDto> examinationCardDetailQueryDtos = examinationCardService.findAllByUser(userId);
        if (examinationCardDetailQueryDtos.isEmpty() || examinationCardDetailQueryDtos == null){
            return new ResponseEntity<>("Không có phiếu khám nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(examinationCardDetailQueryDtos, HttpStatus.OK);
    }
}
