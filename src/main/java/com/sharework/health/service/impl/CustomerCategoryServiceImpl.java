package com.sharework.health.service.impl;

import com.sharework.health.convert.CustomerCategoryConvert;
import com.sharework.health.dto.CustomerCategoryDto;
import com.sharework.health.entity.CustomerCategory;
import com.sharework.health.repository.CustomerCategoryRepository;
import com.sharework.health.service.CustomerCategoryService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(rollbackOn = Exception.class)
@AllArgsConstructor
public class CustomerCategoryServiceImpl implements CustomerCategoryService {

    private CustomerCategoryRepository customerCategoryRepository;

    private CustomerCategoryConvert customerCategoryConvert;

    @Override
    public List<CustomerCategoryDto> findAll() {
        List<CustomerCategory> customerCategories = customerCategoryRepository.findAll();
        List<CustomerCategoryDto> customerCategoryDtos = new ArrayList<>();
        for (CustomerCategory entity: customerCategories
             ) {
            CustomerCategoryDto dto = customerCategoryConvert.entityToDto(entity);
            customerCategoryDtos.add(dto);
        }
        return customerCategoryDtos;
    }

    @Override
    public CustomerCategoryDto findById(Integer id) {
        CustomerCategory entity = customerCategoryRepository.findById(id).get();
        if (entity == null){
            return null;
        }
        return customerCategoryConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(CustomerCategoryDto dto) {
        if (dto == null){
            return false;
        }
        CustomerCategory entity = customerCategoryConvert.dtoToEntity(dto);
        customerCategoryRepository.save(entity);
        return true;
    }

    @Override
    public boolean update(Integer id, CustomerCategoryDto dto) {
        CustomerCategory oldCustomerCategory = customerCategoryRepository.findById(id).get();
        if (oldCustomerCategory == null){
            return false;
        }
        if (dto == null){
            return false;
        }
        oldCustomerCategory.setName(dto.getName());
        oldCustomerCategory.setDescription(dto.getDescription());
        customerCategoryRepository.save(oldCustomerCategory);
        return true;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }
}
