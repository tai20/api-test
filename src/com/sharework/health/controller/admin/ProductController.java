package com.sharework.health.controller.admin;

import com.sharework.health.dto.ProductDto;
import com.sharework.health.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/products")
@AllArgsConstructor
public class ProductController {

    private ProductService productService;

    @GetMapping("")
    public ResponseEntity<List<ProductDto>> getAllProduct() {
        List<ProductDto> productDtos = productService.findAll();
        if (productDtos.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(productDtos,HttpStatus.OK);
    }

    @GetMapping("/{product_id}")
    public ResponseEntity<ProductDto> getProductById(@PathVariable("product_id") Integer id) {
        ProductDto productDto = productService.findById(id);
        if (productDto == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(productDto,HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<ProductDto> addProduct(@RequestBody ProductDto dto,
                                           BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        productService.insert(dto);
        return new ResponseEntity<>(dto,HttpStatus.CREATED);
    }

    @PutMapping("/{product_id}")
    public ResponseEntity<ProductDto> updateProduct(@PathVariable("product_id") Integer id , @RequestBody ProductDto dto,
                                                  BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        productService.update(id, dto);
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }

    @DeleteMapping("/{product_id}")
    public Object deleteProduct(@PathVariable("product_id") Integer id) {
        boolean result = productService.delete(id);
        if (!result){
            return new ResponseEntity<>("Xóa không thành công", HttpStatus.OK);
        }
        return new ResponseEntity<>("Xóa thành công", HttpStatus.OK);
    }

    @PostMapping("searchName")
    public Object searchProductByProductName(@RequestParam("product_name") String name){
        System.out.println(name);
        List<ProductDto> productDtos = productService.searchProductByProductName(name);
        try {
            if (productDtos == null){
                return new ResponseEntity<>("Không tìm thấy sản phẩm",HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(productDtos,HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @GetMapping("findAllByClinic/{clinic_id}")
    public Object findAllByClinic(@PathVariable("clinic_id") Integer clinicId){
        List<ProductDto> productDtos = productService.findAllByClinic(clinicId);
        try {
            if (productDtos == null){
                return new ResponseEntity<>("Không tìm thấy sản phẩm",HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(productDtos,HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @PostMapping("findAllByClinicName")
    public Object findAllByClinicName(@RequestParam("clinic_name") String clinicName){
        List<ProductDto> productDtos = productService.findAllByClinicName(clinicName);
        try {
            if (productDtos == null){
                return new ResponseEntity<>("Không tìm thấy sản phẩm",HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(productDtos,HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
