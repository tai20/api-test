package com.sharework.health.dto;

import com.sharework.health.entity.TransferReceipt;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TransferReceiptDetailsDto {

    private TransferReceiptDto transferReceiptDto;

    private ClinicStockDto clinicStockDto;

    private Integer number_product_transfer;

    private ProductBatchDto productBatchDto;
}
