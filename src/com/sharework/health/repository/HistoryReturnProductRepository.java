package com.sharework.health.repository;

import com.sharework.health.entity.HistoryReturnProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HistoryReturnProductRepository extends JpaRepository<HistoryReturnProduct, Integer> {

    @Query(nativeQuery = true, value = "select * from historyreturnproduct h join slipreturnproduct s on h.slipreturnproduct_id = s.id where s.customer_id= :customerId")
    List<HistoryReturnProduct> findAllByCustomerId(Integer customerId);

    @Query(nativeQuery = true, value = "select * from historyreturnproduct order by id desc limit 1")
    HistoryReturnProduct getHistoryReturnProductAfterInsert();
}
