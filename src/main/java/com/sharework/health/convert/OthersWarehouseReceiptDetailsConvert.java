package com.sharework.health.convert;


import com.sharework.health.dto.*;
import com.sharework.health.entity.OthersWarehouseReceiptDetails;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class OthersWarehouseReceiptDetailsConvert implements BaseConvert<OthersWarehouseReceiptDetails, OthersWarehouseReceiptDetailsDto>{

    @Override
    public OthersWarehouseReceiptDetailsDto entityToDto(OthersWarehouseReceiptDetails entity){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, OthersWarehouseReceiptDetailsDto.class)
                .setOthersWarehouseReceiptDto(modelMapper.map(entity.getOthersWarehouseReceipt(), OthersWarehouseReceiptDto.class))
                .setClinicStockDto(modelMapper.map(entity.getClinicStock(), ClinicStockDto.class))
                .setProductBatchDto(modelMapper.map(entity.getProductBatchId(), ProductBatchDto.class));

    }

    @Override
    public OthersWarehouseReceiptDetails dtoToEntity(OthersWarehouseReceiptDetailsDto dto){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, OthersWarehouseReceiptDetails.class);
    }
}
