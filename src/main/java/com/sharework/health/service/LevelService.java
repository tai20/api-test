package com.sharework.health.service;


import com.sharework.health.dto.LevelDto;
import com.sharework.health.entity.Level;

import java.math.BigDecimal;

public interface LevelService  extends BaseService<LevelDto, Integer>{
    Level findNameCustomerLever(BigDecimal i);
}
