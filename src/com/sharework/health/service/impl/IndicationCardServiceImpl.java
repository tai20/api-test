package com.sharework.health.service.impl;

import com.sharework.health.convert.IndicationCardConvert;
import com.sharework.health.convert.IndicationCardDetailConvert;
import com.sharework.health.dto.IndicationCardDetailDto;
import com.sharework.health.dto.IndicationCardDto;
import com.sharework.health.entity.*;
import com.sharework.health.repository.*;
import com.sharework.health.service.IndicationCardService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class IndicationCardServiceImpl implements IndicationCardService {

    private static final Path CURRENT_FOLDER = Paths.get(System.getProperty("user.dir"));

    private IndicationCardRepository indicationCardRepository;

    private IndicationCardConvert indicationCardConvert;

    private CategoryIndicationCardRepository categoryIndicationCardRepository;

    private IndicationCardDetailRepository indicationCardDetailRepository;

    private IndicationCardDetailConvert indicationCardDetailConvert;

    private UserRepository userRepository;

    private CustomerRepository customerRepository;

    private MedicalCardRepository medicalCardRepository;

    private SlipUsePackageTestDetailRepository slipUsePackageTestDetailRepository;

    private SlipUsePackageTestRepository slipUsePackageTestRepository;

    @Override
    public List<IndicationCardDto> findAll() {
        List<IndicationCard> indicationCards = indicationCardRepository.findAll();
        List<IndicationCardDto> indicationCardDtos = new ArrayList<>();
        for (IndicationCard entity : indicationCards
        ) {
            IndicationCardDto dto = indicationCardConvert.entityToDto(entity);
            indicationCardDtos.add(dto);
        }
        return indicationCardDtos;
    }

    @Override
    public IndicationCardDto findById(Integer id) {
        IndicationCard entity = indicationCardRepository.findById(id).get();

        if (entity == null) {
            return null;
        }
        return indicationCardConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(IndicationCardDto dto) {
        if (dto == null) {
            return false;
        }
        User user = userRepository.findById(dto.getUserDto().getId()).get();
        if (user == null) {
            return false;
        }
        IndicationCard entity = indicationCardConvert.dtoToEntity(dto);
        entity.setId(null);
        entity.setCreatedDate(LocalDateTime.now());
        entity.setUser(user);
        indicationCardRepository.save(entity);

        return true;
    }

    @Override
    public boolean update(Integer id, IndicationCardDto dto) {
        if (dto == null) {
            return false;
        }
        User user = userRepository.findById(dto.getUserDto().getId()).get();

        IndicationCard entity = indicationCardRepository.findById(id).get();
        if (entity != null) {
            if (user != null) {
                entity.setReasonIndication(dto.getReasonIndication());
                entity.setUser(user);
                indicationCardRepository.save(entity);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public List<IndicationCardDto> getIndicationCardByCustomer(Integer customerId) {
        List<IndicationCard> indicationCards = indicationCardRepository.getIndicationCardByCustomer(customerId);
        List<IndicationCardDto> indicationCardDtos = new ArrayList<>();

        for (IndicationCard entity : indicationCards
        ) {
            IndicationCardDto dto = indicationCardConvert.entityToDto(entity);
            indicationCardDtos.add(dto);
        }
        return indicationCardDtos;
    }

    @Override
    public List<IndicationCardDto> getIndicationCardByUser(Integer userId) {
        List<IndicationCard> indicationCards = indicationCardRepository.getIndicationCardByUser(userId);
        List<IndicationCardDto> indicationCardDtos = new ArrayList<>();

        for (IndicationCard entity : indicationCards
        ) {
            IndicationCardDto dto = indicationCardConvert.entityToDto(entity);
            indicationCardDtos.add(dto);
        }
        return indicationCardDtos;
    }

    @Override
    public boolean addIndicationCardDetail(IndicationCardDetailDto dto, Integer medicalCardId) {
        if (dto == null) {
            return false;
        }
        IndicationCard indicationCard = indicationCardRepository.findById(dto.getIndicationCardDto().getId()).get();
        CategoryIndicationCard categoryIndicationCard = categoryIndicationCardRepository.findById(dto.getCategoryIndicationCardDto().getId()).get();
        MedicalCard medicalCard = medicalCardRepository.findById(medicalCardId).get();
        SlipUsePackageTest slipUsePackageTest = null;
        if(medicalCard.getSlipUsePackageTest()!=null){
            slipUsePackageTest = slipUsePackageTestRepository.findById(medicalCard.getSlipUsePackageTest().getId()).get();
        }else{
            slipUsePackageTest = null;
        }

        if (indicationCard != null && categoryIndicationCard != null) {
            IndicationCardDetail entity = indicationCardDetailConvert.dtoToEntity(dto);
            entity.setIndicationCard(indicationCard);
            entity.setCategoryIndicationCard(categoryIndicationCard);
            if(slipUsePackageTest == null ){
                entity.setStatus("Fee");
                indicationCardDetailRepository.save(entity);
                return true;
            }
            else{
                List<SlipUsePackageTestDetail> slipUsePackageTestDetails = slipUsePackageTestDetailRepository.getSlipUsePackageTestDetailBySlipUsePackageId(slipUsePackageTest.getId());
                if(slipUsePackageTestDetails != null){
                    for (SlipUsePackageTestDetail slipUsePackageTest1 : slipUsePackageTestDetails){
                        if(slipUsePackageTest1.getCategoryIndicationCard().getId() == categoryIndicationCard.getId()){
                            entity.setStatus("Free");
                            indicationCardDetailRepository.save(entity);
                            slipUsePackageTest1.setNumberofuse(slipUsePackageTest1.getNumberofuse() - 1);
                            slipUsePackageTestRepository.save(slipUsePackageTest);
                            return true;
                        }
                    }
                }
                entity.setStatus("Fee");
                indicationCardDetailRepository.save(entity);
                return true;
            }
        }
        return false;

    }


    @Override
    public boolean importFileIndicationCard(Integer indicationCardId, Integer categoryIndicationId, MultipartFile file) throws IOException {
        IndicationCardDetail entity = indicationCardDetailRepository
                .getIndicationCardDetailByIndicationCardAndCategoryIndicationCard(indicationCardId, categoryIndicationId);
        if (entity == null) {
            return false;
        }

        Path staticPath = Paths.get("static");
        Path imagePath = Paths.get("fileIndicationCards");

        if (!Files.exists(CURRENT_FOLDER.resolve(staticPath).resolve(imagePath))) {
            Files.createDirectories(CURRENT_FOLDER.resolve(staticPath).resolve(imagePath));
        }
        if (entity.getFileIndication() == null) {
            Path path = Paths.get("static/fileIndicationCards/");
            InputStream inputStream = file.getInputStream();
            Files.copy(inputStream, path.resolve(file.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
            entity.setFileIndication(file.getOriginalFilename());
            indicationCardDetailRepository.save(entity);
            return true;
        }
        return false;
    }

    @Override
    public List<IndicationCardDetailDto> getIndicationCardDetailByIndicationCard(Integer indicationCardId) {
        List<IndicationCardDetail> indicationCardDetails = indicationCardDetailRepository.getIndicationCardDetailByIndicationCard(indicationCardId);
        List<IndicationCardDetailDto> indicationCardDetailDtos = new ArrayList<>();

        for (IndicationCardDetail entity : indicationCardDetails
        ) {
            IndicationCardDetailDto dto = indicationCardDetailConvert.entityToDto(entity);
            indicationCardDetailDtos.add(dto);
        }
        return indicationCardDetailDtos;
    }

    @Override
    public IndicationCardDto getIndicationCardLatelyByCustomer(Integer customerId) {
        IndicationCard entity = indicationCardRepository.getIndicationCardLatelyByCustomer(customerId);
        if (entity == null) {
            return null;
        }
        return indicationCardConvert.entityToDto(entity);
    }

    @Override
    public IndicationCardDto getIndicationCardByMedicalCardId(Integer medicalCardId) {
        IndicationCard entity = indicationCardRepository.getIndicationCardByMedicalCardId(medicalCardId);
        if (entity == null) {
            return null;
        }
        return indicationCardConvert.entityToDto(entity);
    }

    @Override
    public List<IndicationCardDetailDto> getIndicationCardDetailByMedicalCardId(Integer medicalCardId) {
        List<IndicationCardDetail> indicationCardDetails = indicationCardDetailRepository.getIndicationCardDetailByMedicalCardId(medicalCardId);
        List<IndicationCardDetailDto> indicationCardDetailDtos = new ArrayList<>();

        for (IndicationCardDetail entity : indicationCardDetails
        ) {
            IndicationCardDetailDto dto = indicationCardDetailConvert.entityToDto(entity);
            indicationCardDetailDtos.add(dto);
        }
        return indicationCardDetailDtos;
    }

    //////////
    @Override
    public boolean updateIndicationCardInMedicalCard(Integer medicalCardId, IndicationCardDto dto) {
        MedicalCard medicalCard = medicalCardRepository.findById(medicalCardId).get();
        IndicationCard indicationCard = indicationCardRepository.findById(dto.getId()).get();
        if (medicalCard != null && indicationCard != null) {
            medicalCard.setIndicationCard(indicationCard);
            medicalCardRepository.save(medicalCard);
            return true;
        }
        return false;
    }

    @Override
    public IndicationCardDto getIndicationCardAfterInsert() {
        IndicationCard entity = indicationCardRepository.getIndicationCardAfterInsert();
        if (entity == null) {
            return null;
        }
        return indicationCardConvert.entityToDto(entity);
    }

    @Override
    public boolean updateIndicationCardDetailABoutUtralsoud(Integer indicationCardId, Integer categoryIndicationCardId,String liverandkidney,String bilebuctsAndgallbladderandbladder,String abdominalfluid,
                                                            String uterus,String dkts,String density,String ovary,String endometrium,String cervical,
                                                            String fornixfluid,String smallfetusultrasoundinformation1,String smallfetusultrasoundinformation2,String blackandwhitefetalultrasoundinformation,
                                                            String colordoppleroffetalbloodvesselsandnuchaltranslucency,  String pidmuterus,String unbt,
                                                            String interstitial,String tntc,String colordopplerofFetalbloodvessels,String pregnancyultrasound4d,
                                                            String fetalanatomyheadfaceneck,String fetalanatomychest,String fetalanatomystomach,String fetalanatomythefourlimbs,
                                                            String mlt,String bigblackandwhitefetalultrasoundinformation,String conclude,String ctc_dk,
                                                            String bldb,String blda,String thewomb,MultipartFile[] listImages) throws IOException{

        Path staticPath = Paths.get("static");
        Path imagePath = Paths.get("fileIndicationCards");

        if (!Files.exists(CURRENT_FOLDER.resolve(staticPath).resolve(imagePath))) {
            Files.createDirectories(CURRENT_FOLDER.resolve(staticPath).resolve(imagePath));
        }
        Path path = Paths.get("static/fileIndicationCards/");
        if (indicationCardId != null || categoryIndicationCardId != null || (listImages != null && listImages.length > 0)) {
            IndicationCardDetail entity = indicationCardDetailRepository.getIndicationCardDetailByIndicationCardAndCategoryIndicationCard(indicationCardId ,categoryIndicationCardId);
            if (entity != null) {
                if(liverandkidney != null){
                    entity.setLiverandkidney(liverandkidney);
                }
                if(bilebuctsAndgallbladderandbladder != null){
                    entity.setBilebuctsAndgallbladderandbladder(bilebuctsAndgallbladderandbladder);
                }
                if(abdominalfluid != null){
                    entity.setAbdominalfluid(abdominalfluid);
                }
                if(uterus != null){
                    entity.setUterus(uterus);
                }
                if(dkts != null){
                    entity.setDkts(dkts);
                }
                if(density!= null){
                    entity.setDensity(density);
                }
                if(ovary!= null){
                    entity.setOvary(ovary);
                }
                if(endometrium!= null){
                    entity.setEndometrium(endometrium);
                }
                if(cervical != null){
                    entity.setCervical(cervical);
                }
                if(fornixfluid!= null){
                    entity.setFornixfluid(fornixfluid);
                }
                if(smallfetusultrasoundinformation1!=null){
                    entity.setSmallfetusultrasoundinformation1(smallfetusultrasoundinformation1);
                }
                if(smallfetusultrasoundinformation2!=null){
                    entity.setSmallfetusultrasoundinformation2(smallfetusultrasoundinformation2);
                }
                if(blackandwhitefetalultrasoundinformation!=null){
                    entity.setBlackandwhitefetalultrasoundinformation(blackandwhitefetalultrasoundinformation);
                }
                if(colordoppleroffetalbloodvesselsandnuchaltranslucency!=null){
                    entity.setColordoppleroffetalbloodvesselsandnuchaltranslucency(colordoppleroffetalbloodvesselsandnuchaltranslucency);
                }
                if(pidmuterus!=null){
                    entity.setPidmuterus(pidmuterus);
                }
                if(unbt!=null){
                    entity.setUnbt(unbt);
                }
                if(interstitial!=null){
                    entity.setInterstitial(interstitial);
                }
                if(tntc!= null){
                    entity.setTntc(tntc);
                }
                if(colordopplerofFetalbloodvessels!=null){
                    entity.setColordopplerofFetalbloodvessels(colordopplerofFetalbloodvessels);
                }
                if(pregnancyultrasound4d!=null){
                    entity.setPregnancyultrasound4d(pregnancyultrasound4d);
                }
                if(fetalanatomyheadfaceneck!=null){
                    entity.setFetalanatomyheadfaceneck(fetalanatomyheadfaceneck);
                }
                if(fetalanatomychest!=null){
                    entity.setFetalanatomychest(fetalanatomychest);
                }
                if(fetalanatomystomach!=null){
                    entity.setFetalanatomystomach(fetalanatomystomach);
                }
                if(fetalanatomythefourlimbs!=null){
                    entity.setFetalanatomythefourlimbs(fetalanatomythefourlimbs);
                }
                if(mlt!=null){
                    entity.setMlt(mlt);
                }
                if(bigblackandwhitefetalultrasoundinformation!=null){
                    entity.setBigblackandwhitefetalultrasoundinformation(bigblackandwhitefetalultrasoundinformation);
                }
                if(conclude!=null){
                    entity.setConclude(conclude);
                }
                if(ctc_dk!=null){
                    entity.setCtc_dk(ctc_dk);
                }
                if(bldb!=null){
                    entity.setBldb(bldb);
                }
                if(blda!=null){
                    entity.setBlda(blda);
                }
                if(thewomb!=null){
                    entity.setThewomb(thewomb);
                }
                Set<String> listFiles =  entity.getListImages();
                for (MultipartFile file: listImages
                ) {
                    try {
                        InputStream inputStream = file.getInputStream();
                        Files.copy(inputStream, path.resolve(file.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
                        listFiles.add(file.getOriginalFilename());
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
                entity.setListImages(listFiles);
                indicationCardDetailRepository.save(entity);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean updateStatusIndicationCardDetail(Integer medicalCardId, Integer indicationCardId,
                                                    IndicationCardDetailDto dto) {
        if (dto == null) {
            return false;
        }

        MedicalCard medicalCard = medicalCardRepository.findById(medicalCardId).get();
        IndicationCard indicationCard = indicationCardRepository.findById(indicationCardId).get();
        SlipUsePackageTest slipUsePackageTest = slipUsePackageTestRepository.getSlipUsePackageTestByMedicalCardId(medicalCardId);

        List<IndicationCardDetail> indicationCardDetails = indicationCardDetailRepository.getIndicationCardDetailByMedicalCardId(medicalCardId);

        if (slipUsePackageTest != null) {
            List<SlipUsePackageTestDetail> slipUsePackageTestDetails = slipUsePackageTestDetailRepository.getSlipUsePackageTestDetailBySlipUsePackageId(slipUsePackageTest.getId());
            if (indicationCard != null || medicalCard != null) {
                if (indicationCardDetails != null) {
                    for (IndicationCardDetail indicationCardDetail : indicationCardDetails) {
                        for (SlipUsePackageTestDetail slipUsePackageTestDetail : slipUsePackageTestDetails) {
                            if (slipUsePackageTestDetail.getCategoryIndicationCard().getId() == indicationCardDetail.getCategoryIndicationCard().getId()) {
                                indicationCardDetail.setStatus("free");
                            } else {
                                indicationCardDetail.setStatus("fee");
                            }

                        }
                        indicationCardDetailRepository.save(indicationCardDetail);
                        return true;
                    }
                }
            }
        } else {
            if (indicationCard != null && medicalCard != null) {
                if (indicationCardDetails != null) {
                    for (IndicationCardDetail indicationCardDetail : indicationCardDetails) {
                        indicationCardDetail.setStatus("fee");
                        indicationCardDetailRepository.save(indicationCardDetail);
                        return true;
                    }
                }
            }
        }

        return false;
    }
    @Override
    public IndicationCardDetailDto getIndicationCardDetailByAfterUpdate(Integer indicationCardId, Integer categoryIndicationCardId){
        if(indicationCardId!= null && categoryIndicationCardId != null){
            IndicationCardDetail indicationCardDetail = indicationCardDetailRepository.getIndicationCardDetailByIndicationCardAndCategoryIndicationCard(indicationCardId, categoryIndicationCardId);
            if(indicationCardDetail == null){
                return null;
            }

            return indicationCardDetailConvert.entityToDto(indicationCardDetail);
        }
        return null;
    }
}
