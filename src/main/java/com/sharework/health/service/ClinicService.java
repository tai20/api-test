package com.sharework.health.service;

import com.sharework.health.dto.ClinicDto;

public interface ClinicService extends BaseService<ClinicDto, Integer>{

    ClinicDto searchClinicByClinicName(String clinicName);
}
