package com.sharework.health.service;

import com.sharework.health.dto.ProductServiceDetailDto;
import com.sharework.health.dto.ServiceDto;

import java.util.List;

public interface ServiceService extends BaseService<ServiceDto, Integer> {

    boolean addProductServiceDetail(ProductServiceDetailDto dto);

    List<ProductServiceDetailDto> findAllByServiceId(Integer serviceId);

    ServiceDto findServiceAfterInsert();
}
