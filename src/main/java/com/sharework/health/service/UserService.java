package com.sharework.health.service;

import com.sharework.health.dto.LoginDto;
import com.sharework.health.dto.PasswordDto;
import com.sharework.health.dto.UserDto;

import java.util.List;

public interface UserService extends BaseService<UserDto, Integer> {

    List<UserDto> searchUserByUsername(String name);
    
    UserDto getProfile();

    String getRoleName();

    String changePassword(PasswordDto dto);

    LoginDto resetPassword(String email);

    LoginDto showLoginWhenAddUser(UserDto dto);

    UserDto findByEmail(String email);

    boolean updateProfile(UserDto dto);
}
