package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "ProductBatch")
public class ProductBatch {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @ManyToOne
    @JoinColumn(name = "productId")
    private Product productId;

    private Integer quantity;

    private LocalDate expiry;

    @OneToMany(mappedBy = "productBatchId", fetch = FetchType.LAZY)
    private List<OthersWarehouseReceiptDetails> othersWarehouseReceiptDetails;

    @OneToMany(mappedBy = "productBatchId", fetch = FetchType.LAZY)
    private List<TransferReceiptDetails> transferReceiptDetails;

    @OneToMany(mappedBy = "productBatchId", fetch = FetchType.LAZY)
    private List<ImportReceiptDetails> importReceiptDetails;

    @OneToMany(mappedBy = "productBatchId", fetch = FetchType.LAZY)
    private List<ExportReceiptDetails> exportReceiptDetails;
}
