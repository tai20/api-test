package com.sharework.health.dto;

import com.sharework.health.entity.ReceiptStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class StatisticsExportedProductsQueryDto {
    private Integer product_id;

    private String product_name;

    private String product_category;

    private BigDecimal export_price;

    private String unit;

    private Integer quantity_export;

    private Integer warehouse_id;

    private String warehouse_name;


}
