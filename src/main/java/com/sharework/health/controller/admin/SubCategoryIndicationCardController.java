package com.sharework.health.controller.admin;

import com.sharework.health.dto.SubCategoryIndicationCardDto;
import com.sharework.health.service.SubCategoryIndicationCardService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/admin/subcategoryindicationcards")
@AllArgsConstructor
public class SubCategoryIndicationCardController {

    private SubCategoryIndicationCardService subCategoryIndicationCardService;

    @GetMapping("")
    public Object getAllSubCategoryIndicationCard() {
        List<SubCategoryIndicationCardDto> subCategoryIndicationCardDtos = subCategoryIndicationCardService.findAll();
        if (subCategoryIndicationCardDtos.isEmpty() || subCategoryIndicationCardDtos == null) {
            return new ResponseEntity<>("Không có dữ liệu", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(subCategoryIndicationCardDtos, HttpStatus.OK);
    }

    @GetMapping("/{subCategoryIndication_id}")
    public Object getSubCategoryIndicationCardById(@PathVariable("subCategoryIndication_id") Integer id) {
        SubCategoryIndicationCardDto dto = subCategoryIndicationCardService.findById(id);
        if (dto == null) {
            return new ResponseEntity<>("Không tìm thấy", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping("")
    public Object addSubCategoryIndicationCard(@RequestBody SubCategoryIndicationCardDto dto,
                                               BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = subCategoryIndicationCardService.insert(dto);
        if (!result) {
            return new ResponseEntity<>("Thêm chỉ định thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm chỉ định thành công", HttpStatus.CREATED);
    }

    @PutMapping("/{subCategoryIndication_id}")
    public Object updateSubCategoryIndicationCard(@PathVariable("subCategoryIndication_id") Integer id, @RequestBody SubCategoryIndicationCardDto dto,
                                                  BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = subCategoryIndicationCardService.update(id, dto);
        if (!result) {
            return new ResponseEntity<>("Cập nhật chỉ định thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhật chỉ định thành công", HttpStatus.CREATED);
    }

    @DeleteMapping("/{subCategoryIndication_id}")
    public Object deleteSubCategoryIndicationCard(@PathVariable("subCategoryIndication_id") Integer id) {
        boolean result = subCategoryIndicationCardService.delete(id);
        if (!result) {
            return new ResponseEntity<>("Xóa thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Xóa thành công", HttpStatus.OK);
    }


}
