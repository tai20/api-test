package com.sharework.health.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.sharework.health.dto.UserDetailsDto;
import com.sharework.health.entity.User;
import com.sharework.health.repository.UserRepository;

import lombok.AllArgsConstructor;

@Service
@Transactional(rollbackOn = Exception.class)
@AllArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService{

	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		User entity = userRepository.findByEmail(email);
		if (entity == null) {
			throw new UsernameNotFoundException("Email không tồn tại");
		}
		
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		String roleName = entity.getRole().getName();
		authorities.add(new SimpleGrantedAuthority(roleName));
		UserDetails userDetails = new UserDetailsDto(entity.getEmail(), entity.getPassword(),
				authorities);
		
		return userDetails;
	}
	
	
}
