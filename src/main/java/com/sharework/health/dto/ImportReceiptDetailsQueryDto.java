package com.sharework.health.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ImportReceiptDetailsQueryDto {

    private Integer receipt_id;

    private Integer clinic_stock_id;

    private Integer product_batch_id;

    private String name_batch;

    private Integer number_product_import;

    private Integer product_id;

    private String name;

    private BigDecimal import_price;
}
