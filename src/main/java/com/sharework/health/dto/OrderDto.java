package com.sharework.health.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrderDto {

    private Integer id;

    private LocalDateTime orderDate;

    private String paymentMethod;

    private String status;

    private BigDecimal debtMoney;

    private BigDecimal total;

    private double discount;

    private UserDto userDto;

    private CustomerDto customerDto;

    private String type;

    @JsonIgnore
    private List<OrderDetailProductDto> orderDetailProductDtos;

    private Integer id_coupon;
    private String name_coupon;
}
