package com.sharework.health.helper;

public class DataValue {

    /**
     * Clinic
     */
    public static final String CLINIC_NAME[] = {"Sản phụ GLAMER", "Thẩm mỹ GLAMER"};

    public static final String ADDRESS[] = {"Phạm Ngọc Thạch, Xã Thạnh An, Cần Giờ, TPHCM", "5, Xã Nhị Bình, Hóc Môn, TPHCM", "Hòa Mỹ, Phường Phạm Ngũ Lão, Quận 1, TPHCM", "Huỳnh Văn Bánh, Phường 1, Phú Nhuận, TPHCM", "Tân Quy, Xã Phước Hiệp, Củ Chi, TPHCM",
            "Phú Định, Phường 13, Quận 8, TPHCM", "Dương Vũ Tùng, Phường 22, Bình Thạnh, TPHCM", "Kênh Tân Hóa, Phường Hòa Thạnh, Tân Phú, TPHCM", "Sầm Sơn, Phường 14, Tân Bình, TPHCM", "D3, Phường Bình Trị Đông B, Bình Tân, TPHCM",
            "Phạm Văn Đồng, Phường 5, Bình Thạnh, TPHCM", "Trần Quang Diệu, Phường 8, Phú Nhuận, TPHCM", "Nguyễn Hữu Hãn, Phường 12, Quận 4, TPHCM", "Số 11, Phường 15, Quận 11, TPHCM", "9, Phường  Hóc Môn, Hóc Môn, TPHCM",
            "T5, Thị trấn Tân Túc, Bình Chánh, TPHCM", "Đào Tông Nguyên, Thị trấn Nhà Bè, Nhà Bè, TPHCM", "Xã Đê, Xã Phong Phú, Bình Chánh, TPHCM", "Tỉnh lộ 9, Xã Thới Tam Thôn, Hóc Môn, TPHCM", "1, Phường 6, Tân Bình, TPHCM",
            "Tôn Thất Tùng, Phường Đa Kao, Quận 1, TPHCM", "5, Phường Tân Thới Nhất, Quận 12, TPHCM", "Phạm Hữu Chí, Phường 4, Quận 5, TPHCM", "Thạnh Lộc 21, Phường Tân Chánh Hiệp, Quận 12, TPHCM", "Liên Ấp 26, Phường An Lạc A, Bình Tân, TPHCM",
            "Duyên Hải, Xã Lý Nhơn, Cần Giờ, TPHCM", "Đỗ Ngọc Thạnh, Phường 2, Quận 11, TPHCM", "Ngô Quyền, Phường Sơn Kỳ, Tân Phú, TPHCM", "45, Phường 6, Tân Bình, TPHCM", "Xuân Thịnh, Phường Cát Lái, Quận 2, TPHCM",
            "Số 28, Phường 8, Quận 11, TPHCM", "CC3, Phường Tây Thạnh, Tân Phú, TPHCM", "Số 14, Thị trấn Nhà Bè, Nhà Bè, TPHCM", "Chu Văn An, Phường 1, Quận 6, TPHCM", "Số 9, Phường 13, Quận 11, TPHCM",
            "Trương Minh Giảng, Phường 11, Gò Vấp, TPHCM", "D15, Phường Trường Thọ, Thủ Đức, TPHCM", "Tân Trào, Phường Phú Mỹ, Quận 7, TPHCM", "Hoàng Minh Giám, Phường 11, Phú Nhuận, TPHCM", "Doi Lầu, Xã Long Hòa, Cần Giờ, TPHCM",
            "Đỗ Bí, Phường Phú Trung, Tân Phú, TPHCM", "Công Lý, Phường Tam Phú, Thủ Đức, TPHCM", "Cư Xá Phú Lâm, Phường 7, Quận 6, TPHCM", "Nguyễn Huy Thông, Phường 16, Gò Vấp, TPHCM", "37A, Phường Cát Lái, Quận 2, TPHCM",
            "Châu Văn Liêm, Phường 9, Quận 11, TPHCM", "F21, Phường Thảo Điền, Quận 2, TPHCM", "Hương lộ 60, Phường Bà Điểm, Hóc Môn, TPHCM", "D823, Xã Trung Lập Hạ, Củ Chi, TPHCM", "Nguyễn Thị Nhỏ, Phường 7, Quận 10, TPHCM",
            "Giải Phóng, Phường 1, Tân Bình, TPHCM", "Thạnh Xuân 21, Phường 16, Quận 8, TPHCM", "N6, Xã Long Thới, Nhà Bè, TPHCM", "Vành Đai 2, Phường 12, Quận 8, TPHCM", "Trần Huy Liệu, Phường 14, Phú Nhuận, TPHCM",
            "34, Phường 8, Quận 4, TPHCM", "Thới An 07, Phường Tân Thới Nhất, Quận 12, TPHCM", "12C, Phường 6, Quận 4, TPHCM", "Số 35, Phường Long Bình, Quận 9, TPHCM", "21, Phường 6, Quận 4, TPHCM",
            "Đỗ Văn Sửu, Phường 7, Quận 5, TPHCM", "Ngô Bệ, Phường 2, Tân Bình, TPHCM", "Đặng Tất, Phường Cô Giang, Quận 1, TPHCM", "Số 6, Phường 8, Quận 11, TPHCM", "Đào Duy Từ, Phường 4, Phú Nhuận, TPHCM",
            "Võ Văn Kiệt, Phường Nguyễn Cư Trinh, Quận 1, TPHCM", "C9, Phường Tân Quy, Quận 7, TPHCM", "6, Phường 25, Bình Thạnh, TPHCM", "Lê Thị Chừng, Xã Bình Mỹ, Củ Chi, TPHCM", "Triệu Quang Phục, Phường 9, Quận 5, TPHCM",
            "M7, Phường Bình Khánh, Quận 2, TPHCM", "Công Trường Tự Do, Phường 14, Bình Thạnh, TPHCM", "Tái Thiết, Phường 3, Tân Bình, TPHCM", "10, Phường 9, Quận 6, TPHCM", "Trường Sa, Phường Cô Giang, Quận 1, TPHCM",
            "Huỳnh Khương An, Phường Bến Thành, Quận 1, TPHCM", "Lê Thị Kim, Xã Đa Phước, Bình Chánh, TPHCM", "3/2, Phường 10, Quận 11, TPHCM", "TK14, Xã Nhị Bình, Hóc Môn, TPHCM", "Hưng Gia 5, Phường Tân Kiểng, Quận 7, TPHCM",
            "C5, Phường 7, Quận 8, TPHCM", "K1, Xã Trung Chánh, Hóc Môn, TPHCM", "Số 36, Phường 1, Quận 4, TPHCM", "Số 42, Phường Linh Đông, Thủ Đức, TPHCM", "Tân Canh, Phường 1, Tân Bình, TPHCM",
            "Số 9, Thị trấn Tân Túc, Bình Chánh, TPHCM", "Trường Lưu 2, Phường Tăng Nhơn Phú B, Quận 9, TPHCM", "Số 4E, Thị trấn Tân Túc, Bình Chánh, TPHCM", "Số 2B Cư xá Lữ Gia, Phường 13, Quận 11, TPHCM", "Tân Thới Hiệp 10, Phường Thảo Điền, Quận 2, TPHCM",
            "42, Phường An Phú, Quận 2, TPHCM", "Nhị Bình 19A, Xã  Đông Thạnh, Hóc Môn, TPHCM", "47, Phường Thảo Điền, Quận 2, TPHCM", "Ngọc Ngà, Phường Cần Thạnh , Cần Giờ, TPHCM", "Dương Đình Cúc, Thị trấn Tân Túc, Bình Chánh, TPHCM",
            "Trần Huy Liệu, Phường 4, Quận 3, TPHCM", "6B, Xã Phong Phú, Bình Chánh, TPHCM", "Yersin, Phường Đa Kao, Quận 1, TPHCM", "Số 22, Phường 9, Quận 8, TPHCM", "Trại Gà, Phường Bình Khánh, Quận 2, TPHCM",
            "Huỳnh Khương An, Phường 9, Gò Vấp, TPHCM", "Nguyễn Sơn Hà, Phường 12, Quận 10, TPHCM", "Thạnh Lộc 17, Phường Trung Mỹ Tây, Quận 12, TPHCM", "Trường Thành, Phường Tân Chánh Hiệp, Quận 12, TPHCM", "Dương Bá Trạc, Phường 5, Quận 5, TPHCM",
            "An Phú Đông 27, Xã Long Hòa, Cần Giờ, TPHCM", "Bình An, Phường An Lợi Đông, Quận 2, TPHCM", "10C, Phường An Lạc A, Bình Tân, TPHCM", "Hậu Lân, Phường Cần Thạnh , Cần Giờ, TPHCM", "D20, Phường An Lợi Đông, Quận 2, TPHCM",
            "Phan Văn Sửu, Phường 5, Tân Bình, TPHCM", "Mương Khai, Xã Bình Khánh, Cần Giờ, TPHCM", "Hưng Phước, Phường 12, Quận 5, TPHCM", "Bạch Vân, Phường 3, Quận 5, TPHCM", "Nguyễn Văn Chiêm, Phường Cầu Kho, Quận 1, TPHCM",
            "31, Phường Linh Trung, Thủ Đức, TPHCM", "23/9, Xã Tân Hiệp, Hóc Môn, TPHCM", "Thới Tứ 1, Xã Tân Xuân, Hóc Môn, TPHCM", "81, Phường Tân Quy, Quận 7, TPHCM", "Đặng Minh Khiêm, Phường 8, Quận 11, TPHCM",
            "47, Xã Lý Nhơn, Cần Giờ, TPHCM", "Tú Xương, Phường Đa Kao, Quận 1, TPHCM", "Số 6, Phường 10, Phú Nhuận, TPHCM", "Số 15, Xã Phước Lộc, Nhà Bè, TPHCM", "Hoàng Sa, Phường Đa Kao, Quận 1, TPHCM",
            "22, Phường Bình Trị Đông B, Bình Tân, TPHCM", "Lê Hùng Yên, Xã Thạnh An, Cần Giờ, TPHCM", "Tắc Xuất, Xã Long Hòa, Cần Giờ, TPHCM", "Khổng Tử, Phường Tăng Nhơn Phú A, Quận 9, TPHCM", "Số 50, Phường 1, Gò Vấp, TPHCM",
            "Đỗ Quang Đẩu, Phường Bến Nghé, Quận 1, TPHCM", "9, Phường Bình Trị Đông A, Bình Tân, TPHCM", "Số 6B, Xã Tân Quý Tây, Bình Chánh, TPHCM", "ĐT 107, Thị trấn Củ Chi, Củ Chi, TPHCM", "Bà Xán, Xã Long Hòa, Cần Giờ, TPHCM",
            "Quách Điêu 12, Xã Bình Hưng, Bình Chánh, TPHCM", "Phan Ngữ, Phường Bến Thành, Quận 1, TPHCM", "Số 5, Phường Hiệp Bình Chánh, Thủ Đức, TPHCM", "Số 27, Phường Tân Hưng, Quận 7, TPHCM", "Tân Trào, Phường Tân Hưng, Quận 7, TPHCM",
            "4, Phường 6, Bình Thạnh, TPHCM", "Mã Lò, Phường Phú Thọ Hòa, Tân Phú, TPHCM", "Số 14, Xã Nhị Bình, Hóc Môn, TPHCM", "Rạch Lồng Đèn, Phường 14, Quận 8, TPHCM", "Lê Thị Hoa, Phường Hiệp Bình Phước, Thủ Đức, TPHCM",
            "Tân Sơn, Phường 2, Tân Bình, TPHCM", "N4, Xã Phước Kiển, Nhà Bè, TPHCM", "Tây Thạnh, Phường Phú Trung, Tân Phú, TPHCM", "80B, Xã Trung Chánh, Hóc Môn, TPHCM", "Trích Sài, Xã Long Hòa, Cần Giờ, TPHCM",
            "111, Phường 17, Gò Vấp, TPHCM", "Số 10, Phường 13, Quận 3, TPHCM", "Nguyễn An Khương, Phường 1, Quận 5, TPHCM", "Nguyễn Phúc Nguyên, Phường 4, Quận 3, TPHCM", "Bà Ký, Phường 2, Quận 6, TPHCM",
            "Bình Giã, Phường Tân Sơn Nhì, Tân Phú, TPHCM", "23, Phường Bình Thọ, Thủ Đức, TPHCM", "Nghĩa Phát, Phường 11, Tân Bình, TPHCM", "Nguyễn Dữ, Phường Hòa Thạnh, Tân Phú, TPHCM", "Phan Văn Sửu, Phường 8, Tân Bình, TPHCM",
            "Tỉnh lộ 825, Xã An Phú Tây, Bình Chánh, TPHCM", "54, Phường 7, Quận 6, TPHCM", "Phạm Đăng Giảng, Phường Bình Hưng Hòa B, Bình Tân, TPHCM", "Nguyễn Văn Của, Phường 1, Quận 10, TPHCM", "Phạm Bân, Phường 3, Quận 5, TPHCM",
            "Số 29, Phường Phú Mỹ, Quận 7, TPHCM", "783, Phường Long Trường, Quận 9, TPHCM", "Tân Thành, Phường 14, Quận 5, TPHCM", "36, Phường Tân Phú, Quận 9, TPHCM", "Bà Xán, Xã Lý Nhơn, Cần Giờ, TPHCM",
            "Rần Chác, Xã Tam Thôn Hiệp, Cần Giờ, TPHCM", "24, Phường 4, Quận 4, TPHCM", "Trần Đại Nghĩa, Phường Tân Tạo, Bình Tân, TPHCM", "Khiêu Năng Tĩnh, Xã Lý Nhơn, Cần Giờ, TPHCM", "Võ Hoành, Phường Hiệp Tân, Tân Phú, TPHCM",
            "Số 3, Phường Trường Thọ, Thủ Đức, TPHCM", "21, Phường 5, Quận 8, TPHCM", "4, Phường 7, Quận 6, TPHCM", "225, Xã Phước Hiệp, Củ Chi, TPHCM", "Bình Thới, Phường 1, Quận 6, TPHCM",
            "Số 21A, Phường Bình Trị Đông A, Bình Tân, TPHCM", "2, Phường 5, Quận 6, TPHCM", "Lão Tử, Phường 14, Quận 5, TPHCM", "Nguyễn Khanh, Xã Thạnh An, Cần Giờ, TPHCM", "Tân Hiệp 18, Phường Bà Điểm, Hóc Môn, TPHCM",
            "Liên Ấp 1-2-3, Xã Hưng Long, Bình Chánh, TPHCM", "Số 5B, Phường  Thạnh Mỹ Lợi, Quận 2, TPHCM", "Bà Huyện Thanh Quan, Phường 5, Quận 4, TPHCM", "3, Phường 5, Quận 11, TPHCM", "H10, Phường An Lạc, Bình Tân, TPHCM",
            "Thạnh Xuân 22, Phường Thới An, Quận 12, TPHCM", "Trường Sa, Phường 13, Bình Thạnh, TPHCM", "Cầu Đôi, Xã Đa Phước, Bình Chánh, TPHCM", "Rần Chác, Xã Thạnh An, Cần Giờ, TPHCM", "Lê Thị Riêng, Phường 2, Quận 3, TPHCM",
            "Đồng Khởi, Phường 5, Tân Bình, TPHCM", "Nguyễn Thượng Hiền, Phường 5, Quận 10, TPHCM", "Kênh 3, Xã Lê Minh Xuân, Bình Chánh, TPHCM", "Số 97, Phường Bình Khánh, Quận 2, TPHCM", "Lưu Văn Lang, Phường Bến Nghé, Quận 1, TPHCM"};

    public static final String STATUS[] = {"ACTIVE", "DEACTIVE"};

    /**
     * Role
     */
    public static final String ROLE_NAME[] = {"ROLE_ADMIN", "ROLE_DOCTOR","ROLE_COUNSELOR", "ROLE_OPERATION", "ROLE_ACCOUNTANT", "ROLE_TECHNICIAN", "ROLE_SUBWAREHOUSE", "ROLE_BRANCHWAREHOUSE","ROLE_RECEPTIONIST", "ROLE_OBSTETRICIAN", "ROLE_RECEPTIONIST_OBSTETRICAL"};

    public static final String ROLE_DESCRIPTION[] = {"Quản trị hệ thống", "Bác sĩ","Nhân viên tư vấn", "Vận hành", "Kế toán", "Kỹ thuật viên", "nhân viên kho tổng", "Nhân viên kho chi nhánh", "Lễ tân", "Bác sĩ sản khoa", "Lễ tân sản khoa"};

    /**
     * Department
     */
    public static final String DEPARTMENT_NAME[] = {"Quản trị hệ thống", "Bác sĩ","Nhân viên tư vấn"};

    /**
     * User
     */
    public static final String USER_NAME[] = {"Mai Xuân Hoàng", "Tô Thị Hồng Thắm", "Nguyễn Trọng Hảo", "Nguyễn Tấn Lộc", "Nguyễn Văn Tính", "Trương Công Cường", "Mai Minh Thư", "Điểu Long", "Dương Cát Luynh", "Đặng Trường An",
            "Lê Chí Bảo", "Nguyễn Anh Tuấn", "Đỗ Thị Thu Diệp", "Võ Anh Hào", "Vũ Trung Kiên", "Nguyễn Quang Đức", "Nguyễn Hoàng Lâm", "Nguyễn Thanh Hoài", "Nguyễn Thị Kiều Oanh", "Nguyễn Ngọc Trường",
            "Nguyễn Hưng Phát", "Trần Hồng Thịnh", "Nguyễn Văn Hùng", "Tạ Minh Đức", "Trần Quang Triệu", "Lê Tuấn Khang", "Nguyễn Nhật Hào", "Lê Khắc Trung", "Nguyễn Phan Huỳnh Đức", "Tằng Tiến Đạt",
            "Lục Gia Anh", "Vũ Văn Khải", "Nguyễn Hoàng Nhật", "Nguyễn Thị Hoàng Nhi", "Nguyễn Tấn Toàn", "Lê Quang Nhật", "Bùi Thị Mỹ Duyên", "Nguyễn Phúc Thịnh", "Lê Dĩ Khang", "Lê Trần Tuấn Thành",
            "Văn Minh Hoàng", "Phạm Thái Sơn", "Thái Anh Hào", "Đặng Lê Nguyên Lương", "Đỗ Đạt Đức", "Thiều Quang Nghĩa", "Trương Tuấn Phúc", "Cao Thành Đạt", "Phan Xuân Bách", "Hoàng Thiên Thảo",
            "Hoàng Hữu Hiển", "Phạm Quí Phong", "Nguyễn Tuấn Anh", "Dương Anh Tú", "Huỳnh Ngọc Hoàng Phúc", "Lê Nhật Huy", "Nguyễn Tấn Lợi", "Trương Đình Phước", "Nguyễn Đức Huy", "Nguyễn Hữu Thắng",
            "Trần Thái Minh Tân", "Trần Trung Vinh", "Lê Đình Huy", "Nguyễn Hồng Sơn", "Nguyễn Duy Thiện", "Dương Thị Diễm", "Nguyễn Xuân Thìn", "Nguyễn Hiếu Học", "Hà Hiếu Uyên", "Võ Đại Quyền",
            "Phan Huy Quỳnh", "Phùng Khánh Toàn", "Nguyễn Mạnh Trường", "Trần Đông Hoàng", "Đoàn Thị Thanh Hồng", "Nguyễn Văn Mỹ", "Tống Sỹ Nguyên", "Nguyễn Trần Ngọc Tới", "Phạm Ngọc Như ý", "Phan Thị Tứ Thi",
            "Lê Ngọc Tồn", "Vũ Đình Khánh Đăng", "Nguyễn Trần Nhật Hào", "Nguyễn Mai Anh", "Nguyễn Thanh Tú", "Lê Thị Nguyên", "Hà Huy Hoàng", "Hồ Minh Phúc", "Võ Thanh Nhàn", "Hoàng Xuân Khang",
            "Nguyễn Bảo Hòa", "Nguyễn Công Thành Đạt", "Phùng Thanh Toàn", "Võ Tấn Nguyên", "Ngô Quang Long", "Đoàn Văn Vĩnh", "Trần Xuân Quang", "Trần Vũ Hoàng Sơn", "Châu Quốc An", "Bùi Thành Nam",
            "Phạm Thành Khoa", "Mai Lộc", "Huỳnh Thanh Bình", "Nguyễn Xuân Hải", "Trần Phạm Gia Long", "Nguyễn Thiên Cát Tường", "Nguyễn Trần Nhật Hưng", "Võ Thanh Thế", "Trần Minh Nghĩa", "Trần Cao Tường",
            "Nguyễn Thế Hậu", "Lê An Thịnh Phát", "Đinh Thị Hà Phương", "ứng Ngọc Sơn", "Nguyễn Văn Chiến", "Võ Huỳnh Bảo Khương", "Huỳnh Nguyễn Đức Hưng", "Hồ Dương Vũ", "Lê Danh Huy", "Nguyễn Văn Anh",
            "Hà Danh Phúc", "Nguyễn Phước Nguyên Ân", "Phạm Anh Tuấn", "Nguyễn Thành Luân", "Phạm Lê Thành", "Phạm Xuân Lộc", "Nguyễn Ngọc Bảo Duy", "Thái Huy Hoàng", "Nguyễn Thanh Liêm", "Lê Tuấn Tú",
            "Mai Kiên Cường", "Nguyễn Huy Hảo", "Trần Chí Hùng", "Lương Thanh Nhất", "Nguyễn Ngọc Hà", "Nguyễn Thị Thanh Thúy", "Nguyễn Anh Khoa", "Lưu Tuấn Kha", "Võ Đức Hoàng", "Nguyễn Văn Tâm",
            "Trần Hoàng Phúc", "Phan Võ Nhật Hoàng", "Nguyễn Hoàng Phúc Liêm", "Nguyễn Thị Quỳnh Mai", "Nguyễn Đình Hoàng Phương", "Võ Minh Vương", "Lê Nhật Uyên Hương", "Huỳnh Anh Tiên", "Nguyễn Hoàng Việt", "Bùi Lâm Vĩnh",
            "Nguyễn Hoàng Nhật Minh", "Trần Phạm Hải Âu", "Nguyễn Đức Duy", "Nguyễn Thế Đạt", "Nguyễn Hữu Lộc", "Bùi Hoàng Phát", "Nguyễn Như Hà", "Lưu Hoàng Long", "Nguyễn Đinh Hoàng Mỹ", "Nguyễn Trung Hải",
            "Nguyễn Văn Vương", "Nguyễn Đức Mạnh", "Nguyễn Ngọc Trí", "Võ Minh Thiện", "Trần Hoài Phong", "Mai Thanh Trọng", "Phan Duy Tuấn", "Vương Cẩm Phong", "Nguyễn Việt Tiến", "Nguyễn Anh Huy",
            "Tô Văn Hậu", "Phạm Xuân Vũ", "Trần Trọng Thành", "Võ Văn Nghĩa", "Nguyễn Văn Lên", "Nguyễn Trọng Nhân", "Võ Quốc Khánh", "Mai Phát Huy", "Phạm Quốc Toàn", "Phạm Tuấn Đạt",
            "Nguyễn Thế Bảo", "Nguyễn Huỳnh Công Lý", "Phan Văn Thông", "Lê Thiên Tân", "Nguyễn Ngọc Mỹ", "Phạm Thị Thanh Ngân", "Bùi Quốc Thanh", "Trương Đức Hoàn", "Nguyễn Tấn Hưng", "Trần Ngọc Hiển",
            "Trương Nhật Long"};

    public static final String USER_EMAIL[] = {"contact@niengiamtrangvang.vn",
            "info@lacco.com.vn",
            "info@kllc.com.vn",
            "nhatthienhuonglogistics@gmail.com",
            "vijaigroupsg@gmail.com",
            "haphuc.8410@gmail.com",
            "thucnu@idstransport.net",
            "info@nguyenngoc.vn",
            "lthanhvan.act@gmail.com",
            "steven.vn@pcvn.com.vn",
            "vp.daitan@gmail.com",
            "sinh.dir@toannhat.vn",
            "info@dacologistics.com",
            "tranngocvinh@yahoo.com.vn",
            "csoadong@gmail.com",
            "vantaivohongphat@gmail.com",
            "lyphugia10@yahoo.com.vn",
            "xuanhieu@xuanhieugroup.com.vn",
            "infocp@chanphat.com.vn",
            "thanhlongfwd@hcm.vnn.vn",
            "info@ctctrans.vn",
            "taynambacsg@gmail.com",
            "info@vantaitoanviet.com",
            "phuthanhphat@ymail.com",
            "co.ltd.manhdung@gmail.com",
            "vantaingocdung@gmail.com",
            "sales@vantaitruongthanhloc.com",
            "info@vietau.vn",
            "huonggiangachau@gmail.com",
            "smavn@smavn.com",
            "acc@delta.com.vn"};

    public static final String USER_PASSWORD[] = {"123456",
            "password",
            "123456789",
            "12345678",
            "12345",
            "111111",
            "1234567",
            "sunshine",
            "qwerty",
            "iloveyou",
            "princess",
            "admin",
            "welcome"};

    public static final String USER_PHONENUMBER[] = {"0913973814", "0913634820", "0903136974", "0903673771", "0913931777", "0913437225", "0903501858", "0903351248", "0913386037", "0903731434",
            "0903710529", "0913408963", "0903703837", "0903776919", "0903982407", "0913706500", "0913508120", "0913439611", "0913367191", "0913792401",
            "0903144539", "0913636382", "0903276121", "0903510491", "0903304434", "0903332055", "0913469421", "0903370527", "0913676340", "0913820810",
            "0913653245", "0913820846", "0913959223", "0903641479", "0913796000", "0903851555", "0913121959", "0903659537", "0903978246", "0913495864",
            "0903982519", "0903866773", "0903406493", "0903206388", "0903219089", "0903848276", "0913202254", "0913348228", "0903166410", "0903618541",
            "0903626632", "0903134597", "0903874499", "0903721029", "0913181060", "0913488827", "0903215666", "0903243287", "0903602003", "0903709062",
            "0903601592", "0913588226", "0903633761", "0903556528", "0903114285", "0903802022", "0913488707", "0913785995", "0903261344", "0903914356",
            "0913664502", "0903178150", "0903804005", "0903117348", "0903769339", "0913798595", "0903656013", "0913706649", "0913126857", "0903446551",
            "0903133485", "0913678424", "0913592984", "0913549239", "0913861364", "0913833404", "0913525708", "0903217956", "0903818365", "0913136290",
            "0913273165", "0913701180", "0903570650", "0913893133", "0903675458", "0903158767", "0913240079", "0903347142", "0913472823", "0903810466",
            "0903970344", "0913251020", "0913480765", "0913189986", "0913841346", "0913406347", "0903565463", "0913766448", "0913500572", "0903448842",
            "0913250835", "0903791628", "0903141140", "0903833076", "0903942849", "0903159674", "0913135899", "0903657001", "0903783822", "0903891015",
            "0903293339", "0913794315", "0913118608", "0913163633", "0903810565", "0903243159", "0913738567", "0913529818", "0903805703", "0903828978",
            "0913490200", "0913833965", "0913457583", "0903178793", "0903104181", "0913727387", "0903232503", "0913370322", "0913702772", "0913728596",
            "0913542231", "0913103708", "0903796326", "0913554059", "0913576053", "0903409121", "0913370224", "0903707749", "0913386323", "0913940058",
            "0913752821", "0903919767", "0913468089", "0913177200", "0903766357", "0913816446", "0913171569", "0913333020", "0913859325", "0903280516",
            "0913766731", "0913401669", "0913638543", "0903538689", "0913864356", "0903330861", "0903588874", "0913119609", "0913332185", "0913557341",
            "0913289713", "0913545500", "0913686423", "0903600918", "0913520111", "0903596863", "0903727968", "0903549087", "0903768428", "0913542452",
            "0913562365", "0903530873", "0913231161", "0913944228", "0903946784", "0913660296", "0903442158", "0903535819", "0903181921", "0913757135",
            "0903391269", "0913406023", "0903765694", "0903233067", "0913433034", "0913822202", "0903355164", "0903572214", "0903687563", "0913434473",
            "0903895216", "0913676176", "0913808450", "0903145525", "0903393881", "0903973917", "0903976655", "0913185878", "0913941588", "0903408714",
            "0903801752", "0903943200", "0913830970", "0903286801", "0913127479", "0903913569", "0903488263", "0913963377", "0913838635", "0903819969",
            "0903551634", "0903123860", "0903154880", "0913695804", "0913660532", "0903736974", "0913463363", "0903114006", "0913585323", "0903519192",
            "0903281461", "0903573990", "0913243648", "0903116644", "0913620880", "0903275107", "0913186856", "0903276851", "0913265630", "0903493328",
            "0913139392", "0903125987", "0913172264", "0913333067", "0903991467", "0903905994", "0903769979", "0913585301", "0903737725", "0903863455",
            "0903235873", "0903234907", "0903386689", "0903236870", "0903890710", "0913508205", "0903129349", "0903582459", "0913355184", "0903335959",
            "0903169366", "0913816940", "0903829585", "0903957138", "0913409496", "0903795102", "0913366041", "0913473325", "0903399738", "0913790174",
            "0913176238", "0913424043", "0903860605", "0913729801", "0903881824", "0913311101", "0903433278", "0913535492", "0903130893", "0903981368",
            "0913190289", "0913360222", "0903259600", "0913445802", "0903817983", "0913757836", "0913685719", "0903718362", "0903132431", "0903877412",
            "0913523692", "0913870932", "0903439276", "0913936861", "0913411528", "0913149837", "0913553896", "0903380670", "0903443803", "0913609920",
            "0903879505", "0913843949", "0913177079", "0913957710", "0913471171", "0913844169", "0903386622", "0913106151", "0913422265", "0903264848",
            "0913745836", "0913873137", "0903397631", "0903724181", "0903768825", "0913779549", "0913870174", "0903466088", "0913434638", "0913311892",
            "0903331265", "0913876585", "0903715747", "0903489067", "0903248212", "0913399744", "0903439906", "0913391153", "0903292043", "0903868716",
            "0913366563", "0913372130", "0903791421", "0913393487", "0913451683", "0913865626", "0913198614", "0903337180", "0913609816", "0903749894",
            "0903531003", "0913362309", "0913671701", "0913373322", "0913705589", "0903900951", "0913921309", "0903348239", "0903551057", "0913888591",
            "0903607579", "0913554897", "0903593295", "0913980692", "0903152017", "0913170939", "0913821858", "0903982047", "0913246200", "0903478789",
            "0913921180", "0913143484", "0913260402", "0903304139", "0913801728", "0903170702", "0913407734", "0913528721", "0903289594", "0903722505",
            "0903809899", "0913103652", "0913536616", "0913384869", "0903191759", "0903179749", "0913812520", "0903498688", "0913658606", "0913260437",
            "0903665700", "0913934653", "0903943851", "0913756645", "0913224127", "0903477762", "0913331008", "0903722831"};

    public static final int USER_WORKEXPERIENCE[] = {1,2,3,4,5,6,7,8,9,10};

    public static final double USER_SALARY_COEFFICIENTS[] = {0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9};

    public static final String CUSTOMER_HEALTH_INSURANCE_NUMBER[] = {"0913973814", "0913634820", "0903136974", "0903673771", "0913931777", "0913437225", "0903501858", "0903351248", "0913386037", "0903731434",
            "0903710529", "0913408963", "0903703837", "0903776919", "0903982407", "0913706500", "0913508120", "0913439611", "0913367191", "0913792401",
            "0903144539", "0913636382", "0903276121", "0903510491", "0903304434", "0903332055", "0913469421", "0903370527", "0913676340", "0913820810",
            "0913653245", "0913820846", "0913959223", "0903641479", "0913796000", "0903851555", "0913121959", "0903659537", "0903978246", "0913495864",
            "0903982519", "0903866773", "0903406493", "0903206388", "0903219089", "0903848276", "0913202254", "0913348228", "0903166410", "0903618541",
            "0903626632", "0903134597", "0903874499", "0903721029", "0913181060", "0913488827", "0903215666", "0903243287", "0903602003", "0903709062",
            "0903601592", "0913588226", "0903633761", "0903556528", "0903114285", "0903802022", "0913488707", "0913785995", "0903261344", "0903914356",
            "0913664502", "0903178150", "0903804005", "0903117348", "0903769339", "0913798595", "0903656013", "0913706649", "0913126857", "0903446551",
            "0903133485", "0913678424", "0913592984", "0913549239", "0913861364", "0913833404", "0913525708", "0903217956", "0903818365", "0913136290",
            "0913273165", "0913701180", "0903570650", "0913893133", "0903675458", "0903158767", "0913240079", "0903347142", "0913472823", "0903810466",
            "0903970344", "0913251020", "0913480765", "0913189986", "0913841346", "0913406347", "0903565463", "0913766448", "0913500572", "0903448842",
            "0913250835", "0903791628", "0903141140", "0903833076", "0903942849", "0903159674", "0913135899", "0903657001", "0903783822", "0903891015",
            "0903293339", "0913794315", "0913118608", "0913163633", "0903810565", "0903243159", "0913738567", "0913529818", "0903805703", "0903828978",
            "0913490200", "0913833965", "0913457583", "0903178793", "0903104181", "0913727387", "0903232503", "0913370322", "0913702772", "0913728596",
            "0913542231", "0913103708", "0903796326", "0913554059", "0913576053", "0903409121", "0913370224", "0903707749", "0913386323", "0913940058",
            "0913752821", "0903919767", "0913468089", "0913177200", "0903766357", "0913816446", "0913171569", "0913333020", "0913859325", "0903280516",
            "0913766731", "0913401669", "0913638543", "0903538689", "0913864356", "0903330861", "0903588874", "0913119609", "0913332185", "0913557341",
            "0913289713", "0913545500", "0913686423", "0903600918", "0913520111", "0903596863", "0903727968", "0903549087", "0903768428", "0913542452",
            "0913562365", "0903530873", "0913231161", "0913944228", "0903946784", "0913660296", "0903442158", "0903535819", "0903181921", "0913757135",
            "0903391269", "0913406023", "0903765694", "0903233067", "0913433034", "0913822202", "0903355164", "0903572214", "0903687563", "0913434473",
            "0903895216", "0913676176", "0913808450", "0903145525", "0903393881", "0903973917", "0903976655", "0913185878", "0913941588", "0903408714",
            "0903801752", "0903943200", "0913830970", "0903286801", "0913127479", "0903913569", "0903488263", "0913963377", "0913838635", "0903819969",
            "0903551634", "0903123860", "0903154880", "0913695804", "0913660532", "0903736974", "0913463363", "0903114006", "0913585323", "0903519192",
            "0903281461", "0903573990", "0913243648", "0903116644", "0913620880", "0903275107", "0913186856", "0903276851", "0913265630", "0903493328",
            "0913139392", "0903125987", "0913172264", "0913333067", "0903991467", "0903905994", "0903769979", "0913585301", "0903737725", "0903863455",
            "0903235873", "0903234907", "0903386689", "0903236870", "0903890710", "0913508205", "0903129349", "0903582459", "0913355184", "0903335959",
            "0903169366", "0913816940", "0903829585", "0903957138", "0913409496", "0903795102", "0913366041", "0913473325", "0903399738", "0913790174",
            "0913176238", "0913424043", "0903860605", "0913729801", "0903881824", "0913311101", "0903433278", "0913535492", "0903130893", "0903981368",
            "0913190289", "0913360222", "0903259600", "0913445802", "0903817983", "0913757836", "0913685719", "0903718362", "0903132431", "0903877412",
            "0913523692", "0913870932", "0903439276", "0913936861", "0913411528", "0913149837", "0913553896", "0903380670", "0903443803", "0913609920",
            "0903879505", "0913843949", "0913177079", "0913957710", "0913471171", "0913844169", "0903386622", "0913106151", "0913422265", "0903264848",
            "0913745836", "0913873137", "0903397631", "0903724181", "0903768825", "0913779549", "0913870174", "0903466088", "0913434638", "0913311892",
            "0903331265", "0913876585", "0903715747", "0903489067", "0903248212", "0913399744", "0903439906", "0913391153", "0903292043", "0903868716",
            "0913366563", "0913372130", "0903791421", "0913393487", "0913451683", "0913865626", "0913198614", "0903337180", "0913609816", "0903749894",
            "0903531003", "0913362309", "0913671701", "0913373322", "0913705589", "0903900951", "0913921309", "0903348239", "0903551057", "0913888591",
            "0903607579", "0913554897", "0903593295", "0913980692", "0903152017", "0913170939", "0913821858", "0903982047", "0913246200", "0903478789",
            "0913921180", "0913143484", "0913260402", "0903304139", "0913801728", "0903170702", "0913407734", "0913528721", "0903289594", "0903722505",
            "0903809899", "0913103652", "0913536616", "0913384869", "0903191759", "0903179749", "0913812520", "0903498688", "0913658606", "0913260437",
            "0903665700", "0913934653", "0903943851", "0913756645", "0913224127", "0903477762", "0913331008", "0903722831"};

    public static final String CUSTOMER_PASS_MEDICAL_RECORD[] = {"mang thai", "sinh con", "chu kỳ kinh nguyệt",
            "các bệnh di truyền", "bệnh tự miễn", "bệnh tim mạch", "bệnh ác tính","Dịch tễ lâm sàng"};

    public static final String CUSTOMER_ALLERGY[] = {"Dị ứng mùa xuân", "Dị ứng mùa hè", "Dị ứng mùa thu",
            "Dị ứng mùa đông", "Viêm mũi dị ứng", "Dị ứng phấn hoa", "Dị ứng nấm mốc", "Dị ứng bụi", "Dị ứng với vật nuôi, thú cưng",
            "Dị ứng thức ăn"};

    public static final String MEDICAL_RECORD_CONTENT[]= {"Khám nội","Khám sản phụ", "Khám thai","Khám dị ứng", "Khám nội soi",
            "Xét nghiệm","Chụp X-quang", "Khám hồi sức"};

    public static final String MEDICAL_RECORD_DIAGNOSE[]= {"Nhiễm trùng", "Huyết áp cao", "Khó thở", "Suy tim", "Vỡ ối",
            "Ung thư tử cung giao đoạn đầu", "Ung thư tử cung giao đoạn giữa", "Ung thư tử cung giao đoạn cuối",
            "chảy máu", "tiểu đường thai kì", "sự suy giảm nồng độ i ốt từ vừa đến nặng", "rối loạn tuyến giáp",
            "dị tật bẩm sinh"};

    public static final String PAYMENT_METHOD[] = {"Bank account", "Credit card", "Mo Mo", "payment directly"};

    public static final String PRODUCT_CATEGORY_NAME[] = {"Thuốc","Mỹ phẩm","Dụng cụ y tế","Thiết bị phẫu thuật"};

    public static final String PRODUCT_CATEGORY_DESCRIPTION[] = {"Thuốc là chế phẩm có chứa dược chất hoặc dược liệu dùng cho người nhằm Mục đích phòng bệnh, chẩn đoán bệnh, chữa bệnh, Điều trị bệnh, giảm nhẹ bệnh, Điều chỉnh" +
            " chức năng sinh lý cơ thể người bao gồm thuốc hóa dược, thuốc dược liệu, thuốc cổ truyền, vắc xin và sinh phẩm.",
            "mỹ phẩm là một chất hay chế phẩm được sử dụng để tiếp xúc với những bộ phận bên ngoài cơ thể con người (da, hệ thống lông tóc, móng tay, móng chân, môi và cơ quan sinh dục ngoài) hoặc răng và niêm mạc miệng với mục đích chính là để làm sạch," +
                    " làm thơm, thay đổi diện mạo, hình thức, điều chỉnh mùi cơ thể, bảo vệ cơ thể hoặc giữ cơ thể trong điều kiện tốt."
            ,"Dụng cụ y tế là Là tất cả những loại vật dụng, vật tư tiêu hao, vật liệu được sử dụng trong lĩnh vực y tế dùng để chẩn đoán, theo dõi, điều trị và ngăn ngừa những tổn thương, chấn thương hay bệnh tật. Duy trì và hỗ trợ sự sống." +
            " Kiểm soát quá trình thụ thai, Khử trùng thiết bị y tế….và còn rất nhiều mục đích khác nữa","Thiết bị phẫu thuật là nhứngx thiết bị được sử dụng trong phẫu thuật"};

    public static final String PRODUCT_NAME[] = {"Thuốc bổ Prenatal Mutil DHA","Thuốc bổ cho bà bầu Elevit","Blackmores Pregnancy and Breast Feeding Gold",
            "Viên uống Bio Island DHA For Prenancy", "Thuốc bổ cho bà bầu PM Procare", "Viên tổng hợp cho bà bầu Kobayashi Pregnancy", "Vitamin bổ sung DHA cho bà bầu Beanstalkmom",
            "Collagen", "Coenzyme Q10", "Vitamin C", "Vitamin E", "Polyphenol", "Apigenin", "Riboside Nicotinamide", "Sekkisei", "Dầu Tẩy Trang Shu Uemura Ultime 8 Sublime Beauty Cleansing Oil",
            "Tinh chất làm trắng da SKII Cellumination Aura Essence ", "Serum dưỡng da Obagi C20 Serum", "Sản phẩm trị thâm nám Haku Melanofocus",
            "Kem Dưỡng Mắt BA The Eye Cream", "Kem dưỡng da dạng thạch Astalift Jelly Aquarysta", "Mặt nạ SKII Facial Treatment Mask", "Sản phẩm dưỡng ẩm Ipsa ME Moist 2",
            "Dầu tẩy trang THREE Balancing Cleansing Oil", "Kem dưỡng da Cle de Peau Beaute La Creme", "Viên bôi dưỡng da Naga Venus"};

    public static final String PRODUCT_IMAGES[] = {"thuoc.png","thuoc1.png","thuoc2.png","thuoc3.png","thuoc4.png","thuoc5.png","thuoc6.png",
            "thuoc7.png","thuoc8.png","thuoc9.png"};

    public static final String PRODUCT_INSTRUCTION[] = {"Ngậm dưới lưỡi, uống hoặc tiêm bắp, tĩnh mạch… Còn liều được ghi: Liều dùng cho một lần, liều trong 24 giờ (tức trong một ngày)," +
            " liều cho một đợt điều trị. Ví dụ thuốc được ghi: 500mg x 3 lần/ngày, trong 10 ngày","Ghi cách dùng thuốc như thế nào như: ngậm dưới lưỡi, uống hoặc tiêm bắp, tĩnh mạch… Còn liều được ghi: liều dùng cho một lần, liều trong 24 giờ (tức trong một ngày), liều cho một đợt điều trị. Ví dụ thuốc được ghi: 500mg x 3 lần/ngày, trong 10 ngày, có nghĩa là mỗi lần dùng 500mg thuốc (thường là uống 1 viên chứa 500mg hoạt chất)," +
            " dùng 3 lần trong ngày, dùng trong 10 ngày liên tiếp.","Tiêm bắp, truyền tĩnh mạch 15 - 20mg/kg/ngày. \n" +
            "Dùng 1 lần/ngày","< 1 tuần tuổi. 12 - 15mg/kg, 36/ 48 giờ 1lần\n" +
            "\n" +
            "> 1 tuần tuổi 12mg/kg/ngày\n" +
            "\n" +
            "Trẻ em: liều như người lớn","TB, truyền TM chậm 30 – 60 phút, pha loãng với natri clorid 0,9% hoặc glucose 5%, 6 mg/kg/ngày \n" +
            "dùng 1 lần duy nhất.","< 1 tuần tuổi 4 - 5mg/kg, 36/ 48 giờ/ lần\n" +
            "\n" +
            "> 1 tuần tuổi 4mg/kg/ngày\n" +
            "\n" +
            "Trẻ em liều như người lớn","TB, truyền TM 3 - 6,5 mg/kg/ngày, chia 3 lần hoặc 1 lần duy nhất",
            "thuoc7.png","thuoc8.png","thuoc9.png"};

    public static final String PRODUCT_UNIT[] = {"Vỉ", "Hộp", "Viên", "Gói"};

    public static final String TREATMENT_PACKAGE_CATEGORY_NAME[] = {"Thẩm mỹ","Điều trị","Chăm sóc","Bảo dưỡng",
        "Ưu đãi"};

    public static final String TREATMENT_PACKAGE_NAME[] = {"Chăm sóc thai kỳ toàn diện", "Dịch vụ Theo dõi Thai kỳ nguy cơ cao",
        "Lớp học tiền sản", "Kế hoạch sinh con để cuộc sinh diễn ra hoàn hảo", "Dịch vụ Sinh con an toàn",
        "Vật lý trị liệu hồi phục sau sinh", "Khám phụ khoa định kỳ", "Điều trị Bệnh lý Phụ khoa"};

    public static final String TREATMENT_PACKAGE_DESCRIPTION[]= {"Chương trình bao gồm các xét nghiệm, siêu âm, tiêm ngừa, chẩn đoán bằng các thiết bị kỹ thuật cao nhằm tư vấn " +
            "cho sản phụ một thai kỳ khỏe mạnh và chuẩn bị cho cuộc vượt cạn an toàn",
            "Thai kỳ gọi là nguy cơ cao khi mà mẹ và/hoặc thai nhi đang trong quá trình phát triển có nguy cơ mắc" +
                    " các biến chứng khi mang thai và khi sinh.",
            "Mỗi tháng, FV đều tổ chức lớp học tiền sản do các bác sĩ sản, chuyên gia dinh dưỡng, chuyên gia vật lý trị liệu, nữ hộ sinh, điều dưỡng nhi hướng dẫn nhằm" +
                    " trang bị cho bạn những kiến thức để sẵn sàng cho hành trình làm mẹ.",
            "Cung cấp tài liệu khảo sát, hướng dẫn giúp thai phụ lên kế hoạch cho cuộc sinh cùng bác sĩ theo dõi thai kỳ của mình. Đồng thời, kế hoạch sinh con còn giúp bác sĩ, nữ hộ sinh biết rõ yêu cầu của sản phụ về các vấn đề như kiểm soát cơn đau khi chuyển dạ, cách chăm" +
                    " sóc bé sau sinh và giúp các sản phụ dễ hình dung được cuộc sanh của mình được diễn ra như thế nào",
            "Mang thai suốt một hành trình dài với nhiều cung bậc cảm xúc thì sinh con được xem như là cột mốc và là khoảnh khắc quan trọng nhất để đón bé yêu chào đời. FV luôn chú trọng và lựa chọn đội ngũ làm chuyên môn nhiều kinh" +
                    " nghiệm cùng trang thiết bị hiện đại sẽ giúp sản phụ có được cuộc sanh an toàn và thoải mái với:",
            "Sinh đẻ là một trong những nguyên nhân phổ biến làm suy yếu các cơ sàn chậu. Áp dụng các bài tập sàn chậu sau sinh có thể phòng ngừa được tình trạng tiểu không tự chủ hoặc các bệnh lý liên quan đến sàn chậu về sau." +
                    " Khoa Vật lý Trị liệu tại FV được dẫn dắt bởi Trưởng khoa người Pháp",
            "Khám, tầm soát và điều trị các bệnh lý về phụ khoa như: Xét nghiệm HPV; Soi cổ tử cung; Khám vú và khám sàn chậu; Niệu phụ khoa (tiểu không kiểm soát); Bệnh lý sàn chậu; Phẫu thuật tái tạo màng trinh;" +
                    " Điều trị các bệnh lý thời kỳ mãn kinh; Tránh thai; Đình chỉ thai kỳ.",
            "Khám tầm soát qua siêu âm 3D và chụp nhũ ảnh hỗ trợ phụ nữ trong chẩn đoán, điều trị các bệnh phụ khoa như: u bướu lành tính, đau vú, xơ nang tuyến vú, u xơ tử cung và các bệnh lý khác.\n" +
                    "\n" +
                    "Đặc biệt, khoa Sản Phụ kết hợp với khoa Ung Bướu và khoa Ngoại Tổng Quát của FV để điều trị các bệnh lý Ung thư phụ khoa như: Ung thư vú, Ung thư buồng trứng," +
                    " Ung thư cổ tử cung và các loại Ung thư sàn chậu khác…"};

    public static final String ALPHABETS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdedghijklmnopqrstuvwxyz0123456789";

    public static final String COUPON_NAME[] = {"Sinh nhật tháng 4", "Lễ 30 tháng tư", "Lễ 1 tháng năm", "Mua lần đầu", "Khách hàng mới",
            "Khách hàng thân thiết", "Mua hai sản phẩm trở lên",
            "Giảm 5% cho phụ kiện", "Ưu đãi tháng năm", "Ưu đãi mọi mặt hàng 5%", "Giảm 10% khi thanh toán online", "Ưu đãi xả hàng 10%"};

    public static final String CAREER[] = {"Công nghệ thực phẩm", "Công nghệ chế biến sau thu hoạch", "Kiến trúc",
            "kỹ thuật xây dựng", "Quản trị kinh doanh", "Quản trị dịch vụ du lịch và lữ hành", "Quản trị khách sạn",
            "Marketing", "Kỹ thuật máy tính", "Kỹ thuật phần mềm", "Mạng máy tính và truyền thông dữ liệu", "Khoa học máy tính",
            "An toàn thông tin", "Công nghệ thông tin"};

    public static final String PRODUCER_PRODUCT[] = {"MEDISTAR VIỆT Nam", "Ecogreen", "Trang Vàng", "VNRAS", "GMP EU", "Navita",
    "Dược Phẩm Trung Ương Codupha", "S.P.M", "TNHH Liên Doanh Stellapharm", "Cổ Phần Dược Phẩm Dược Liệu Pharmedic",
    "TNHH Thương Mại Dược Phẩm úc Châu", "TNHH Phân Phối Tiên Tiến", "TNHH Dược Phẩm Phương Đông", "TNHH Y Học Cổ Truyền Đại Hồng Phúc"};

    public static final String RECEIPT_TYPE[] = {"Nhập hàng", "Xuất hàng", "Chuyển hàng"};

    public static final String SKIN_STATUS[] = {"Bỏng", "Nhiều chân lông", "Nhiều mụn", "Mụn thâm", "Vỡ mụn", "Nhiều da chết"};

    public static final String SERVICE_NAME[] = {"Điều trị da", "Bắn lazer", "Hút lỗ chân lông", "Chiếu tia tím thu nhỏ lỗ chân lông",
        "Đắp mặt nạ", "Dưỡng ẩm da mặt", "Đắp dưỡng chất", "lpl", "G- Hydrat", "Bio light"};

    public static final String ADVISORY[] = {"Nên đi khám da mặt", "Uống thuốc bổ hàng ngày", "Sau 1 tháng nên quay lại để kiểm tra", "Muốn gia hạn thời gian gói sử dụng",
            "Tư vấn mua gói bắn lazer", "Khách hàng yêu cầu tư vấn về gói điều trị về da", "Tư vấn về các loại thuốc nên dùng"};

    public static final String CUSTOMER_CATEGORY_NAME[] = {"Khách hàng mới", "Khách hàng thường xuyên", "Khách hàng có nhiều hóa đơn", "Khách hàng VIP"};

    public static final String CUSTOMER_CATEGORY_DESCRIPTION[] = {"Khách hàng mới đến trong vòng 1 tháng","Khách hàng mua ít nhất 1 gói dịch vụ", "Khách hàng đã có từ 20 hóa đơn trở lên", "Khách hàng có hóa đơn trên 300 triệu" };

    public static final String ORDER_SERVICE[] = {"Sản phẩm", "Gói dịch vụ", "Dịch vụ"};
}
