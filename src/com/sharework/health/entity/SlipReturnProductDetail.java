package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@IdClass(SlipReturnProductDetail_PK.class)
public class SlipReturnProductDetail implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "")
    private SlipReturnProduct slipReturnProduct;

    @Id
    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    private int quantityStock;

    private int quantity;
}
