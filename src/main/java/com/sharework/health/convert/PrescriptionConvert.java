package com.sharework.health.convert;

import com.sharework.health.dto.PrescriptionDto;
import com.sharework.health.entity.Prescription;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class PrescriptionConvert implements BaseConvert<Prescription, PrescriptionDto> {

    @Override
    public PrescriptionDto entityToDto(Prescription entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, PrescriptionDto.class);
    }

    @Override
    public Prescription dtoToEntity(PrescriptionDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, Prescription.class);
    }
}
