package com.sharework.health.entity;

public enum Status {
    ACTIVE, DEACTIVE, EXAMINED, INACTIVE
}
