package com.sharework.health.service.impl;

import com.sharework.health.convert.ExaminationCardConvert;
import com.sharework.health.convert.ExaminationCardDetailConvert;
import com.sharework.health.dto.*;
import com.sharework.health.entity.*;
import com.sharework.health.repository.*;
import com.sharework.health.response.ExaminationCardResponse;
import com.sharework.health.service.ExaminationCardService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackOn = Exception.class)
@AllArgsConstructor
public class ExaminationCardServiceImpl implements ExaminationCardService {

    private static final Path CURRENT_FOLDER = Paths.get(System.getProperty("user.dir"));

    private ExaminationCardRepository examinationCardRepository;

    private ExaminationCardDetailRepository examinationCardDetailRepository;

    private ServiceRepository serviceRepository;

    private ProductServiceDetailRepository productServiceDetailRepository;

    private ExaminationCardConvert examinationCardConvert;

    private CustomerRepository customerRepository;

    private UserRepository userRepository;

    private ExaminationCardDetailConvert examinationCardDetailConvert;

    private SlipUseRepository slipUseRepository;

    private SlipUseDetailRepository slipUseDetailRepository;

    private ProductRepository productRepository;

    @Override
    public List<ExaminationCardDto> findAll() {
        List<ExaminationCard> examinationCards = examinationCardRepository.findAll();
        List<ExaminationCardDto> examinationCardDtos = new ArrayList<>();
        for (ExaminationCard entity: examinationCards
             ) {
            ExaminationCardDto dto = examinationCardConvert.entityToDto(entity);
            examinationCardDtos.add(dto);
        }
        return examinationCardDtos;
    }

    @Override
    public ExaminationCardDto findById(Integer id) {
        ExaminationCard entity = examinationCardRepository.findById(id).get();
        if (entity == null){
            return null;
        }
        return examinationCardConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(ExaminationCardDto dto) {
        if (dto == null){
            return false;
        }
        SlipUse slipUse = slipUseRepository.findById(dto.getSlipUseDto().getId()).get();
        if (slipUse != null){
            ExaminationCard entity = examinationCardConvert.dtoToEntity(dto);
            entity.setId(null);
            entity.setStatus(Status.DEACTIVE);
            if (dto.getDateOfExamination() == null){
                entity.setDateOfExamination(LocalDateTime.now());
            }else {
                entity.setDateOfExamination(dto.getDateOfExamination());
            }
            entity.setSlipUse(slipUse);
            examinationCardRepository.save(entity);
            return true;
        }
        return false;
    }

    @Override
    public boolean update(Integer id, ExaminationCardDto dto) {
        if (dto == null){
            return false;
        }
        ExaminationCard oldExaminationCard = examinationCardRepository.findById(id).get();
        if (oldExaminationCard != null){
            SlipUse slipUse = slipUseRepository.findById(dto.getSlipUseDto().getId()).get();
            //User user = userRepository.findById(dto.getUserDto().getId()).get();
            if (slipUse != null){
                oldExaminationCard.setSlipUse(slipUse);
//                oldExaminationCard.setUser(user);
                examinationCardRepository.save(oldExaminationCard);
                return true;
            }
            return false;
        }
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

//    @Override
//    public boolean activeExaminationCard(Integer examinationCardId, MultipartFile signature) throws IOException {
//        ExaminationCard entity = examinationCardRepository.findById(examinationCardId).get();
//        if (entity == null){
//            return false;
//        }
//        if (signature == null){
//            return false;
//        }
//        Path staticPath = Paths.get("static");
//        Path signaturePath = Paths.get("signatures");
//
//        if (!Files.exists(CURRENT_FOLDER.resolve(staticPath).resolve(signaturePath))){
//            Files.createDirectories(CURRENT_FOLDER.resolve(staticPath).resolve(signaturePath));
//        }
//        if (signature != null && entity.getSignature() == null && entity.getStatus() == Status.DEACTIVE){
//            Path path = Paths.get("static/signatures/");
//            try {
//                InputStream inputStream = signature.getInputStream();
//                Files.copy(inputStream, path.resolve(signature.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
//                entity.setSignature(signature.getOriginalFilename().toLowerCase());
//                entity.setStatus(Status.ACTIVE);
//                examinationCardRepository.save(entity);
//                return true;
//            }catch (Exception e){
//                e.printStackTrace();
//            }
//        }
//        return false;
//    }

    @Override
    public boolean activeExaminationCard(Integer examinationCardId) {
        ExaminationCard entity = examinationCardRepository.findById(examinationCardId).get();
        List<ExaminationCardDetail> examinationCardDetail = examinationCardDetailRepository.findAllByExaminationCard(examinationCardId);
        if (entity == null){
            return false;
        }
        //com.sharework.health.entity.Service service = serviceRepository.findById().get();
        if (entity.getSignature() == null && entity.getStatus() == Status.DEACTIVE) {
            entity.setStatus(Status.ACTIVE);
            List<SlipUseDetail> slipUseDetails = slipUseDetailRepository.findAllBySlipUse(entity.getSlipUse().getId());
            for (SlipUseDetail slipUseDetail: slipUseDetails
            ) {
                System.out.println(slipUseDetails.size());
                for(ExaminationCardDetail ex: examinationCardDetail) {
                    if (slipUseDetail.getService().getId() == ex.getService().getId()) {
                        System.out.println(ex.getService().getId());
                        System.out.println(slipUseDetail.getService().getId());
                        slipUseDetail.setNumberOfStock(slipUseDetail.getNumberOfStock() - 1);
                        //slipUseDetail.setNumberOfUse(slipUseDetail.getNumberOfUse() + 1);
                        slipUseDetailRepository.save(slipUseDetail);
                        return true;
                    }
                }
            }
            examinationCardRepository.save(entity);
            return true;
        }
        return false;
    }

    @Override
    public boolean ExaminedExaminationCard(Integer examinationCardId) {
        ExaminationCard entity = examinationCardRepository.findById(examinationCardId).get();
        if (entity == null){
            return false;
        }
        if (entity.getSignature() == null && entity.getStatus() == Status.ACTIVE) {
            entity.setStatus(Status.EXAMINED);
            examinationCardRepository.save(entity);
            return true;
        }
        return false;
    }

    @Override
    public boolean addExaminationCardDetail(ExaminationCardDetailDto dto) {
        ExaminationCard examinationCard = examinationCardRepository.findById(dto.getExaminationCardDto().getId()).get();
        com.sharework.health.entity.Service service = serviceRepository.findById(dto.getServiceDto().getId()).get();
        User user = userRepository.findById(dto.getUserDto().getId()).get();
        if ( examinationCard != null && service != null && user != null){
            List<ExaminationCardDetail> examinationCardDetails = examinationCardDetailRepository.findAllByExaminationCard(examinationCard.getId());
            if (examinationCardDetails.isEmpty()){
                ExaminationCardDetail entity = examinationCardDetailConvert.dtoToEntity(dto);
                entity.setExaminationCard(examinationCard);
                entity.setService(service);
                entity.setStatus(ExaminationStatus.Examining);
                entity.setUser(user);
                examinationCardDetailRepository.save(entity);

                List<SlipUseDetail> slipUseDetails = slipUseDetailRepository.findAllBySlipUse(examinationCard.getSlipUse().getId());
                for (SlipUseDetail slipUseDetail: slipUseDetails
                ) {
                    if (slipUseDetail.getService().getId() == service.getId()){
                        slipUseDetail.setNumberOfStock(slipUseDetail.getNumberOfStock());
                        slipUseDetailRepository.save(slipUseDetail);
                        return true;
                    }
                }
            }
            else {
                for (ExaminationCardDetail examinationCardDetail: examinationCardDetails
                ) {
                    if (examinationCardDetail.getService().getId() != dto.getServiceDto().getId()){
                        ExaminationCardDetail entity = examinationCardDetailConvert.dtoToEntity(dto);
                        entity.setExaminationCard(examinationCard);
                        entity.setService(service);
                        entity.setStatus(ExaminationStatus.Examining);
                        entity.setUser(user);
                        examinationCardDetailRepository.save(entity);

                        List<SlipUseDetail> slipUseDetails = slipUseDetailRepository.findAllBySlipUse(examinationCard.getSlipUse().getId());
                        for (SlipUseDetail slipUseDetail: slipUseDetails
                        ) {
                            if (slipUseDetail.getService().getId() == service.getId()){
                                slipUseDetail.setNumberOfStock(slipUseDetail.getNumberOfStock());
                                slipUseDetailRepository.save(slipUseDetail);
                                return true;
                            }
                        }
                    }
                    if (examinationCardDetail.getService().getId() == dto.getServiceDto().getId()){
                        ExaminationCardDetail entity = examinationCardDetailConvert.dtoToEntity(dto);
                        entity.setExaminationCard(examinationCard);
                        entity.setService(service);
                        entity.setStatus(ExaminationStatus.Examining);
                        entity.setUser(user);
                        examinationCardDetailRepository.save(entity);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public boolean addExaminationCardDetail2(ExaminationCardDetailDto dto) {
        ExaminationCard examinationCard = examinationCardRepository.findById(dto.getExaminationCardDto().getId()).get();
        com.sharework.health.entity.Service service = serviceRepository.findById(dto.getServiceDto().getId()).get();
        User user = userRepository.findById(dto.getUserDto().getId()).get();
        if ( examinationCard != null && service != null && user != null){
            List<ExaminationCardDetail> examinationCardDetails = examinationCardDetailRepository.findAllByExaminationCard(examinationCard.getId());
            if (examinationCardDetails.isEmpty()){
                ExaminationCardDetail entity = examinationCardDetailConvert.dtoToEntity(dto);
                entity.setExaminationCard(examinationCard);
                entity.setService(service);
                entity.setStatus(ExaminationStatus.Examining);
                entity.setUser(user);
                examinationCardDetailRepository.save(entity);


            }
            else {
                for (ExaminationCardDetail examinationCardDetail: examinationCardDetails
                ) {
                    if (examinationCardDetail.getService().getId() != dto.getServiceDto().getId()){
                        ExaminationCardDetail entity = examinationCardDetailConvert.dtoToEntity(dto);
                        entity.setExaminationCard(examinationCard);
                        entity.setService(service);
                        entity.setStatus(ExaminationStatus.Examining);
                        entity.setUser(user);
                        examinationCardDetailRepository.save(entity);


                    }
                    if (examinationCardDetail.getService().getId() == dto.getServiceDto().getId()){
                        ExaminationCardDetail entity = examinationCardDetailConvert.dtoToEntity(dto);
                        entity.setExaminationCard(examinationCard);
                        entity.setService(service);
                        entity.setStatus(ExaminationStatus.Examining);
                        entity.setUser(user);
                        examinationCardDetailRepository.save(entity);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public boolean confirmExamination(Integer examinationCardId,Integer serviceId, Integer userId) {
        ExaminationCardDetail entity = examinationCardDetailRepository.findAllByExaminationCardServiceUser(examinationCardId, serviceId, userId);
        if (entity == null){
            return false;
        }
        if (entity.getStatus() == ExaminationStatus.Examining) {
            List<ExaminationCardDetail> examinationCardDetails = examinationCardDetailRepository.findAllByExaminationCardServiceStatusExamined(examinationCardId, serviceId);
            if (examinationCardDetails.isEmpty()){
                List<ProductServiceDetail> productServiceDetails = productServiceDetailRepository.findAllByServiceId(entity.getService().getId());
                for (ProductServiceDetail productServiceDetail:  productServiceDetails
                ) {
                    Product product = productRepository.findById(productServiceDetail.getProduct().getId()).get();
                    if (product.getCapacity() <= 0){
                        product.setQuantity(product.getQuantity() - productServiceDetail.getQuantity());
                        productRepository.save(product);
                    }else {
                        //int capacityStock = product.getCapacity() - productServiceDetail.getQuantity();
                        int quantityStock = (product.getTotalCapacity() - productServiceDetail.getQuantity())/(product.getCapacity());
                        product.setQuantity(quantityStock + 1);
                        product.setTotalCapacity(product.getTotalCapacity() - productServiceDetail.getQuantity());
                        productRepository.save(product);
                    }

                }
            }
            entity.setStatus(ExaminationStatus.Examined);
            examinationCardDetailRepository.save(entity);
            List<ExaminationCardDetail> ed = examinationCardDetailRepository.findAllByExaminationCard(entity.getExaminationCard().getId());
            boolean x=true;
            for (ExaminationCardDetail e: ed
                  ) {
                if(e.getStatus().toString()=="Examining"){
                    x=false;
                }
            }
            if(x==true){
                ExaminedExaminationCard(entity.getExaminationCard().getId());
            }
            return true;
        }
        return false;
    }

    @Override
    public List<ExaminationCardDetailQueryDto> findAllByUser(Integer userId) {
        List<ExaminationCardDetailQueryDto> examinationCardQueryDtos = examinationCardDetailRepository.findAllByUser(userId);
        if (examinationCardQueryDtos.isEmpty() || examinationCardQueryDtos == null){
            return null;
        }
        return examinationCardQueryDtos;
    }

    @Override
    public List<ExaminationCardDto> findAllBySlipUse(Integer slipUseId) {
        List<ExaminationCard> examinationCards = examinationCardRepository.findAllBySlipUse(slipUseId);
        List<ExaminationCardDto> examinationCardDtos = new ArrayList<>();
        for (ExaminationCard entity: examinationCards
        ) {
            ExaminationCardDto dto = examinationCardConvert.entityToDto(entity);
            examinationCardDtos.add(dto);
        }
        return examinationCardDtos;
    }

    @Override
    public List<ExaminationCardDetailDto> findAllByExaminationCard(Integer examinationCardId) {
        List<ExaminationCardDetail> examinationCardDetails = examinationCardDetailRepository.findAllByExaminationCard(examinationCardId);
        List<ExaminationCardDetailDto> examinationCardDetailDtos = new ArrayList<>();
        for (ExaminationCardDetail entity: examinationCardDetails
        ) {
            ExaminationCardDetailDto dto = examinationCardDetailConvert.entityToDto(entity);
            examinationCardDetailDtos.add(dto);
        }
        return examinationCardDetailDtos;
    }

    @Override
    public ExaminationCardDto findExaminationCardAfterInsert() {
        ExaminationCard entity = examinationCardRepository.getExaminationCardAfterInsert();
        if (entity == null){
            return null;
        }
        return examinationCardConvert.entityToDto(entity);
    }

    @Override
    public List<ExaminationCardCustomerQueryDto> getExaminationCardWithCustomer() {
        List<ExaminationCardCustomerQueryDto> list = examinationCardRepository.getExaminationCardWithCustomer();
        if (list.isEmpty()){
            return null;
        }
        return list;
    }

    @Override
    public List<ExaminationcardOfCustomerDto> getExaminationCardByIDCustomer(Integer id) {
        List<ExaminationCardCustomerQueryDto> list = examinationCardRepository.getExaminationCardByIDCustomer(id);
        List<ExaminationcardOfCustomerDto> dto = new ArrayList<>();
        for (ExaminationCardCustomerQueryDto d: list) {
            ExaminationcardOfCustomerDto dto1 = new ExaminationcardOfCustomerDto();
            dto1.setId(d.getId());
            dto1.setDateOfExamination(d.getDateOfExamination());
            List<ExaminationCardDetailDto> liste= findAllByExaminationCard(d.getId());
            String a="";
            for (ExaminationCardDetailDto t: liste) {
                a= t.getServiceDto().getName()+", "+a;
            }
            dto1.setNameservice(a);
            dto.add(dto1);
        }
        if (list.isEmpty()){
            return null;
        }
        return dto;
    }

    @Override
    public ExaminationCardResponse getAllExaminationCards(int pageNo, int pageSize, String sortBy, String sortDir) {
        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
                : Sort.by(sortBy).descending();
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize,sort);
        Page<ExaminationCard> examinationCards = examinationCardRepository.findAll(pageable);
        List<ExaminationCard> examinationCardList = examinationCards.getContent();

        List<ExaminationCardDto> examinationCardDtos = examinationCardList.stream().map(examinationCard -> examinationCardConvert.entityToDto(examinationCard)).collect(Collectors.toList());

        ExaminationCardResponse examinationCardResponse = new ExaminationCardResponse();
        examinationCardResponse.setData(examinationCardDtos);
        examinationCardResponse.setCurrent(examinationCards.getNumber());
        examinationCardResponse.setPageSize(examinationCards.getSize());
        examinationCardResponse.setTotalElements(examinationCards.getTotalElements());
        examinationCardResponse.setTotalPages(examinationCards.getTotalPages());
        examinationCardResponse.setLast(examinationCards.isLast());

        return examinationCardResponse;
    }

//    @Override
//    public List<ExaminationCardQueryDto> findAllExaminationCardByUserAndCustomer() {
//        List<ExaminationCardQueryDto> examinationCardQueryDtos = examinationCardRepository.findAllExaminationCardByUserAndCustomer();
//        if (examinationCardQueryDtos.isEmpty()){
//            return null;
//        }
//        return examinationCardQueryDtos;
//    }
}
