package com.sharework.health.service;

public interface WarehouseReceiptDetailInfo {
    int getNumberProductImport();

    ProductInfo getProduct();

    WarehouseReceiptInfo getWarehouseReceipt();

    interface ProductInfo {
        Integer getId();
    }

    interface WarehouseReceiptInfo {
        Integer getId();
    }
}
