package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class OthersWarehouseReceiptDetailsDto implements Serializable{

    private OthersWarehouseReceiptDto othersWarehouseReceiptDto;

    private ClinicStockDto clinicStockDto;

    private ProductBatchDto productBatchDto;

    private Integer number_product;

}
