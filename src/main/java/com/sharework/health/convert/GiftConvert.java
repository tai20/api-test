package com.sharework.health.convert;


import com.sharework.health.dto.GiftDto;
import com.sharework.health.dto.GiftListDto;
import com.sharework.health.entity.Gift;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class GiftConvert implements BaseConvert<Gift, GiftDto>{
    @Override
    public GiftDto entityToDto(Gift entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, GiftDto.class);
    }

    @Override
    public Gift dtoToEntity(GiftDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, Gift.class);
    }
}
