package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ImportReceiptDetailsDto {
    private ImportReceiptDto importReceiptDto;
    private ClinicStockDto clinicStockDto;
    private Integer number_product_import;
    private BigDecimal import_price;
    private ProductBatchDto productBatchDto;

}
