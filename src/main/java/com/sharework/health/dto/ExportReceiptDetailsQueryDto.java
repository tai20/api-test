package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ExportReceiptDetailsQueryDto {

    private Integer receipt_id;

    private Integer clinic_stock_id;

    private Integer product_branch_id;

    private String product_branch_name;

    private Integer number_product_export;

    private Integer product_id;

    private String name;

    private BigDecimal export_price;
}
