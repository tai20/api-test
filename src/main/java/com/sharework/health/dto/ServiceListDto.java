package com.sharework.health.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sharework.health.entity.Customer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ServiceListDto {
    private Integer id;

    private Integer quantity;

    private Integer remains;

    private ServiceDto serviceDto;

    private CustomerDto customerDto;

}
