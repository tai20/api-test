package com.sharework.health.controller.admin;

import com.sharework.health.dto.AdvisoryDto;
import com.sharework.health.dto.TestServiceDto;
import com.sharework.health.dto.TotalCostDto;
import com.sharework.health.service.TestServiceService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/testservice")
@AllArgsConstructor
public class TestServiceController {

    private TestServiceService testServiceService;

    @GetMapping("")
    public Object getAllTestService() {
        List<TestServiceDto> testServiceDtos = testServiceService.findAll();
        if (testServiceDtos.isEmpty() || testServiceDtos == null) {
            return new ResponseEntity<>("Không có dữ liệu", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(testServiceDtos, HttpStatus.OK);
    }

    @PostMapping("")
    public Object addTestService(@RequestBody TestServiceDto dto,
                                 BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = testServiceService.insert(dto);
        if (!result) {
            return new ResponseEntity<>("Thêm thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm thành công", HttpStatus.CREATED);
    }

    @PutMapping("{testservice_id}")
    public Object updateTestService(@PathVariable("testservice_id") Integer testserviceId, @RequestBody TestServiceDto dto,
                                    BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = testServiceService.update(testserviceId, dto);
        if (!result) {
            return new ResponseEntity<>("Cập nhật thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhật thành công", HttpStatus.OK);
    }

    @DeleteMapping("{testservice_id}")
    public Object deleteTestService(@PathVariable("testservice_id") Integer testserviceId) {

        boolean result = testServiceService.delete(testserviceId);
        if (!result) {
            return new ResponseEntity<>("Xóa thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Xóa thành công", HttpStatus.OK);
    }
}
