package com.sharework.health.service;

import com.sharework.health.dto.IndicationCardDetailDto;
import com.sharework.health.dto.SlipUsePackageTestDetailDto;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SlipUsePackageTestDetailService extends BaseService<SlipUsePackageTestDetailDto, Integer> {
    List<SlipUsePackageTestDetailDto> getSlipUsePackageTestDetailBySlipUsePackageId(Integer slipUsePackageTestId);
}
