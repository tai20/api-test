package com.sharework.health.controller.admin;


import com.sharework.health.dto.*;
import com.sharework.health.service.GiftService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/admin/gift")
@AllArgsConstructor
public class GiftController {

    private GiftService giftService;

    @GetMapping("")
    public Object getAllGift(){
        List<GiftAndCouponDto> giftDtos = giftService.getAllCouponGift();
        if (giftDtos.size()==0){
            return new ResponseEntity<>("Hiện không có mã khuyến mã nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(giftDtos, HttpStatus.OK);
    }


    @GetMapping("{gift_id}")
    public Object getGiftByID(@PathVariable("gift_id") Integer giftId){
        GiftDto listDtos = giftService.findById(giftId);
        if (listDtos == null){
            return new ResponseEntity<>("Hiện không có mã khuyến mã nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(listDtos, HttpStatus.OK);
    }

    @GetMapping("findByTimes/{times}")
    public Object findByTimes(@PathVariable("times") Integer times){
        List<GiftAndCouponDto> findByTimes = giftService.findByTimes(times);
        if (findByTimes == null){
            return new ResponseEntity<>("Hiện không có mã khuyến mã nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(findByTimes, HttpStatus.OK);
    }


    @PostMapping("")
    public Object addGift(@RequestBody GiftDto dto) {
        if (dto == null) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = giftService.insert(dto);
        if (!result) {
            return new ResponseEntity<>("Thêm quà tặng thất bại", HttpStatus.BAD_REQUEST);
        }
        GiftDto giftDto = giftService.findGiftAfterInsert();
        GiftAfterInsertDto giftAfterInsertDto = new GiftAfterInsertDto()
                .setMessage("Thêm quà tặng thành công")
                .setGiftDto(giftDto);
        return new ResponseEntity<>(giftDto, HttpStatus.OK);
    }


    @PutMapping("{gift_id}")
    public Object updateGift(@PathVariable("gift_id") Integer giftId, @RequestBody GiftDto dto) {

        if (dto == null) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }

        boolean result = giftService.update(giftId, dto);
        if (!result) {
            return new ResponseEntity<>("Cập nhập quà tặng thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhập quà tặng thành công", HttpStatus.OK);
    }

    @PutMapping("reload")
    public Object reloadTimeGift(@RequestBody Integer[] ids) {

        boolean result = giftService.reloadTimesGift(ids);
        if (!result) {
            return new ResponseEntity<>("Xử lý thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Xử lý thành công", HttpStatus.OK);
    }

    @DeleteMapping("/{gift_id}")
    public ResponseEntity<CustomerDto> deleteGift(@PathVariable("gift_id") Integer id) {
        giftService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/giftforuser/{id_customer}")
    public ResponseEntity getGiftforUser(Principal principal, @PathVariable("id_customer") Integer id_customer) {
        List<GiftUserDto> giftLists= giftService.getGiftUser(principal,id_customer);
        if (giftLists.size() == 0) {
            return new ResponseEntity<>("Bạn không có quà tặng nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(giftLists, HttpStatus.OK);
    }

    @GetMapping("/test")
    public ResponseEntity getAllGiftnew() {
        List<GiftDto> giftDtos = giftService.findAll();
        if (giftDtos.size() == 0) {
            return new ResponseEntity<>("Hiện không có mã khuyến mã nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(giftDtos, HttpStatus.OK);
    }
}
