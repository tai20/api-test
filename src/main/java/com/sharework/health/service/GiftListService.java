package com.sharework.health.service;


import com.sharework.health.entity.GiftList;

public interface GiftListService extends BaseService<GiftList, Integer> {
}
