package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDate;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MedicalCardDto {

    private Integer id;

    private String diagnose;

    private String differentDiagnose;

    private String management;

    private LocalDate nextAppointment;

    private String note;

    private String advice;

    private String vulva;

    private String vagina;

    private String cervix;

    private String fornix;

    private String tuboovarian;

    private String perineal;

    private String uterus;

    private String cardiovascularrisk;

    private String breastrisk;

    private String popq;

    private String cough;

    private String bonney;

    private String knack;

    private String pelvicfloorusclestrength;

    private String oldsurgicalwound;

    private String contractions;

    private String shapeuterus;

    private String bctc;

    private String breast;

    private String heartfetus;

    private String amnioticfluidstatus;

    private String timeboken;

    private String color;

    private String smell;

    private String throne;

    private String penetration;

    private String pelvic;

    private LocalDate estimateddateofdelivery;

    private String shape;

    private String limitedbreastright;

    private String density;

    private String breastright;

    private String positionbreastright;

    private String densitybreastright;

    private String dimensionbreastright;

    private String secretionsbreastright;

    private String breastleft;

    private String positionbreastleft;

    private String densitybreastleft;

    private String dimensionbreastleft;

    private String secretionsbreastleft;

    private String axillarylymphnodes;

    private String supraclavicularlymphnodes;

    private String pulserate;

    private String bloodpressure;

    private Double height;

    private Double weight;

    private String bmi;

    private String allergy;

    private String para;

    private String obstetricsherself;

    private String internalherself;

    private String edema;

    private String heart;

    private String generalcondition;

    private String herfamily;

    private String lastmenstrualperiod;

    private String lung;

    private String mucosalskin;

    private String sign;

    private String stomach;

    private TestServiceDto testServiceDto;

    private SlipUsePackageTestDto slipUsePackageTestDto;

    private IndicationCardDto indicationCardDto;

    private PrescriptionDto prescriptionDto;

    private TotalCostDto totalCostDto;

    private String nameMedicalCard;

    private String limitedBreastLeft;

    private String bodyTemperature;

    private String others;

}
